/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _TREEADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "TreeADT.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/
static void TREEADT_PrintNode(TREEADT_TREE_TYPE Tree);

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void TREEADT_CreateTree(TREEADT_TREE_TYPE *Tree, TREEADT_ELEMENT_TYPE FirstElement)
{
   *Tree = (TREEADT_TREE_TYPE) malloc (sizeof(TREEADT_NODE_TYPE));
   
   if (*Tree == NULL)
   {
      printf("Error: malloc failed in TREEADT_CreateTree.\n");
      exit(1);
   }
   else
   {
      (*Tree)->Info = FirstElement;
      (*Tree)->Child = NULL;
      (*Tree)->Sibling = NULL;
      (*Tree)->Parent = NULL;
   }
}

/*********************************************************************************/

void TREEADT_PrintTree(TREEADT_TREE_TYPE Tree)
{
   if (Tree != NULL)
   {
      TREEADT_PrintNode(Tree);
      TREEADT_PrintTree(Tree->Sibling);
      TREEADT_PrintTree(Tree->Child);
   }
}

/*********************************************************************************/

void TREEADT_AddNodeToTree(TREEADT_TREE_TYPE Tree, 
                           TREEADT_ELEMENT_TYPE NewElement, 
                           TREEADT_ELEMENT_TYPE ParentElement)
{
   TREEADT_NODE_POINTER_TYPE NodePointer;
   TREEADT_NODE_POINTER_TYPE NewNode;
                        
   if (TREEADT_FindNode(Tree, ParentElement.Key, &NodePointer) == FALSE)
   {
      printf("Error: TREEADT_FindNode failed in TREEADT_AddNodeToTree.\n");
      exit(1);
   }
   else
   {
      NewNode = (TREEADT_NODE_POINTER_TYPE) malloc (sizeof(TREEADT_NODE_TYPE));

      if (NewNode == NULL)
      {
         printf("Error: malloc failed in TREEADT_AddNodeToTree.\n");
         exit(1);
      }
      else
      {
         NewNode->Info = NewElement;
         NewNode->Child = NULL;
         NewNode->Sibling = NULL;
         NewNode->Parent = NodePointer;
      }

      if (NodePointer->Child == NULL)
      {
         NodePointer->Child = NewNode;
      }
      else
      {
         NodePointer = NodePointer->Child;
         while (NodePointer->Sibling != NULL)
         {
            NodePointer = NodePointer->Sibling;
         }
         NodePointer->Sibling = NewNode;
      }
   }
}

/*********************************************************************************/

void TREEADT_DeleteTree(TREEADT_TREE_TYPE *Tree)
{
   if (*Tree != NULL)
   {
      TREEADT_DeleteTree(&((*Tree)->Child));
      TREEADT_DeleteTree(&((*Tree)->Sibling));
      free(*Tree);
      *Tree = NULL;
   }
}

/*********************************************************************************/

BOOLEAN TREEADT_FindNode(TREEADT_TREE_TYPE Tree, 
                        TREEADT_KEY_TYPE KeyValue, 
                        TREEADT_NODE_POINTER_TYPE *NodePointer)
{
   BOOLEAN Found = FALSE;

   if (Tree != NULL)
   {
      if (Tree->Info.Key == KeyValue)
      {
         Found = TRUE;
         *NodePointer = Tree;
      }
      else
      {
         Found = TREEADT_FindNode(Tree->Sibling, KeyValue, NodePointer);
         if (Found == FALSE)
         {
            Found = TREEADT_FindNode(Tree->Child, KeyValue, NodePointer);
         }
      }
   }
   
   return (Found);
}

/*********************************************************************************/

void TREEADT_PrintNode(TREEADT_TREE_TYPE Tree)
{
   if (Tree != NULL)
   {
      printf("%ld Parent = ", Tree->Info.Key);

      if (Tree->Parent != NULL)
      {
         printf("%ld\n", Tree->Parent->Info.Key);
      }
      else
      {
         printf("None\n");
      }
   }
}


