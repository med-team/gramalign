/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _GROUPARRAYADT
#define GROUPARRAYADT_EXTERN
#else
#define GROUPARRAYADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef struct
{
   ULONG GroupId;
   ULONG ElementId;
   ULONG ParentId;
   ULONG GroupSize;
} GROUPARRAYADT_RUN_TYPE;

typedef struct
{
   ULONG Length;
   GROUPARRAYADT_RUN_TYPE *GroupRuns;
   ULONG *ElementLocation;
   ULONG NextAvailableLocation;
} GROUPARRAYADT_TYPE;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/

void GROUPARRAYADT_InitializeGroupArray(GROUPARRAYADT_TYPE *GroupArray);
void GROUPARRAYADT_DefineGroupArrayLength(GROUPARRAYADT_TYPE *GroupArray, ULONG Length);
void GROUPARRAYADT_CleanUpGroupArray(GROUPARRAYADT_TYPE *GroupArray);
void GROUPARRAYADT_SetElementGroup(GROUPARRAYADT_TYPE *GroupArray, 
                                   ULONG ElementId, 
                                   ULONG ParentId,
                                   BOOLEAN InAGroup);

