/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _STADT
#define STADT_EXTERN
#else
#define STADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef struct
{
   ULONG EdgeStart;
   ULONG EdgeStop;
   ULONG Depth;
} STADT_ELEMENT_TYPE;

typedef struct STADT_node
{
   STADT_ELEMENT_TYPE Info;
   struct STADT_node *Child;
   struct STADT_node *PreviousSibling;
   struct STADT_node *NextSibling;
   struct STADT_node *Parent;
   struct STADT_node *SuffixLink;
} STADT_NODE_TYPE;

typedef struct
{
   UBYTE *String;
   BOOLEAN *Reproducible;
   ULONG Length;
   STADT_NODE_TYPE *Tree;
} STADT_TREE_TYPE;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/
void STADT_CreateTree (STADT_TREE_TYPE *Tree, 
                        UBYTE *String, 
                        ULONG Length,
                        UBYTE Terminator);
void STADT_DeleteTree (STADT_TREE_TYPE *Tree);
void STADT_PrintTree (STADT_TREE_TYPE *Tree);
BOOLEAN STADT_FindSubstring (STADT_TREE_TYPE *Tree, 
                              UBYTE *String, 
                              ULONG Length);
