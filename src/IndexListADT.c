/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _INDEXLISTADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "IndexListADT.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void INDEXLISTADT_InitializeIndexList(INDEXLISTADT_INDEXLIST_TYPE *IndexList, 
                                       ULONG MaxLength,
                                       INDEXLISTADT_MINMAX_TYPE MinMaxType,
                                       BYTE *CallingFunction)
{
   IndexList->Scores = (FLOAT *) malloc(sizeof(FLOAT) * MaxLength);
   if (IndexList->Scores == NULL)
   {
      printf("Error: malloc failed in %s.\n", CallingFunction);
      exit(1);
   }

   IndexList->Indicies = (ULONG *) malloc(sizeof(ULONG) * MaxLength);
   if (IndexList->Indicies == NULL)
   {
      printf("Error: malloc failed in %s.\n", CallingFunction);
      exit(1);
   }

   IndexList->Length = 0;
   IndexList->MaxLength = MaxLength;
   IndexList->MinMaxType = MinMaxType;
}

/*********************************************************************************/

void INDEXLISTADT_CleanUpIndexList(INDEXLISTADT_INDEXLIST_TYPE *IndexList)
{
   if (IndexList->Scores != NULL)
   {
      free(IndexList->Scores);
   }
   
   if (IndexList->Indicies != NULL)
   {
      free(IndexList->Indicies);
   }
   
   IndexList->MaxLength = 0;
   IndexList->Length = 0;
   IndexList->Scores = NULL;
   IndexList->Indicies = NULL;   
}

/*********************************************************************************/

void INDEXLISTADT_InsertIndexIntoList(INDEXLISTADT_INDEXLIST_TYPE *IndexList, 
                                       ULONG Index,
                                       FLOAT Score)
{
   ULONG i;
   ULONG ReplaceIndex;
   FLOAT WorstScore;
   
   if (IndexList->Length < IndexList->MaxLength)
   {
      IndexList->Scores[IndexList->Length] = Score;
      IndexList->Indicies[IndexList->Length] = Index;
      IndexList->Length += 1;
   }
   else
   {
      if (IndexList->MinMaxType == INDEXLISTADT_MINMAX_MIN)
      {
         ReplaceIndex = 0;
         WorstScore = IndexList->Scores[0];
         for (i = 1; i < IndexList->Length; i++)
         {
            if (IndexList->Scores[i] > WorstScore)
            {
               ReplaceIndex = i;
               WorstScore = IndexList->Scores[i];
            }
         }

         if (WorstScore > Score)
         {
            IndexList->Scores[ReplaceIndex] = Score;
            IndexList->Indicies[ReplaceIndex] = Index;
         }
      }
      else
      {
         ReplaceIndex = 0;
         WorstScore = IndexList->Scores[0];
         for (i = 1; i < IndexList->Length; i++)
         {
            if (IndexList->Scores[i] < WorstScore)
            {
               ReplaceIndex = i;
               WorstScore = IndexList->Scores[i];
            }
         }

         if (WorstScore < Score)
         {
            IndexList->Scores[ReplaceIndex] = Score;
            IndexList->Indicies[ReplaceIndex] = Index;
         }
      }
   }
}
