/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _MATRIXUBYTEADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "MatrixUByteADT.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void MATRIXUBYTEADT_InitializeMatrix(MATRIXUBYTEADT_MATRIX_TYPE *Matrix)
{
   Matrix->NumRows = 0;
   Matrix->NumCols = 0;
   Matrix->Rows = NULL;
}

/*********************************************************************************/

void MATRIXUBYTEADT_DefineMatrix(MATRIXUBYTEADT_MATRIX_TYPE *Matrix, ULONG Rows, ULONG Cols)
{
   ULONG i;
   
   Matrix->NumRows = Rows;
   Matrix->NumCols = Cols;
   Matrix->Rows = (MATRIXUBYTEADT_ROW_TYPE *) malloc (sizeof(MATRIXUBYTEADT_ROW_TYPE) * Rows);
   if (Matrix->Rows == NULL)
   {
      printf("Error: malloc failed in MATRIXUBYTEADT_InitializeMatrix.\n");
      exit(1);
   }
   else
   {
      for (i = 0; i < Rows; i++)
      {
         Matrix->Rows[i].Cols = (UBYTE *) malloc (sizeof(UBYTE) * Cols);
         if (Matrix->Rows[i].Cols == NULL)
         {
            printf("Error: malloc failed in MATRIXUBYTEADT_InitializeMatrix.\n");
            exit(1);
         }
         else
         {
            memset(Matrix->Rows[i].Cols, 0, sizeof(UBYTE) * Cols);
         }
      }
   }
}

/*********************************************************************************/

void MATRIXUBYTEADT_CleanUpMatrix(MATRIXUBYTEADT_MATRIX_TYPE *Matrix)
{
   ULONG i;

   for (i = 0; i < Matrix->NumRows; i++)
   {
      if (Matrix->Rows[i].Cols != NULL)
      {
         free(Matrix->Rows[i].Cols);
         Matrix->Rows[i].Cols = NULL;
      }
   }

   if (Matrix->Rows != NULL)
   {
      free(Matrix->Rows);
      Matrix->Rows = NULL;
   }

   Matrix->NumRows = 0;
   Matrix->NumCols = 0;
}

/*********************************************************************************/

void MATRIXUBYTEADT_SetElement(MATRIXUBYTEADT_MATRIX_TYPE *Matrix, 
                           UBYTE Element, 
                           ULONG Row, 
                           ULONG Col)
{
   if ((Row < Matrix->NumRows) && (Col < Matrix->NumCols))
   {
      Matrix->Rows[Row].Cols[Col] = Element;
   }
   else
   {
      printf("Error: either Row or Col is outside the dimension of the matrix.\n");
      MATRIXUBYTEADT_CleanUpMatrix(Matrix);
      exit(1);
   }
}

/*********************************************************************************/

UBYTE MATRIXUBYTEADT_GetElement(MATRIXUBYTEADT_MATRIX_TYPE *Matrix, 
                           ULONG Row, 
                           ULONG Col)
{
   UBYTE Element;
   
   if ((Row < Matrix->NumRows) && (Col < Matrix->NumCols))
   {
      Element = Matrix->Rows[Row].Cols[Col];
   }
   else
   {
      printf("Error: either Row or Col is outside the dimension of the matrix.\n");
      MATRIXUBYTEADT_CleanUpMatrix(Matrix);
      exit(1);
   }
   
   return (Element);
}
                           
/*********************************************************************************/

void MATRIXUBYTEADT_PrintMatrix(MATRIXUBYTEADT_MATRIX_TYPE *Matrix)
{
   ULONG i;
   ULONG j;
   
   for (i = 0; i < Matrix->NumRows; i++)
   {
      for (j = 0; j < Matrix->NumCols; j++)
      {
         printf("%d\t", MATRIXUBYTEADT_GetElement(Matrix, i, j));
      }
      printf("\n");
   }
}
