/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _PAIRS
#define PAIRS_EXTERN
#else
#define PAIRS_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/

void PAIRS_ConstructPairwiseAlignment(ENSEMBLEADT_TYPE *Ensemble,
                                      STRINGADT_STRING_TYPE *StringToAdd,
                                      STRINGADT_STRING_TYPE *GrammarPieceStringToAdd,
                                      ULONG Id,
                                      FLOAT WindowOverhangPercentage);
void PAIRS_MergeEnsembleAlignments(ENSEMBLEADT_TYPE *Ensemble,
                                   ENSEMBLEADT_TYPE *LocalGroupEnsemble,
                                   GROUPARRAYADT_TYPE *SequenceAlignmentOrder,
                                   FLOAT WindowOverhangPercentage);

