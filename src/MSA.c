/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _MSA

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "CoinFlip.h"
#include "MSA.h"
#include "StringADT.h"
#include "StringListADT.h"
#include "MatrixUByteADT.h"
#include "MatrixFloatADT.h"
#include "SparseMatrixFloatADT.h"
#include "FileIO.h"
#include "Gram.h"
#include "TreeADT.h"
#include "Prims.h"
#include "Parameters.h"
#include "GroupArrayADT.h"
#include "EnsembleADT.h"
#include "Pairs.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/
#define MSA_PERIOD_CHAR       '.'
#define MSA_DASH_CHAR         '-'

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/
static void MSA_ScanTreeForAlignmentOrder(TREEADT_TREE_TYPE Tree);
static void MSA_AddSequenceToAlignmentOrder(TREEADT_TREE_TYPE Tree);
static void MSA_AlignSequences(void);

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/
static STRINGLISTADT_STRINGLIST_TYPE m_Sequences;
static STRINGLISTADT_STRINGLIST_TYPE m_MergedAlphabetSequences;
static STRINGLISTADT_STRINGLIST_TYPE m_HeaderLines;
static SMF_ADT_MATRIX_TYPE m_DistanceMatrix;
static STRINGLISTADT_STRINGLIST_TYPE m_GrammarPieces;

static GROUPARRAYADT_TYPE m_SequenceAlignmentOrder;
static ENSEMBLEADT_TYPE m_Ensemble;

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

INT main(INT argc, BYTE **argv)
{
   TREEADT_TREE_TYPE MinimalSpanningTree;
   STRINGLISTADT_STRINGLIST_TYPE AlignedStrings;
   STRINGLISTADT_STRINGLIST_TYPE AlignedGrammarPieces;
   STRINGADT_STRING_TYPE ConsensusString;
   FLOAT *ConsensusConfidence;
   STRINGLISTADT_STRINGLIST_TYPE HeaderLines;
   ULONG i;
   ULONG j;
   STRINGADT_STRING_TYPE ZeroString;

   if (PARMS_ProcessCommandLine(argc, argv) == TRUE)
   {
      STRINGLISTADT_InitializeStringList(&m_Sequences);
      STRINGLISTADT_InitializeStringList(&m_MergedAlphabetSequences);
      STRINGLISTADT_InitializeStringList(&m_HeaderLines);
      SMF_ADT_InitializeMatrix(&m_DistanceMatrix);
      GROUPARRAYADT_InitializeGroupArray(&m_SequenceAlignmentOrder);

      COINFLIP_Reset();
      
      FILEIO_ReadFASTASequences(&m_Sequences, &m_MergedAlphabetSequences, &m_HeaderLines);

      if (m_Sequences.NumberOfStrings > 1)
      {
         GROUPARRAYADT_DefineGroupArrayLength(&m_SequenceAlignmentOrder,  m_Sequences.NumberOfStrings);
         
         PARMS_Printf("Calculating grammar-based distances.\n");
         if (PARMS_GetUseMergedAlphabetForDistance() == TRUE)
         {
            GRAM_CalculateDistances(&m_MergedAlphabetSequences, &m_DistanceMatrix);
         }
         else
         {
            GRAM_CalculateDistances(&m_Sequences, &m_DistanceMatrix);
         }

         if (PARMS_GetOutputFileFormat() == PARMS_OUTPUT_DISTANCE_MATRIX)
         {
            FILEIO_WriteDistanceMatrix(&m_DistanceMatrix);
         }
         else
         {
            STRINGLISTADT_InitializeStringList(&m_GrammarPieces);
            if (PARMS_GetUseGrammarPieceFile() == TRUE)
            {
               STRINGLISTADT_InitializeStringList(&HeaderLines);
               FILEIO_ReadFASTAGrammarPieces(&m_GrammarPieces, &HeaderLines);
               STRINGLISTADT_CleanUpStringList(&HeaderLines);
            
               if (m_Sequences.NumberOfStrings != m_GrammarPieces.NumberOfStrings)
               {
                  printf("ERROR: grammar piece file does not match input sequence file.\n");
                  exit(1);
               }
               else
               {
                  for (i = 0; i < m_Sequences.NumberOfStrings; i++)
                  {
                     if (m_Sequences.Strings[i].Length != m_GrammarPieces.Strings[i].Length)
                     {
                        printf("ERROR: grammar piece file does not match input sequence file.\n");
                        exit(1);
                     }
                  }
               }
            }
            else
            {
               for (i = 0; i < m_Sequences.NumberOfStrings; i++)
               {
                  STRINGADT_InitializeString (&ZeroString);
                  for (j = 0; j < m_Sequences.Strings[i].Length; j++)
                  {
                     STRINGADT_AppendStringFromChar(&ZeroString, 0, "main");
                  }
                  STRINGLISTADT_AppendStringToStringList(&m_GrammarPieces, &ZeroString, "main");
                  STRINGADT_CleanUpString (&ZeroString);
               }
            }
   
            PARMS_Printf("Generating minimal spanning tree.\n");
            PRIMS_ConstructMinimalSpanningTree(&m_DistanceMatrix, &MinimalSpanningTree, &m_Sequences);
   
            PARMS_Printf("Generating sequence alignment order.\n");
            MSA_ScanTreeForAlignmentOrder(MinimalSpanningTree);

            PARMS_Printf("Aligning sequences.\n");
            MSA_AlignSequences();

            if ((PARMS_GetGapsInColumnThreshold() < 1.0) && (PARMS_GetGapAdjustmentWindow() > 0))
            {
               PARMS_Printf("Adjusting MSA gaps.\n");
               ENSEMBLEADT_AdjustMSAGaps(&m_Ensemble);
            }
            
            STRINGLISTADT_InitializeStringList(&AlignedStrings);
            STRINGLISTADT_InitializeStringList(&AlignedGrammarPieces);
            STRINGADT_InitializeString(&ConsensusString);
   
            switch (PARMS_GetOutputFileFormat())
            {
               case PARMS_OUTPUT_DISTANCE_MATRIX:
                  break;
   
               case PARMS_OUTPUT_PHYLIP:
                  ENSEMBLEADT_GetPrintableAlignment(&m_Ensemble,
                                                    &AlignedStrings, 
                                                    &AlignedGrammarPieces, 
                                                    &m_SequenceAlignmentOrder, 
                                                    MSA_DASH_CHAR);
                  FILEIO_WriteAlignmentInPHYLIPFormat(&AlignedStrings, 
                                                      &m_HeaderLines, 
                                                      PARMS_GetOutputFileName());
                  if (PARMS_GetUseGrammarPieceFile() == TRUE)
                  {
                     FILEIO_WriteAlignmentInPHYLIPFormat(&AlignedGrammarPieces, 
                                                         &m_HeaderLines, 
                                                         PARMS_GetOutputGrammarPieceFileName());
                  }
                  break;
                  
               case PARMS_OUTPUT_FASTA:   
                  ENSEMBLEADT_GetPrintableAlignment(&m_Ensemble,
                                                    &AlignedStrings, 
                                                    &AlignedGrammarPieces, 
                                                    &m_SequenceAlignmentOrder, 
                                                    MSA_DASH_CHAR);
                  FILEIO_WriteAlignmentInFASTAFormat(&AlignedStrings, 
                                                     &m_HeaderLines, 
                                                     PARMS_GetOutputFileName());
                  if (PARMS_GetUseGrammarPieceFile() == TRUE)
                  {
                     FILEIO_WriteAlignmentInFASTAFormat(&AlignedGrammarPieces, 
                                                        &m_HeaderLines, 
                                                        PARMS_GetOutputGrammarPieceFileName());
                  }
                  break;
                  
               case PARMS_OUTPUT_MSF_GCG:
                  ENSEMBLEADT_GetPrintableAlignment(&m_Ensemble,
                                                    &AlignedStrings, 
                                                    &AlignedGrammarPieces, 
                                                    &m_SequenceAlignmentOrder, 
                                                    MSA_PERIOD_CHAR);
                  FILEIO_WriteAlignmentInMSF_GCGFormat(&AlignedStrings, 
                                                       &m_HeaderLines, 
                                                       PARMS_GetOutputFileName());
                  if (PARMS_GetUseGrammarPieceFile() == TRUE)
                  {
                     FILEIO_WriteAlignmentInMSF_GCGFormat(&AlignedGrammarPieces, 
                                                          &m_HeaderLines, 
                                                          PARMS_GetOutputGrammarPieceFileName());
                  }
                  break;
   
               case PARMS_OUTPUT_CONSENSUS:
                  ENSEMBLEADT_GetConsensusString(&m_Ensemble,
                                                 &ConsensusString, 
                                                 &ConsensusConfidence, 
                                                 TRUE, 
                                                 MSA_DASH_CHAR,
                                                 NULL,
                                                 FALSE);
                  FILEIO_WriteConsensusSequence(&ConsensusString, ConsensusConfidence);
                  if (ConsensusConfidence != NULL)
                  {
                     free(ConsensusConfidence);
                  }
                  break;
   
               case PARMS_OUTPUT_CONSENSUS_IGNORE_GAPS:
                  ENSEMBLEADT_GetConsensusString(&m_Ensemble,
                                                 &ConsensusString, 
                                                 &ConsensusConfidence, 
                                                 FALSE, 
                                                 MSA_DASH_CHAR,
                                                 NULL,
                                                 FALSE);
                  FILEIO_WriteConsensusSequence(&ConsensusString, 
                                                ConsensusConfidence);
                  if (ConsensusConfidence != NULL)
                  {
                     free(ConsensusConfidence);
                  }
                  break;
                                 
               default:
                  printf("Unknown output file format specified.\n");
                  break;
            }
         }

         PARMS_Printf("Done.\n");

         if (PARMS_GetOutputFileFormat() != PARMS_OUTPUT_DISTANCE_MATRIX)
         {
            STRINGLISTADT_CleanUpStringList(&m_GrammarPieces);
            TREEADT_DeleteTree(&MinimalSpanningTree);
            STRINGLISTADT_CleanUpStringList(&AlignedStrings);
            STRINGLISTADT_CleanUpStringList(&AlignedGrammarPieces);
            STRINGADT_CleanUpString(&ConsensusString);
            ENSEMBLEADT_CleanUp(&m_Ensemble);
         }
      }
      else
      {
         printf("Input file was either invalid or only contained 1 sequence.\n");
      }
      
      STRINGLISTADT_CleanUpStringList(&m_Sequences);
      STRINGLISTADT_CleanUpStringList(&m_MergedAlphabetSequences);
      STRINGLISTADT_CleanUpStringList(&m_HeaderLines);
      SMF_ADT_CleanUpMatrix(&m_DistanceMatrix);
      GROUPARRAYADT_CleanUpGroupArray(&m_SequenceAlignmentOrder);
   }
   
   return(0);
}

/*********************************************************************************/

void MSA_ScanTreeForAlignmentOrder(TREEADT_TREE_TYPE Tree)
{
   if (Tree != NULL)
   {
      MSA_AddSequenceToAlignmentOrder(Tree);
      MSA_ScanTreeForAlignmentOrder(Tree->Child);
      MSA_ScanTreeForAlignmentOrder(Tree->Sibling);
   }
}

/*********************************************************************************/

void MSA_AddSequenceToAlignmentOrder(TREEADT_TREE_TYPE Tree)
{
   if (Tree != NULL)
   {
      if (Tree->Parent != NULL)
      {
         if (SMF_ADT_GetElement(&m_DistanceMatrix, Tree->Parent->Info.Key, Tree->Info.Key) <= PARMS_GetGrammarSimilarityThreshold())
         {
            GROUPARRAYADT_SetElementGroup(&m_SequenceAlignmentOrder, 
                                          Tree->Info.Key, 
                                          Tree->Parent->Info.Key,
                                          TRUE);
         }
         else
         {
            GROUPARRAYADT_SetElementGroup(&m_SequenceAlignmentOrder, 
                                          Tree->Info.Key, 
                                          Tree->Parent->Info.Key,
                                          FALSE);
         }
      }
      else
      {
         GROUPARRAYADT_SetElementGroup(&m_SequenceAlignmentOrder, 
                                       Tree->Info.Key, 
                                       Tree->Info.Key,
                                       FALSE);
      }
   }
}

/*********************************************************************************/

void MSA_AlignSequences(void)
{
   ULONG i;
   ULONG j;
   ULONG ElementId;
   ENSEMBLEADT_TYPE LocalGroupEnsemble;
   BOOLEAN WorkingOnFirstGroup = TRUE;
   FLOAT Distance;
   FLOAT OverhangPercentage;
   FLOAT SimilarOverhangPercentage = PARMS_GetSimilarAlignmentOverhangPercent();
   FLOAT DissimilarOverhangPercentage = PARMS_GetDissimilarAlignmentOverhangPercent();

   ENSEMBLEADT_Initialize(m_SequenceAlignmentOrder.Length);

   i = 0;
   while (i < m_SequenceAlignmentOrder.Length)
   {
      PARMS_PrintfProgress(i, m_SequenceAlignmentOrder.Length - 1);

      if (WorkingOnFirstGroup == TRUE)
      {
         ElementId = m_SequenceAlignmentOrder.GroupRuns[i].ElementId;
         ENSEMBLEADT_InitializeEnsemble(&m_Ensemble,
                                        &(m_Sequences.Strings[ElementId]),
                                        &(m_GrammarPieces.Strings[ElementId]),
                                        0);
         for (j = 1; j < m_SequenceAlignmentOrder.GroupRuns[i].GroupSize; j++)
         {
            PARMS_PrintfProgress(i + j, m_SequenceAlignmentOrder.Length - 1);
            ElementId = m_SequenceAlignmentOrder.GroupRuns[i + j].ElementId;

            Distance = SMF_ADT_GetElement(&m_DistanceMatrix, 
                                          m_SequenceAlignmentOrder.GroupRuns[i].ParentId, 
                                          m_SequenceAlignmentOrder.GroupRuns[i].ElementId);

            if (Distance < 0.01)
            {
               OverhangPercentage = 0.01;
            }
            else if (Distance < SimilarOverhangPercentage)
            {
               OverhangPercentage = Distance;
            }
            else
            {
               OverhangPercentage = SimilarOverhangPercentage;
            }
            PAIRS_ConstructPairwiseAlignment(&m_Ensemble,
                                             &(m_Sequences.Strings[ElementId]),
                                             &(m_GrammarPieces.Strings[ElementId]),
                                             j,
                                             OverhangPercentage);
         }

         WorkingOnFirstGroup = FALSE;
         i += m_SequenceAlignmentOrder.GroupRuns[i].GroupSize;
      }
      else
      {
         ElementId = m_SequenceAlignmentOrder.GroupRuns[i].ElementId;
         ENSEMBLEADT_InitializeEnsemble(&LocalGroupEnsemble,
                                        &(m_Sequences.Strings[ElementId]),
                                        &(m_GrammarPieces.Strings[ElementId]),
                                        0);
         for (j = 1; j < m_SequenceAlignmentOrder.GroupRuns[i].GroupSize; j++)
         {
            PARMS_PrintfProgress(i + j, m_SequenceAlignmentOrder.Length - 1);
            ElementId = m_SequenceAlignmentOrder.GroupRuns[i + j].ElementId;

            Distance = SMF_ADT_GetElement(&m_DistanceMatrix, 
                                          m_SequenceAlignmentOrder.GroupRuns[i].ParentId, 
                                          m_SequenceAlignmentOrder.GroupRuns[i].ElementId);

            if (Distance < 0.01)
            {
               OverhangPercentage = 0.01;
            }
            else if (Distance < SimilarOverhangPercentage)
            {
               OverhangPercentage = Distance;
            }
            else
            {
               OverhangPercentage = SimilarOverhangPercentage;
            }
            PAIRS_ConstructPairwiseAlignment(&LocalGroupEnsemble,
                                             &(m_Sequences.Strings[ElementId]),
                                             &(m_GrammarPieces.Strings[ElementId]),
                                             j,
                                             OverhangPercentage);
         }

         PAIRS_MergeEnsembleAlignments(&m_Ensemble,
                                       &LocalGroupEnsemble,
                                       &m_SequenceAlignmentOrder,
                                       DissimilarOverhangPercentage);

         ENSEMBLEADT_CleanUp(&LocalGroupEnsemble);
         i += m_SequenceAlignmentOrder.GroupRuns[i].GroupSize;
      }
   }
}

