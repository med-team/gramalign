/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _PARMS

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "Parameters.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/
#define PARMS_STARTING_ARGUMENT_POS                      2
#define PARMS_DELIMITER_POS                              0
#define PARMS_DELIMITER_CHAR                             '-'
#define PARMS_OPTION_POS                                 1

#define PARMS_MAX_ARGLENGTH                              4096
#define PARMS_MAX_STRING_LENGTH                          4096

#define PARMS_DEFAULT_INFILE                             "infile"
#define PARMS_DEFAULT_OUTFILE                            "outfile"
#define PARMS_DEFAULT_OUTFILE_FORMAT                     PARMS_OUTPUT_PHYLIP
#define PARMS_DEFAULT_INFILE_FORMAT                      PARMS_INPUT_SEQUENCE_AUTO_DETECT

#define PARMS_DEFAULT_AA_INDEL_GAP_START_COST            15.2
#define PARMS_DEFAULT_AA_INDEL_GAP_EXTENSION_COST        0.6
#define PARMS_DEFAULT_AA_INDEL_TAIL_GAP_START_COST       15.2
#define PARMS_DEFAULT_AA_INDEL_TAIL_GAP_EXTENSION_COST   0.3

#define PARMS_DEFAULT_DNA_INDEL_GAP_START_COST           8.7
#define PARMS_DEFAULT_DNA_INDEL_GAP_EXTENSION_COST       0.8
#define PARMS_DEFAULT_DNA_INDEL_TAIL_GAP_START_COST      8.7
#define PARMS_DEFAULT_DNA_INDEL_TAIL_GAP_EXTENSION_COST  0.4

#define PARMS_DEFAULT_GAPS_IN_COLUMN_THRESHOLD           1.0
#define PARMS_DEFAULT_GAP_ADJUSTMENT_WINDOW              0

#define PARMS_DEFAULT_AA_SUBSTITUTION_MATRIX             PARMS_AA_SUBSTITUTION_GONNET250
#define PARMS_DEFAULT_AA_MERGING                         TRUE

#define PARMS_DEFAULT_GENERATE_COMPLETE_MATRIX           FALSE

#define PARMS_DEFAULT_QUIET_MODE                         FALSE

#define PARMS_DEFAULT_MAX_TB_MATRIX_SIZE                 100

#define PARMS_DEFAULT_USE_GRAMMAR_PIECE_FILE             FALSE
#define PARMS_DEFAULT_GRAMMAR_PIECE_MISMATCH_COST        0.0
#define PARMS_DEFAULT_GRAMMAR_PIECE_MATCH_SCORE          0.0

#define PARMS_DEFAULT_GRAMMAR_SIMILARITY_THRESHOLD       0.30
#define PARMS_DEFAULT_SIMILAR_ALIGN_OVERHANG_PERCENT     0.10
#define PARMS_DEFAULT_DISSIMILAR_ALIGN_OVERHANG_PERCENT  0.25

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/
static BYTE m_InputFileName[PARMS_MAX_STRING_LENGTH];
static BYTE m_OutputFileName[PARMS_MAX_STRING_LENGTH];
static PARMS_OUTPUT_FILE_FORMAT m_OutputFileFormat;
static PARMS_INPUT_FILE_FORMAT m_InputFileFormat;
static PARMS_SEQUENCE_TYPE m_SequenceType;
static FLOAT m_AAInDelGapStartCost;
static FLOAT m_AAInDelGapExtensionCost;
static FLOAT m_AAInDelTailGapStartCost;
static FLOAT m_AAInDelTailGapExtensionCost;
static FLOAT m_DNAInDelGapStartCost;
static FLOAT m_DNAInDelGapExtensionCost;
static FLOAT m_DNAInDelTailGapStartCost;
static FLOAT m_DNAInDelTailGapExtensionCost;
static FLOAT m_GapsInColumnThreshold;
static ULONG m_GapAdjustmentWindow;
static PARMS_AA_SUBSTITUTION_MATRIX m_AASubstitutionMatrix;
static BOOLEAN m_AAMerging;
static BOOLEAN m_GenerateCompleteDistanceMatrix;
static BOOLEAN m_QuietMode;
static ULONG m_MaxStorageSizePerTraceBackMatrix;
static BYTE m_GrammarPieceFileName[PARMS_MAX_STRING_LENGTH];
static BYTE m_OutputGrammarPieceFileName[PARMS_MAX_STRING_LENGTH];
static BOOLEAN m_UseGrammarPieceFile;
static FLOAT m_GrammarPieceMismatchPenalty;
static FLOAT m_GrammarPieceMatchBenefit;
static FLOAT m_GrammarSimilarityThreshold;
static FLOAT m_SimilarAlignmentOverhangPercent;
static FLOAT m_DissimilarAlignmentOverhangPercent;

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/
static BYTE PARMS_GetNextOption(INT argc, 
                                 BYTE **argv, 
                                 BYTE *OptionDelimiters, 
                                 BYTE *OptionArgument, 
                                 INT *CurrentOptionIndex);
static void PARMS_InitializeDefaultValues(void);
static void PARMS_OutputHeaderText(void);
static void PARMS_OutputHelpText(void);
                           
/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

BYTE *PARMS_GetInputFileName(void)
{
   return (m_InputFileName);
}

/*********************************************************************************/

BYTE *PARMS_GetOutputFileName(void)
{
   return (m_OutputFileName);
}

/*********************************************************************************/

PARMS_OUTPUT_FILE_FORMAT PARMS_GetOutputFileFormat(void)
{
   return (m_OutputFileFormat);
}

/*********************************************************************************/

PARMS_INPUT_FILE_FORMAT PARMS_GetInputFileFormat(void)
{
   return (m_InputFileFormat);
}

/*********************************************************************************/

PARMS_SEQUENCE_TYPE PARMS_GetSequenceType(void)
{
   return (m_SequenceType);
}

/*********************************************************************************/

FLOAT PARMS_GetAAGapOpenPenalty(void)
{
   return (m_AAInDelGapStartCost);
}

/*********************************************************************************/

FLOAT PARMS_GetAAGapExtensionPenalty(void)
{
   return (m_AAInDelGapExtensionCost);
}

/*********************************************************************************/

FLOAT PARMS_GetAATailGapOpenPenalty(void)
{
   return (m_AAInDelTailGapStartCost);
}

/*********************************************************************************/

FLOAT PARMS_GetAATailGapExtensionPenalty(void)
{
   return (m_AAInDelTailGapExtensionCost);
}

/*********************************************************************************/

FLOAT PARMS_GetDNAGapOpenPenalty(void)
{
   return (m_DNAInDelGapStartCost);
}

/*********************************************************************************/

FLOAT PARMS_GetDNAGapExtensionPenalty(void)
{
   return (m_DNAInDelGapExtensionCost);
}

/*********************************************************************************/

FLOAT PARMS_GetDNATailGapOpenPenalty(void)
{
   return (m_DNAInDelTailGapStartCost);
}

/*********************************************************************************/

FLOAT PARMS_GetDNATailGapExtensionPenalty(void)
{
   return (m_DNAInDelTailGapExtensionCost);
}

/*********************************************************************************/

FLOAT PARMS_GetGapsInColumnThreshold(void)
{
   return (m_GapsInColumnThreshold);
}

/*********************************************************************************/

ULONG PARMS_GetGapAdjustmentWindow(void)
{
   return (m_GapAdjustmentWindow);
}

/*********************************************************************************/

PARMS_AA_SUBSTITUTION_MATRIX PARMS_GetAASubstitutionMatrix(void)
{
   return (m_AASubstitutionMatrix);
}

/*********************************************************************************/

BOOLEAN PARMS_GetUseMergedAlphabetForDistance(void)
{
   return (m_AAMerging);
}

/*********************************************************************************/

BOOLEAN PARMS_GetGenerateCompleteDistanceMatrix(void)
{
   return (m_GenerateCompleteDistanceMatrix);
}

/*********************************************************************************/

ULONG PARMS_GetMaxStorageSizePerTraceBackMatrix(void)
{
   return (m_MaxStorageSizePerTraceBackMatrix);
}

/*********************************************************************************/

BYTE *PARMS_GetGrammarPieceFileName(void)
{
   return (m_GrammarPieceFileName);
}

/*********************************************************************************/

BYTE *PARMS_GetOutputGrammarPieceFileName(void)
{
   return (m_OutputGrammarPieceFileName);
}

/*********************************************************************************/

BOOLEAN PARMS_GetUseGrammarPieceFile(void)
{
   return (m_UseGrammarPieceFile);
}

/*********************************************************************************/

FLOAT PARMS_GetGrammarPieceMismatchPenalty(void)
{
   return (m_GrammarPieceMismatchPenalty);
}

/*********************************************************************************/

FLOAT PARMS_GetGrammarPieceMatchBenefit(void)
{
   return (m_GrammarPieceMatchBenefit);
}

/*********************************************************************************/

FLOAT PARMS_GetGrammarSimilarityThreshold(void)
{
   return (m_GrammarSimilarityThreshold);
}

/*********************************************************************************/

FLOAT PARMS_GetSimilarAlignmentOverhangPercent(void)
{
   return (m_SimilarAlignmentOverhangPercent);
}

/*********************************************************************************/

FLOAT PARMS_GetDissimilarAlignmentOverhangPercent(void)
{
   return (m_DissimilarAlignmentOverhangPercent);
}

/*********************************************************************************/

void PARMS_SetSequenceType(PARMS_SEQUENCE_TYPE SequenceType)
{
   switch (SequenceType)
   {
      case PARMS_SEQUENCE_DNA:
         m_AAMerging = FALSE;
         m_SequenceType = SequenceType;
         break;
         
      case PARMS_SEQUENCE_AMINO_ACID:
         m_GrammarPieceMismatchPenalty = 0.0;
         m_GrammarPieceMatchBenefit = 0.0;
         m_UseGrammarPieceFile = FALSE;
         m_SequenceType = SequenceType;
         break;
      
      default:
         printf("Unknown sequence type.\n");
         exit(1);
   }
}

/*********************************************************************************/

BOOLEAN PARMS_ProcessCommandLine(INT argc, BYTE **argv)
{
   BYTE Option;
   BYTE OptionArgument[PARMS_MAX_ARGLENGTH];
   INT CurrentOption = PARMS_STARTING_ARGUMENT_POS;
   BOOLEAN OkToRun = TRUE;

   PARMS_InitializeDefaultValues();
   
   while ((Option = PARMS_GetNextOption(argc, argv, "c:e:E:f:F:g:G:i:m:o:p:s:S:t:T:v:V:w", OptionArgument, &CurrentOption)) != EOF)
   {
      switch(Option)
      {
         case 'f':
            if (strlen(OptionArgument) > 0)
            {
               switch(atoi(OptionArgument))
               {
                  case PARMS_OUTPUT_DISTANCE_MATRIX:
                  case PARMS_OUTPUT_PHYLIP:
                  case PARMS_OUTPUT_FASTA:
                  case PARMS_OUTPUT_MSF_GCG:
                  case PARMS_OUTPUT_CONSENSUS:
                  case PARMS_OUTPUT_CONSENSUS_IGNORE_GAPS:                  
                     m_OutputFileFormat = atoi(OptionArgument);
                     break;
                     
                  default:
                     printf("Unknown output file format.  Use -h for a list of options.\n\n");
                     OkToRun = FALSE;
                     break;
               }
            }
            else
            {
               printf("Unknown output file format.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;
               
         case 'F':
            if (strlen(OptionArgument) > 0)
            {
               switch(atoi(OptionArgument))
               {
                  case PARMS_INPUT_SEQUENCE_AUTO_DETECT:
                  case PARMS_INPUT_SEQUENCE_DNA_RNA:
                  case PARMS_INPUT_SEQUENCE_AMINO_ACID:
                     m_InputFileFormat = atoi(OptionArgument);
                     break;
                     
                  default:
                     printf("Unknown input file format.  Use -h for a list of options.\n\n");
                     OkToRun = FALSE;
                     break;
               }
            }
            else
            {
               printf("Unknown input file format.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;
               
         case 'h':
            PARMS_OutputHelpText();
            OkToRun = FALSE;
            break;

         case 'q':
            m_QuietMode = TRUE;
            break;
                        
         case 'i':
            if (strlen(OptionArgument) > 0)
            {
               sprintf(m_InputFileName, "%s", OptionArgument);
            }
            else
            {
               printf("No input file specified.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         case 'o':
            if (strlen(OptionArgument) > 0)
            {
               sprintf(m_OutputFileName, "%s", OptionArgument);
               sprintf(m_OutputGrammarPieceFileName, "%s.grammar_piece_output", OptionArgument);
            }
            else
            {
               printf("No output file specified.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         case 'g':
            if (strlen(OptionArgument) > 0)
            {
               m_AAInDelGapStartCost = atof(OptionArgument);
               m_DNAInDelGapStartCost = atof(OptionArgument);
            }
            else
            {
               printf("Unspecified option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;
               
         case 'e':
            if (strlen(OptionArgument) > 0)
            {
               m_AAInDelGapExtensionCost = atof(OptionArgument);
               m_DNAInDelGapExtensionCost = atof(OptionArgument);
            }
            else
            {
               printf("Unspecified option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;
               
         case 'G':
            if (strlen(OptionArgument) > 0)
            {
               m_AAInDelTailGapStartCost = atof(OptionArgument);
               m_DNAInDelTailGapStartCost = atof(OptionArgument);
            }
            else
            {
               printf("Unspecified option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;
               
         case 'E':
            if (strlen(OptionArgument) > 0)
            {
               m_AAInDelTailGapExtensionCost = atof(OptionArgument);
               m_DNAInDelTailGapExtensionCost = atof(OptionArgument);
            }
            else
            {
               printf("Unspecified option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         case 't':
            if (strlen(OptionArgument) > 0)
            {
               m_GapsInColumnThreshold = atof(OptionArgument);
               if ((m_GapsInColumnThreshold < 0.0) || (m_GapsInColumnThreshold > 1.0))
               {
                  printf("Threshold must be a value between 0.0 and 1.0.\n\n");
                  OkToRun = FALSE;
               }
            }
            else
            {
               printf("Unspecified option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         case 'w':
            if (strlen(OptionArgument) > 0)
            {
               m_GapAdjustmentWindow = atoi(OptionArgument);
            }
            else
            {
               printf("Unknown option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         case 'S':
            if (strlen(OptionArgument) > 0)
            {
               m_MaxStorageSizePerTraceBackMatrix = atoi(OptionArgument);
            }
            else
            {
               printf("Unknown option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;
            
         case 'm':
            if (strlen(OptionArgument) > 0)
            {
               switch(atoi(OptionArgument))
               {
                  case PARMS_AA_SUBSTITUTION_GONNET250:
                  case PARMS_AA_SUBSTITUTION_BLOSUM45:
                  case PARMS_AA_SUBSTITUTION_BLOSUM62:
                  case PARMS_AA_SUBSTITUTION_BLOSUM80:
                     m_AASubstitutionMatrix = atoi(OptionArgument);
                     break;
                     
                  default:
                     printf("Unknown amino acid substitution matrix.  Use -h for a list of options.\n\n");
                     OkToRun = FALSE;
                     break;
               }
            }
            else
            {
               printf("Unknown amino acid substitution matrix.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;
            
         case 'M':
            m_AAMerging = FALSE;
            break;

         case 'C':
            m_GenerateCompleteDistanceMatrix = TRUE;
            break;
            
         case 'p':
            if (strlen(OptionArgument) > 0)
            {
               sprintf(m_GrammarPieceFileName, "%s", OptionArgument);
               m_UseGrammarPieceFile = TRUE;
            }
            else
            {
               printf("No grammar piece file specified.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         case 'c':
            if (strlen(OptionArgument) > 0)
            {
               m_GrammarPieceMismatchPenalty = atof(OptionArgument);
            }
            else
            {
               printf("Unspecified option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         case 's':
            if (strlen(OptionArgument) > 0)
            {
               m_GrammarPieceMatchBenefit = atof(OptionArgument);
            }
            else
            {
               printf("Unspecified option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         case 'T':
            if (strlen(OptionArgument) > 0)
            {
               m_GrammarSimilarityThreshold = atof(OptionArgument);
               if (m_GrammarSimilarityThreshold < 0.0)
               {
                  printf("Threshold must be a positive amount.\n\n");
                  OkToRun = FALSE;
               }
            }
            else
            {
               printf("Unspecified option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         case 'v':
            if (strlen(OptionArgument) > 0)
            {
               m_SimilarAlignmentOverhangPercent = atof(OptionArgument);
               if ((m_SimilarAlignmentOverhangPercent < 0.01) || (m_SimilarAlignmentOverhangPercent > 1.0))
               {
                  printf("Percent must be a value between 0.01 and 1.0.\n\n");
                  OkToRun = FALSE;
               }
            }
            else
            {
               printf("Unspecified option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         case 'V':
            if (strlen(OptionArgument) > 0)
            {
               m_DissimilarAlignmentOverhangPercent = atof(OptionArgument);
               if ((m_DissimilarAlignmentOverhangPercent < 0.01) || (m_DissimilarAlignmentOverhangPercent > 1.0))
               {
                  printf("Percent must be a value between 0.01 and 1.0.\n\n");
                  OkToRun = FALSE;
               }
            }
            else
            {
               printf("Unspecified option value.  Use -h for a list of options.\n\n");
               OkToRun = FALSE;
            }
            break;

         default:
            printf("Unknown option.  Use -h for a list of options.\n\n");
            OkToRun = FALSE;
            break;
      }
   }

   if (OkToRun == TRUE)
   {
      PARMS_OutputHeaderText();
   }

   return (OkToRun);
}

/*********************************************************************************/

void PARMS_Printf(BYTE *String)
{
   if (m_QuietMode == FALSE)
   {
      fprintf(stderr, "%s", String);
   }
}

/*********************************************************************************/

void PARMS_PrintfProgress(ULONG Current, ULONG Total)
{
   if (m_QuietMode == FALSE)
   {
      fprintf(stderr, "Progress: %ld / %ld\r", Current, Total);
      if (Current >= Total)
      {
         fprintf(stderr, "\n");
      }
   }
}

/*********************************************************************************/

BYTE PARMS_GetNextOption(INT argc, 
                           BYTE **argv, 
                           BYTE *OptionDelimiters, 
                           BYTE *OptionArgument, 
                           INT *CurrentOptionIndex)
{
   BYTE ReturnOption = EOF;
   BOOLEAN Done;

   OptionArgument[0] = 0;

   if (argc >= PARMS_STARTING_ARGUMENT_POS)
   {
      if (*CurrentOptionIndex <= argc)
      {
         if (*CurrentOptionIndex < PARMS_STARTING_ARGUMENT_POS)
         {
            *CurrentOptionIndex = PARMS_STARTING_ARGUMENT_POS;
         }

         Done = FALSE;
         while (Done == FALSE)
         {
            if (*CurrentOptionIndex <= argc)
            {
               if (argv[*CurrentOptionIndex - 1][PARMS_DELIMITER_POS] != PARMS_DELIMITER_CHAR)
               {
                  *CurrentOptionIndex += 1;
               }
               else
               {
                  Done = TRUE;
               }
            }
            else
            {
               Done = TRUE;
            }
         }

         if (*CurrentOptionIndex <= argc)
         {
            ReturnOption = argv[*CurrentOptionIndex - 1][PARMS_OPTION_POS];
            *CurrentOptionIndex += 1;

            if (*CurrentOptionIndex <= argc)
            {
               if (argv[*CurrentOptionIndex - 1][PARMS_DELIMITER_POS] != PARMS_DELIMITER_CHAR)
               {
                  if (strchr(OptionDelimiters, ReturnOption) != NULL)
                  {
                     strcpy(OptionArgument, argv[*CurrentOptionIndex - 1]);
                     *CurrentOptionIndex += 1;
                  }
               }
            }
         }
      }
   }

   return (ReturnOption);
}

/*********************************************************************************/

void PARMS_InitializeDefaultValues(void)
{
   sprintf(m_InputFileName, "%s", PARMS_DEFAULT_INFILE);
   sprintf(m_OutputFileName, "%s", PARMS_DEFAULT_OUTFILE);
   m_OutputFileFormat = PARMS_DEFAULT_OUTFILE_FORMAT;
   m_InputFileFormat = PARMS_DEFAULT_INFILE_FORMAT;
   m_SequenceType = PARMS_SEQUENCE_DNA;
   m_AAInDelGapStartCost = PARMS_DEFAULT_AA_INDEL_GAP_START_COST;
   m_AAInDelTailGapStartCost = PARMS_DEFAULT_AA_INDEL_TAIL_GAP_START_COST;
   m_AAInDelGapExtensionCost = PARMS_DEFAULT_AA_INDEL_GAP_EXTENSION_COST;
   m_AAInDelTailGapExtensionCost = PARMS_DEFAULT_AA_INDEL_TAIL_GAP_EXTENSION_COST;
   m_DNAInDelGapStartCost = PARMS_DEFAULT_DNA_INDEL_GAP_START_COST;
   m_DNAInDelTailGapStartCost = PARMS_DEFAULT_DNA_INDEL_TAIL_GAP_START_COST;
   m_DNAInDelGapExtensionCost = PARMS_DEFAULT_DNA_INDEL_GAP_EXTENSION_COST;
   m_DNAInDelTailGapExtensionCost = PARMS_DEFAULT_DNA_INDEL_TAIL_GAP_EXTENSION_COST;
   m_GapsInColumnThreshold = PARMS_DEFAULT_GAPS_IN_COLUMN_THRESHOLD;
   m_GapAdjustmentWindow = PARMS_DEFAULT_GAP_ADJUSTMENT_WINDOW;
   m_AASubstitutionMatrix = PARMS_DEFAULT_AA_SUBSTITUTION_MATRIX;
   m_AAMerging = PARMS_DEFAULT_AA_MERGING;
   m_GenerateCompleteDistanceMatrix = PARMS_DEFAULT_GENERATE_COMPLETE_MATRIX;
   m_QuietMode = PARMS_DEFAULT_QUIET_MODE;
   m_MaxStorageSizePerTraceBackMatrix = PARMS_DEFAULT_MAX_TB_MATRIX_SIZE;
   m_UseGrammarPieceFile = PARMS_DEFAULT_USE_GRAMMAR_PIECE_FILE;
   sprintf(m_OutputGrammarPieceFileName, "%s.grammar_piece_output", PARMS_DEFAULT_OUTFILE);
   m_GrammarPieceMismatchPenalty = PARMS_DEFAULT_GRAMMAR_PIECE_MISMATCH_COST;
   m_GrammarPieceMatchBenefit = PARMS_DEFAULT_GRAMMAR_PIECE_MATCH_SCORE;
   m_GrammarSimilarityThreshold = PARMS_DEFAULT_GRAMMAR_SIMILARITY_THRESHOLD;
   m_SimilarAlignmentOverhangPercent = PARMS_DEFAULT_SIMILAR_ALIGN_OVERHANG_PERCENT;
   m_DissimilarAlignmentOverhangPercent = PARMS_DEFAULT_DISSIMILAR_ALIGN_OVERHANG_PERCENT;
}

/*********************************************************************************/

void PARMS_OutputHeaderText(void)
{
   if (m_QuietMode == FALSE)
   {
      printf("\n   GramAlign -- ");
      printf("Version %d.%d -- ", VERSION_MAJOR, VERSION_MINOR);
      printf("%s\n", VERSION_DATE);
      printf("   University of Nebraska-Lincoln\n");
      printf("   Department of Electrical Engineering\n");
      printf("   David Russell\n");
      printf("\n");
   }
}

/*********************************************************************************/

void PARMS_OutputHelpText(void)
{
   printf("\nUsage: GramAlign [options]\n\n");
   printf("General Options:\n\n");
   printf("\t-h \t\tHelp\n");
   printf("\t-q \t\tQuiet mode (default is verbose)\n");
   printf("\t-S value\tSpecify the maximum trace-back matrix before temporary \n\t\t\tpaging begins (default is %d million bytes)\n", PARMS_DEFAULT_MAX_TB_MATRIX_SIZE);

   printf("\nFile Options:\n\n");
   printf("\t-i filename\tSpecify input FASTA file (default is '%s')\n", PARMS_DEFAULT_INFILE);
   printf("\t-o filename\tSpecify output file (default is '%s')\n", PARMS_DEFAULT_OUTFILE);
   printf("\t-f value\tSpecify output file format (default is %d)\n", PARMS_DEFAULT_OUTFILE_FORMAT);
   printf("\t\t\tValues:\t%d = Grammar-based distance matrix\n", PARMS_OUTPUT_DISTANCE_MATRIX);
   printf("\t\t\t\t%d = PHYLIP\n", PARMS_OUTPUT_PHYLIP);
   printf("\t\t\t\t%d = Aligned FASTA\n", PARMS_OUTPUT_FASTA);
   printf("\t\t\t\t%d = MSF/GCG\n", PARMS_OUTPUT_MSF_GCG);
   printf("\t\t\t\t%d = Consensus sequence\n", PARMS_OUTPUT_CONSENSUS);
   printf("\t\t\t\t%d = Consensus sequence (ignore gaps)\n", PARMS_OUTPUT_CONSENSUS_IGNORE_GAPS);
   printf("\t-F value\tSpecify input file format (default is %d)\n", PARMS_DEFAULT_INFILE_FORMAT);
   printf("\t\t\tValues:\t%d = Try to automatically detect type\n", PARMS_INPUT_SEQUENCE_AUTO_DETECT);
   printf("\t\t\t\t%d = DNA/RNA\n", PARMS_INPUT_SEQUENCE_DNA_RNA);
   printf("\t\t\t\t%d = Amino Acid\n", PARMS_INPUT_SEQUENCE_AMINO_ACID);

   printf("\nDistance Matrix Options:\n\n");
   printf("\t-C \t\tGenerate complete distance matrix (default is partial)\n");
   printf("\t-M \t\tDisable the merged amino acid alphabet (default is enabled)\n");
   printf("\t\t\t\t(Ignored for DNA sequences)\n");
   printf("\t-T value\tSpecify grammar similarity threshold (default is %2.2f)\n", PARMS_DEFAULT_GRAMMAR_SIMILARITY_THRESHOLD);

   printf("\nAlignment Heuristic Options:\n\n");
   printf("\t-v value\tSpecify alignment overhang for similar sequences \n\t\t\t\t(default is %2.2f)\n", PARMS_DEFAULT_SIMILAR_ALIGN_OVERHANG_PERCENT);
   printf("\t-V value\tSpecify alignment overhang for dissimilar sequences \n\t\t\t\t(default is %2.2f)\n", PARMS_DEFAULT_DISSIMILAR_ALIGN_OVERHANG_PERCENT);

   printf("\nAlignment Scoring Options:\n\n");
   printf("\t-g value\tSpecify gap open cost (default is %2.2f)\n", PARMS_DEFAULT_AA_INDEL_GAP_START_COST);
   printf("\t\t\t\t(default is %2.2f for DNA sequences)\n", PARMS_DEFAULT_DNA_INDEL_GAP_START_COST);
   printf("\t-G value\tSpecify tail gap open cost (default is %2.2f)\n", PARMS_DEFAULT_AA_INDEL_TAIL_GAP_START_COST);
   printf("\t\t\t\t(default is %2.2f for DNA sequences)\n", PARMS_DEFAULT_DNA_INDEL_TAIL_GAP_START_COST);
   printf("\t-e value\tSpecify gap extension cost (default is %2.2f)\n", PARMS_DEFAULT_AA_INDEL_GAP_EXTENSION_COST);
   printf("\t\t\t\t(default is %2.2f for DNA sequences)\n", PARMS_DEFAULT_DNA_INDEL_GAP_EXTENSION_COST);
   printf("\t-E value\tSpecify tail gap extension cost (default is %2.2f)\n", PARMS_DEFAULT_AA_INDEL_TAIL_GAP_EXTENSION_COST);
   printf("\t\t\t\t(default is %2.2f for DNA sequences)\n", PARMS_DEFAULT_DNA_INDEL_TAIL_GAP_EXTENSION_COST);
   printf("\t-m value\tSpecify amino acid substitution matrix (default is %d)\n", PARMS_DEFAULT_AA_SUBSTITUTION_MATRIX);
   printf("\t\t\t\t(Ignored for DNA sequences)\n");
   printf("\t\t\tValues:\t%d = GONNET250\n", PARMS_AA_SUBSTITUTION_GONNET250);
   printf("\t\t\t\t%d = BLOSUM45\n", PARMS_AA_SUBSTITUTION_BLOSUM45);
   printf("\t\t\t\t%d = BLOSUM62\n", PARMS_AA_SUBSTITUTION_BLOSUM62);
   printf("\t\t\t\t%d = BLOSUM80\n", PARMS_AA_SUBSTITUTION_BLOSUM80);

   printf("\nAlignment Gap Filter Options:\n\n");
   printf("\t-t value\tSpecify percentage of gaps in a column for adjustment \n\t\t\t\t(default is %2.2f)\n", PARMS_DEFAULT_GAPS_IN_COLUMN_THRESHOLD);
   printf("\t-w value\tSpecify number of columns in gap-adjustment window \n\t\t\t\t(default is %d)\n", PARMS_DEFAULT_GAP_ADJUSTMENT_WINDOW);

   printf("\nSecondary Structure Options (Experimental):\n\n");
   printf("\t-p filename\tSpecify grammar piece file (default is disabled)\n");
   printf("\t\t\t\t(Ignored for amino acid sequences)\n");
   printf("\t-c value\tSpecify grammar piece mismatch cost \n\t\t\t\t(default is %2.2f)\n", PARMS_DEFAULT_GRAMMAR_PIECE_MISMATCH_COST);
   printf("\t\t\t\t(Ignored for amino acid sequences)\n");
   printf("\t-s value\tSpecify grammar piece match score \n\t\t\t\t(default is %2.2f)\n", PARMS_DEFAULT_GRAMMAR_PIECE_MATCH_SCORE);
   printf("\t\t\t\t(Ignored for amino acid sequences)\n");
   printf("\n\n");
}

