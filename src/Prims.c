/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _PRIMS

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "CoinFlip.h"
#include "SparseMatrixFloatADT.h"
#include "TreeADT.h"
#include "StringADT.h"
#include "StringListADT.h"
#include "Prims.h"
#include "Parameters.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/
static void PRIMS_InitializeArrays(ULONG N);
static void PRIMS_CleanUpArrays(void);
static ULONG PRIMS_FindRootOfSpanningTree(ULONG N, 
                                          STRINGLISTADT_STRINGLIST_TYPE *Sequences);
static ULONG PRIMS_LoadInitialValues(ULONG N, 
                                    ULONG RootIndex, 
                                    SMF_ADT_MATRIX_TYPE *DistanceMatrix);
static ULONG PRIMS_UpdateValues(ULONG Length, 
                                 ULONG NewestIndex, 
                                 SMF_ADT_MATRIX_TYPE *DistanceMatrix);

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/
static ULONG *m_NodesNotInTreeIndicies;
static FLOAT *m_NodesNotInTreeMinScores;
static ULONG *m_NodesNotInTreeMinScoreParentIndicies;

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void PRIMS_ConstructMinimalSpanningTree(SMF_ADT_MATRIX_TYPE *Matrix,
                                          TREEADT_TREE_TYPE *Tree,
                                          STRINGLISTADT_STRINGLIST_TYPE *Sequences)
{
   ULONG N;
   ULONG RootIndex;
   ULONG MinIndex;
   ULONG NodesNotInTree;
   TREEADT_ELEMENT_TYPE ParentLeaf;
   TREEADT_ELEMENT_TYPE MinimalLeaf;

   if (Matrix->NumRows != Matrix->NumCols)
   {
      printf("Error: matrix must be square and symmetric.\n");
      exit(1);
   }
   
   N = Matrix->NumRows;
   
   PRIMS_InitializeArrays(N);

   RootIndex = PRIMS_FindRootOfSpanningTree(N, Sequences);

   ParentLeaf.Key = RootIndex;
   TREEADT_CreateTree(Tree, ParentLeaf);

   MinIndex = PRIMS_LoadInitialValues(N, RootIndex, Matrix);
   NodesNotInTree = N-1;

   while (NodesNotInTree > 0)
   {
      /*
         Add latest minimum node to the tree.
      */
      ParentLeaf.Key = m_NodesNotInTreeMinScoreParentIndicies[MinIndex];
      MinimalLeaf.Key = m_NodesNotInTreeIndicies[MinIndex];
      TREEADT_AddNodeToTree(*Tree, MinimalLeaf, ParentLeaf);
      NodesNotInTree--;

      /*
         Remove minimum node from the three arrays.
      */
      memmove (&(m_NodesNotInTreeIndicies[MinIndex]), &(m_NodesNotInTreeIndicies[MinIndex + 1]), sizeof(ULONG) * (NodesNotInTree - MinIndex));
      memmove (&(m_NodesNotInTreeMinScores[MinIndex]), &(m_NodesNotInTreeMinScores[MinIndex + 1]), sizeof(FLOAT) * (NodesNotInTree - MinIndex));
      memmove (&(m_NodesNotInTreeMinScoreParentIndicies[MinIndex]), &(m_NodesNotInTreeMinScoreParentIndicies[MinIndex + 1]), sizeof(ULONG) * (NodesNotInTree - MinIndex));

      MinIndex = PRIMS_UpdateValues(NodesNotInTree, MinimalLeaf.Key, Matrix);

      PARMS_PrintfProgress((N - NodesNotInTree), N);
   }

   PRIMS_CleanUpArrays();
}

/*********************************************************************************/

void PRIMS_InitializeArrays(ULONG N)
{
   m_NodesNotInTreeIndicies = (ULONG *) malloc (sizeof(ULONG) * N);
   if (m_NodesNotInTreeIndicies == NULL)
   {
      printf("Error: malloc failed in PRIMS_InitializeArrays.\n");
      exit(1);
   }

   m_NodesNotInTreeMinScores = (FLOAT *) malloc (sizeof(FLOAT) * N);
   if (m_NodesNotInTreeMinScores == NULL)
   {
      printf("Error: malloc failed in PRIMS_InitializeArrays.\n");
      exit(1);
   }

   m_NodesNotInTreeMinScoreParentIndicies = (ULONG *) malloc (sizeof(ULONG) * N);
   if (m_NodesNotInTreeMinScoreParentIndicies == NULL)
   {
      printf("Error: malloc failed in PRIMS_InitializeArrays.\n");
      exit(1);
   }
}

/*********************************************************************************/

void PRIMS_CleanUpArrays(void)
{
   if (m_NodesNotInTreeIndicies != NULL)
   {
      free(m_NodesNotInTreeIndicies);
      m_NodesNotInTreeIndicies = NULL;
   }

   if (m_NodesNotInTreeMinScores != NULL)
   {
      free(m_NodesNotInTreeMinScores);
      m_NodesNotInTreeMinScores = NULL;
   }

   if (m_NodesNotInTreeMinScoreParentIndicies != NULL)
   {
      free(m_NodesNotInTreeMinScoreParentIndicies);
      m_NodesNotInTreeMinScoreParentIndicies = NULL;
   }
}

/*********************************************************************************/

ULONG PRIMS_FindRootOfSpanningTree(ULONG N, 
                                    STRINGLISTADT_STRINGLIST_TYPE *Sequences)
{
   ULONG i;
   ULONG MinRow = 0;

   /*
      Begin with one of the shortest-length nodes.
   */
   for (i = 1; i < N; i++)
   {
      if (Sequences->Strings[i].Length < Sequences->Strings[MinRow].Length)
      {
         MinRow = i;
      }
      else if (Sequences->Strings[i].Length == Sequences->Strings[MinRow].Length)
      {
         if (COIN_FLIP)
         {
            MinRow = i;
         }
      }
      PARMS_PrintfProgress(i, N);
   }
   PARMS_PrintfProgress(i, N);

   return (MinRow);
}

/*********************************************************************************/

ULONG PRIMS_LoadInitialValues(ULONG N, 
                              ULONG RootIndex, 
                              SMF_ADT_MATRIX_TYPE *DistanceMatrix)
{
   ULONG i;
   ULONG MinCol = 0;
   
   for (i = 0; i < (N-1); i++)
   {
      if (i < RootIndex)
      {
         m_NodesNotInTreeIndicies[i] = i;
         m_NodesNotInTreeMinScores[i] = SMF_ADT_GetElement(DistanceMatrix, RootIndex, i);
         m_NodesNotInTreeMinScoreParentIndicies[i] = RootIndex;         
      }
      else
      {
         m_NodesNotInTreeIndicies[i] = i+1;
         m_NodesNotInTreeMinScores[i] = SMF_ADT_GetElement(DistanceMatrix, RootIndex, i+1);
         m_NodesNotInTreeMinScoreParentIndicies[i] = RootIndex;         
      }
      
      if (m_NodesNotInTreeMinScores[i] < m_NodesNotInTreeMinScores[MinCol])
      {
         MinCol = i;
      }
      else if (m_NodesNotInTreeMinScores[i] == m_NodesNotInTreeMinScores[MinCol])
      {
         if (COIN_FLIP)
         {
            MinCol = i;
         }
      }
      PARMS_PrintfProgress(i, N-1);
   }
   PARMS_PrintfProgress(i, N-1);
   
   return (MinCol);
}

/*********************************************************************************/

ULONG PRIMS_UpdateValues(ULONG Length, 
                           ULONG NewestIndex, 
                           SMF_ADT_MATRIX_TYPE *DistanceMatrix)
{
   ULONG i;
   ULONG MinIndex = 0;
   FLOAT TempValue;
   
   for (i = 0; i < Length; i++)
   {
      TempValue = SMF_ADT_GetElement(DistanceMatrix, NewestIndex, m_NodesNotInTreeIndicies[i]);
      if (TempValue < m_NodesNotInTreeMinScores[i])
      {
         m_NodesNotInTreeMinScores[i] = TempValue;
         m_NodesNotInTreeMinScoreParentIndicies[i] = NewestIndex;
      }
      else if (TempValue == m_NodesNotInTreeMinScores[i])
      {
         if (COIN_FLIP)
         {
            m_NodesNotInTreeMinScores[i] = TempValue;
            m_NodesNotInTreeMinScoreParentIndicies[i] = NewestIndex;
         }
      }
      
      if (m_NodesNotInTreeMinScores[i] < m_NodesNotInTreeMinScores[MinIndex])
      {
         MinIndex = i;
      }
      else if (m_NodesNotInTreeMinScores[i] == m_NodesNotInTreeMinScores[MinIndex])
      {
         if (COIN_FLIP)
         {
            MinIndex = i;
         }
      }
   }
   
   return (MinIndex);
}
