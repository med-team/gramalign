/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _STRINGADT
#define STRINGADT_EXTERN
#else
#define STRINGADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef struct
{
   ULONG LengthAllocated;
   ULONG Length;
   UBYTE *Characters;
} STRINGADT_STRING_TYPE;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/

void STRINGADT_InitializeString(STRINGADT_STRING_TYPE *String);
void STRINGADT_CleanUpString(STRINGADT_STRING_TYPE *String);
void STRINGADT_AppendStringFromChar(STRINGADT_STRING_TYPE *Target, 
                                    UBYTE Source, 
                                    BYTE *CallingFunction);
void STRINGADT_AppendStringFromChars(STRINGADT_STRING_TYPE *Target, 
                                    UBYTE *Source,
                                    ULONG SourceLength, 
                                    BYTE *CallingFunction);
void STRINGADT_AppendStringFromString(STRINGADT_STRING_TYPE *Target, 
                                       STRINGADT_STRING_TYPE *Source, 
                                       BYTE *CallingFunction);
BOOLEAN STRINGADT_FindString(STRINGADT_STRING_TYPE *Target,
                              STRINGADT_STRING_TYPE *Source,
                              ULONG *FirstCharacterIndex);
