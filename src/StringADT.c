/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _STRINGADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "StringADT.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/
#define STRINGADT_CHUNK_SIZE  64

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void STRINGADT_InitializeString(STRINGADT_STRING_TYPE *String)
{
   String->LengthAllocated = 0;
   String->Length = 0;
   String->Characters = NULL;
}

/*********************************************************************************/

void STRINGADT_CleanUpString(STRINGADT_STRING_TYPE *String)
{
   if (String->Characters != NULL)
   {
      free(String->Characters);
   }
   
   String->LengthAllocated = 0;
   String->Length = 0;
   String->Characters = NULL;
}

/*********************************************************************************/

void STRINGADT_AppendStringFromChar(STRINGADT_STRING_TYPE *Target, 
                                    UBYTE Source, 
                                    BYTE *CallingFunction)
{
   if ((Target->Length + 1) >= (Target->LengthAllocated))
   {
      if (Target->LengthAllocated == 0)
      {
         Target->Characters = (UBYTE *) malloc(STRINGADT_CHUNK_SIZE);

         if (Target->Characters == NULL)
         {
            printf("Error: malloc failed in %s.\n", CallingFunction);
            exit(1);
         }
         else
         {
            Target->LengthAllocated = STRINGADT_CHUNK_SIZE;
         }
      }
      else
      {
         Target->Characters = (UBYTE *) realloc(Target->Characters, Target->LengthAllocated + STRINGADT_CHUNK_SIZE);

         if (Target->Characters == NULL)
         {
            printf("Error: realloc failed in %s.\n", CallingFunction);
            exit(1);
         }
         else
         {
            Target->LengthAllocated += STRINGADT_CHUNK_SIZE;
         }
      }
      memset(&(Target->Characters[Target->Length]), 0, Target->LengthAllocated - Target->Length);
   }
   
   Target->Characters[Target->Length] = Source;
   Target->Length += 1;
}

/*********************************************************************************/

void STRINGADT_AppendStringFromChars(STRINGADT_STRING_TYPE *Target, 
                                    UBYTE *Source,
                                    ULONG SourceLength, 
                                    BYTE *CallingFunction)
{
   if ((Target->Length + SourceLength) >= (Target->LengthAllocated))
   {
      if (Target->LengthAllocated == 0)
      {
         Target->Characters = (UBYTE *) malloc(SourceLength + STRINGADT_CHUNK_SIZE);

         if (Target->Characters == NULL)
         {
            printf("Error: malloc failed in %s.\n", CallingFunction);
            exit(1);
         }
         else
         {
            Target->LengthAllocated = SourceLength + STRINGADT_CHUNK_SIZE;
         }
      }
      else
      {
         Target->Characters = (UBYTE *) realloc(Target->Characters, Target->LengthAllocated + SourceLength + STRINGADT_CHUNK_SIZE);

         if (Target->Characters == NULL)
         {
            printf("Error: realloc failed in %s.\n", CallingFunction);
            exit(1);
         }
         else
         {
            Target->LengthAllocated += SourceLength + STRINGADT_CHUNK_SIZE;
         }
      }
      memset(&(Target->Characters[Target->Length]), 0, Target->LengthAllocated - Target->Length);
   }
   
   memcpy (&(Target->Characters[Target->Length]), Source, SourceLength);
   Target->Length += SourceLength;
}

/*********************************************************************************/

void STRINGADT_AppendStringFromString(STRINGADT_STRING_TYPE *Target, 
                                       STRINGADT_STRING_TYPE *Source, 
                                       BYTE *CallingFunction)
{
   STRINGADT_AppendStringFromChars(Target, 
                                    Source->Characters, 
                                    Source->Length, 
                                    CallingFunction);
}

/*********************************************************************************/

BOOLEAN STRINGADT_FindString(STRINGADT_STRING_TYPE *Target,
                              STRINGADT_STRING_TYPE *Source,
                              ULONG *FirstCharacterIndex)
{
   BOOLEAN Found = FALSE;
   BYTE *FirstOccurance;
   
   if ((Target->Length >= Source->Length) && (Source->Length > 0))
   {
      if (Source->Characters[0] != 0)
      {
         FirstOccurance = strstr((BYTE *) &(Target->Characters[*FirstCharacterIndex]), (BYTE *) Source->Characters);

         if (FirstOccurance != NULL)
         {
            Found = TRUE;
            *FirstCharacterIndex = ((ULONG) FirstOccurance) - ((ULONG) &(Target->Characters[0]));
         }
         else
         {
            *FirstCharacterIndex = 0;
         }
      }
   }
   
   return (Found);
}
