/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _MATRIXFLOATADT
#define MATRIXFLOATADT_EXTERN
#else
#define MATRIXFLOATADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef struct
{
   FLOAT *Cols;
} MATRIXFLOATADT_ROW_TYPE;

typedef struct
{
   ULONG NumRows;
   ULONG NumCols;
   MATRIXFLOATADT_ROW_TYPE *Rows;
} MATRIXFLOATADT_MATRIX_TYPE;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/

void MATRIXFLOATADT_InitializeMatrix(MATRIXFLOATADT_MATRIX_TYPE *Matrix);
void MATRIXFLOATADT_DefineMatrix(MATRIXFLOATADT_MATRIX_TYPE *Matrix, 
                                 ULONG Rows, 
                                 ULONG Cols, 
                                 FLOAT DefaultValue);
void MATRIXFLOATADT_CleanUpMatrix(MATRIXFLOATADT_MATRIX_TYPE *Matrix);
void MATRIXFLOATADT_SetElement(MATRIXFLOATADT_MATRIX_TYPE *Matrix, 
                           FLOAT Element, 
                           ULONG Row, 
                           ULONG Col);
FLOAT MATRIXFLOATADT_GetElement(MATRIXFLOATADT_MATRIX_TYPE *Matrix, 
                           ULONG Row, 
                           ULONG Col);
void MATRIXFLOATADT_PrintMatrix(MATRIXFLOATADT_MATRIX_TYPE *Matrix);
