/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _PARMS
#define PARMS_EXTERN
#else
#define PARMS_EXTERN extern
#endif

/*********************************************************************************/
/* Public Module Constants */
/*********************************************************************************/

/*********************************************************************************/
/* Public Type Definitions */
/*********************************************************************************/
typedef enum
{
   PARMS_OUTPUT_DISTANCE_MATRIX,
   PARMS_OUTPUT_PHYLIP,
   PARMS_OUTPUT_FASTA,
   PARMS_OUTPUT_MSF_GCG,
   PARMS_OUTPUT_CONSENSUS,
   PARMS_OUTPUT_CONSENSUS_IGNORE_GAPS
} PARMS_OUTPUT_FILE_FORMAT;

typedef enum
{
   PARMS_INPUT_SEQUENCE_AUTO_DETECT,
   PARMS_INPUT_SEQUENCE_DNA_RNA,
   PARMS_INPUT_SEQUENCE_AMINO_ACID,
} PARMS_INPUT_FILE_FORMAT;

typedef enum
{
   PARMS_SEQUENCE_DNA,
   PARMS_SEQUENCE_AMINO_ACID
} PARMS_SEQUENCE_TYPE;

typedef enum
{
   PARMS_AA_SUBSTITUTION_GONNET250,
   PARMS_AA_SUBSTITUTION_BLOSUM45,
   PARMS_AA_SUBSTITUTION_BLOSUM62,
   PARMS_AA_SUBSTITUTION_BLOSUM80
} PARMS_AA_SUBSTITUTION_MATRIX;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/
#ifdef _PARMS
/*
   A,S,T,X
   B,D,N
   C
   E,K,Q,R,Z
   F
   G
   H
   I,L,M,V
   P
   W
   Y
*/
PARMS_EXTERN const BYTE PARMS_MergedAlphabetLookup[ALPHABETIC_CHARACTERS] =
{
   'A',
   'B',
   'C',
   'B',
   'E',
   'F',
   'G',
   'H',
   'I',
   'A',  /* Unknown -- X is unknown A.A. */
   'E',
   'I',
   'I',
   'B',
   'A',  /* Unknown -- X is unknown A.A. */
   'P',
   'E',
   'E',
   'A',
   'A',
   'A',  /* Unknown -- X is unknown A.A. */
   'I',
   'W',
   'A',
   'Y',
   'E',
   'A',  /* Unknown -- X is unknown A.A. */
};
                           
#else
PARMS_EXTERN const BYTE PARMS_MergedAlphabetLookup[ALPHABETIC_CHARACTERS];
#endif

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/
BYTE *PARMS_GetInputFileName(void);
BYTE *PARMS_GetOutputFileName(void);
PARMS_OUTPUT_FILE_FORMAT PARMS_GetOutputFileFormat(void);
PARMS_INPUT_FILE_FORMAT PARMS_GetInputFileFormat(void);
PARMS_SEQUENCE_TYPE PARMS_GetSequenceType(void);
FLOAT PARMS_GetAAGapOpenPenalty(void);
FLOAT PARMS_GetAAGapExtensionPenalty(void);
FLOAT PARMS_GetAATailGapOpenPenalty(void);
FLOAT PARMS_GetAATailGapExtensionPenalty(void);
FLOAT PARMS_GetDNAGapOpenPenalty(void);
FLOAT PARMS_GetDNAGapExtensionPenalty(void);
FLOAT PARMS_GetDNATailGapOpenPenalty(void);
FLOAT PARMS_GetDNATailGapExtensionPenalty(void);
FLOAT PARMS_GetGapsInColumnThreshold(void);
ULONG PARMS_GetGapAdjustmentWindow(void);
PARMS_AA_SUBSTITUTION_MATRIX PARMS_GetAASubstitutionMatrix(void);
BOOLEAN PARMS_GetUseMergedAlphabetForDistance(void);
BOOLEAN PARMS_GetGenerateCompleteDistanceMatrix(void);
ULONG PARMS_GetMaxStorageSizePerTraceBackMatrix(void);
BYTE *PARMS_GetGrammarPieceFileName(void);
BYTE *PARMS_GetOutputGrammarPieceFileName(void);
BOOLEAN PARMS_GetUseGrammarPieceFile(void);
FLOAT PARMS_GetGrammarPieceMismatchPenalty(void);
FLOAT PARMS_GetGrammarPieceMatchBenefit(void);
FLOAT PARMS_GetGrammarSimilarityThreshold(void);
FLOAT PARMS_GetSimilarAlignmentOverhangPercent(void);
FLOAT PARMS_GetDissimilarAlignmentOverhangPercent(void);
void PARMS_SetSequenceType(PARMS_SEQUENCE_TYPE SequenceType);
BOOLEAN PARMS_ProcessCommandLine(INT argc, BYTE **argv);
void PARMS_Printf(BYTE *String);
void PARMS_PrintfProgress(ULONG Current, ULONG Total);
