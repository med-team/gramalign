/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _GROUPARRAYADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "GroupArrayADT.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void GROUPARRAYADT_InitializeGroupArray(GROUPARRAYADT_TYPE *GroupArray)
{
   GroupArray->Length = 0;
   GroupArray->GroupRuns = NULL;
   GroupArray->ElementLocation = NULL;
}

/*********************************************************************************/

void GROUPARRAYADT_DefineGroupArrayLength(GROUPARRAYADT_TYPE *GroupArray, ULONG Length)
{
   GroupArray->Length = Length;
   GroupArray->NextAvailableLocation = 0;
   GroupArray->GroupRuns = (GROUPARRAYADT_RUN_TYPE *) malloc (sizeof(GROUPARRAYADT_RUN_TYPE) * Length);
   if (GroupArray->GroupRuns == NULL)
   {
      printf("Error: malloc failed in GROUPARRAYADT_DefineGroupArrayLength.\n");
      exit(1);
   }

   GroupArray->ElementLocation = (ULONG *) malloc (sizeof(ULONG) * Length);
   if (GroupArray->ElementLocation == NULL)
   {
      printf("Error: malloc failed in GROUPARRAYADT_DefineGroupArrayLength.\n");
      exit(1);
   }
}

/*********************************************************************************/

void GROUPARRAYADT_CleanUpGroupArray(GROUPARRAYADT_TYPE *GroupArray)
{
   if (GroupArray->GroupRuns != NULL)
   {
      free(GroupArray->GroupRuns);
      GroupArray->GroupRuns = NULL;
   }

   if (GroupArray->ElementLocation != NULL)
   {
      free(GroupArray->ElementLocation);
      GroupArray->ElementLocation = NULL;
   }

   GroupArray->Length = 0;
}

/*********************************************************************************/

void GROUPARRAYADT_SetElementGroup(GROUPARRAYADT_TYPE *GroupArray,
                                   ULONG ElementId, 
                                   ULONG ParentId,
                                   BOOLEAN InAGroup)
{
   ULONG ElementLocation;
   ULONG GroupElementLocation;
   ULONG ParentLocation;
   ULONG GroupId;
   ULONG GroupSize;
   ULONG MovingElementId;
   ULONG i;

   if ((ElementId < GroupArray->Length) && (ParentId < GroupArray->Length))
   {
      if (InAGroup == FALSE)
      {
         ElementLocation = GroupArray->NextAvailableLocation;

         GroupId = ElementId;
         GroupSize = 1;
      }
      else
      {
         ParentLocation = GroupArray->ElementLocation[ParentId];
         GroupId = GroupArray->GroupRuns[ParentLocation].GroupId;
         GroupElementLocation = GroupArray->ElementLocation[GroupId];
         GroupSize = GroupArray->GroupRuns[GroupElementLocation].GroupSize;
         ElementLocation = GroupElementLocation + GroupSize;

         for (i = GroupArray->NextAvailableLocation; i > ElementLocation; i--)
         {
            GroupArray->GroupRuns[i] = GroupArray->GroupRuns[i - 1];
            MovingElementId = GroupArray->GroupRuns[i].ElementId;
            GroupArray->ElementLocation[MovingElementId] = i;
         }

         GroupArray->GroupRuns[GroupElementLocation].GroupSize += 1;

         GroupSize = 0;
      }

      GroupArray->GroupRuns[ElementLocation].GroupId = GroupId;
      GroupArray->GroupRuns[ElementLocation].ElementId = ElementId;
      GroupArray->GroupRuns[ElementLocation].ParentId = ParentId;
      GroupArray->GroupRuns[ElementLocation].GroupSize = GroupSize;
      GroupArray->ElementLocation[ElementId] = ElementLocation;

      GroupArray->NextAvailableLocation += 1;
   }
   else
   {
      printf("Error: either ElementId or ParentId is outside the length of the array.\n");
      GROUPARRAYADT_CleanUpGroupArray(GroupArray);
      exit(1);
   }
}

