/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _FILEIO

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "StringADT.h"
#include "StringListADT.h"
#include "MatrixUByteADT.h"
#include "MatrixFloatADT.h"
#include "SparseMatrixFloatADT.h"
#include "FileIO.h"
#include "Parameters.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/
#define FILEIO_SEQUENCE_PART_LENGTH                   512

#define FILEIO_PHYLIP_HEADER_INFORMATION_LENGTH       10
#define FILEIO_PHYLIP_BASEPAIRS_PER_LINE              50
#define FILEIO_PHYLIP_LOCATION_OF_SPACES              10

#define FILEIO_FASTA_BASEPAIRS_PER_LINE               60

#define FILEIO_CONSENSUS_BASEPAIRS_PER_LINE           120

#define FILEIO_MSF_GCG_HEADER_INFORMATION_LENGTH      10
#define FILEIO_MSF_GCG_BASEPAIRS_PER_LINE             50
#define FILEIO_MSF_GCG_LOCATION_OF_SPACES             10
#define FILEIO_MSF_GCG_WEIGHT                         1.0
#define FILEIO_GCG_MAXIMUM_LOOP_VALUE                 57
#define FILEIO_GCG_MAXIMUM_CHECKSUM_VALUE             10000

#define FILEIO_TB_PAGE_FILENAME_TEMPLATE              "_ga_temp.page%ld"
#define FILEIO_TB_PAGE_FILENAME_MAX_LENGTH            24
#define FILEIO_DELETE_TB_PAGE_TEMPLATE                "rm _ga_temp.page%ld"
#define FILEIO_DELETE_TB_PAGE_MAX_LENGTH              27

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/
typedef void (*FILEIO_POINTER_TO_STATE_FUNCTION) (BYTE);

typedef enum
{
   FILEIO_STATE_IDLE,
   FILEIO_STATE_HEADER_LINE,
   FILEIO_STATE_SEQUENCE,
} FILEIO_STATE;

typedef struct
{
   FILEIO_STATE State;
   FILEIO_POINTER_TO_STATE_FUNCTION StateFunction;
} FILEIO_STATE_ENTRY;

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/

/* States */
static void FILEIO_StateIdle (BYTE Ch);
static void FILEIO_StateHeaderLine(BYTE Ch);
static void FILEIO_StateSequence(BYTE Ch);

static void FILEIO_StateIdleGrammarPieces (BYTE Ch);
static void FILEIO_StateHeaderLineGrammarPieces(BYTE Ch);
static void FILEIO_StateSequenceGrammarPieces(BYTE Ch);

static void FILEIO_StoreSequencePart(void);
static void FILEIO_CreateNewSequence(void);
static void FILEIO_StoreLastSequencePart(void);
static void FILEIO_CreateNewHeaderLine(void);
static void FILEIO_StoreHeaderLine(void);
static void FILEIO_CreateNewGrammarPieceSequence(void);
static void FILEIO_StoreLastGrammarPieceSequencePart(void);
static void FILEIO_StoreGrammarPieceSequencePart(void);

static ULONG FILEIO_CalculateGCGChecksum(BYTE *Sequence, ULONG Length);

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/
static STRINGLISTADT_STRINGLIST_TYPE *m_Sequences;
static STRINGLISTADT_STRINGLIST_TYPE *m_MergedAlphabetSequences;
static BYTE m_SequencePart[FILEIO_SEQUENCE_PART_LENGTH];
static BYTE m_MergedAlphabetSequencePart[FILEIO_SEQUENCE_PART_LENGTH];
static USHORT m_CurrentSequenceIndex;
static STRINGADT_STRING_TYPE m_CurrentSequence;
static STRINGADT_STRING_TYPE m_CurrentMergedAlphabetSequence;
static STRINGLISTADT_STRINGLIST_TYPE *m_HeaderLines;
static STRINGADT_STRING_TYPE m_CurrentHeaderLine;
static PARMS_SEQUENCE_TYPE m_SequenceType;
static STRINGLISTADT_STRINGLIST_TYPE *m_GrammarPieces;
static UBYTE m_CurrentGrammarPieceLength;

static FILEIO_STATE m_State;

const static FILEIO_STATE_ENTRY m_StateTable[] =
{
   {FILEIO_STATE_IDLE, FILEIO_StateIdle},
   {FILEIO_STATE_HEADER_LINE, FILEIO_StateHeaderLine},
   {FILEIO_STATE_SEQUENCE, FILEIO_StateSequence},
};

const static FILEIO_STATE_ENTRY m_StateGrammarPiecesTable[] =
{
   {FILEIO_STATE_IDLE, FILEIO_StateIdleGrammarPieces},
   {FILEIO_STATE_HEADER_LINE, FILEIO_StateHeaderLineGrammarPieces},
   {FILEIO_STATE_SEQUENCE, FILEIO_StateSequenceGrammarPieces},
};

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void FILEIO_ReadFASTASequences(STRINGLISTADT_STRINGLIST_TYPE *Sequences,
                                 STRINGLISTADT_STRINGLIST_TYPE *MergedAlphabetSequences,
                                 STRINGLISTADT_STRINGLIST_TYPE *HeaderLines)
{
   FILE *infile;
   BYTE Ch;
   
   m_State = FILEIO_STATE_IDLE;
   m_CurrentSequenceIndex = 0;
   m_Sequences = Sequences;
   m_MergedAlphabetSequences = MergedAlphabetSequences;
   m_HeaderLines = HeaderLines;

   if (PARMS_GetInputFileFormat() == PARMS_INPUT_SEQUENCE_AMINO_ACID)
   {
      m_SequenceType = PARMS_SEQUENCE_AMINO_ACID;
   }
   else
   {
      m_SequenceType = PARMS_SEQUENCE_DNA;
   }

   infile = fopen(PARMS_GetInputFileName(), "r");
   if (infile != NULL)
   {
      do
      {
         Ch = getc(infile);
         m_StateTable[m_State].StateFunction(Ch);
      } while (Ch != EOF);

      fclose(infile);
   }
   else
   {
      printf("Cannot open %s.\n", PARMS_GetInputFileName());
      exit(1);
   }
   
   PARMS_SetSequenceType(m_SequenceType);
}

/*********************************************************************************/

void FILEIO_ReadFASTAGrammarPieces(STRINGLISTADT_STRINGLIST_TYPE *GrammarPieces,
                                    STRINGLISTADT_STRINGLIST_TYPE *HeaderLines)
{
   FILE *infile;
   BYTE Ch;
   
   m_State = FILEIO_STATE_IDLE;
   m_CurrentSequenceIndex = 0;
   m_GrammarPieces = GrammarPieces;
   m_HeaderLines = HeaderLines;

   infile = fopen(PARMS_GetGrammarPieceFileName(), "r");
   if (infile != NULL)
   {
      do
      {
         Ch = getc(infile);
         m_StateGrammarPiecesTable[m_State].StateFunction(Ch);
      } while (Ch != EOF);

      fclose(infile);
   }
   else
   {
      printf("Cannot open %s.\n", PARMS_GetGrammarPieceFileName());
      exit(1);
   }
}

/*********************************************************************************/

void FILEIO_WriteDistanceMatrix(SMF_ADT_MATRIX_TYPE *DistanceMatrix)
{
   FILE *outfile;
   ULONG i;
   ULONG j;
   ULONG N;
   
   if (DistanceMatrix->NumRows == DistanceMatrix->NumCols)
   {   
      N = DistanceMatrix->NumRows;

      outfile = fopen(PARMS_GetOutputFileName(), "w");
      if (outfile != NULL)
      {
         fprintf(outfile, "%ld\n", N);
         for (i = 0; i < N; i++)
         {
            fprintf(outfile, "Seq%ld\t", i);
            for (j = 0; j < N; j++)
            {
               fprintf(outfile, "%3.3f", SMF_ADT_GetElement(DistanceMatrix, i, j));
               if ((j + 1) >= N)
               {
                  fprintf(outfile, "\n");
               }
               else
               {
                  fprintf(outfile, "\t");
               }
            }
         }
         fclose(outfile);
      }
      else
      {
         printf("Cannot create %s.\n", PARMS_GetOutputFileName());
         exit(1);
      }
   }
   else
   {
      printf("Matrix must be square.\n");
      exit(1);
   }   
}

/*********************************************************************************/

void FILEIO_WriteAlignmentInPHYLIPFormat(STRINGLISTADT_STRINGLIST_TYPE *AlignedStrings,
                                          STRINGLISTADT_STRINGLIST_TYPE *HeaderLines,
                                          BYTE *OutputFileName)
{
   FILE *outfile;
   ULONG i;
   ULONG j;
   ULONG N;
   ULONG NumStrings;
   BOOLEAN Done = FALSE;
   ULONG StringIndex = 0;
   BOOLEAN PrintStringName = TRUE;
   
   NumStrings = AlignedStrings->NumberOfStrings;
   if (NumStrings != HeaderLines->NumberOfStrings)
   {
      printf("ERROR: missing the header information.\n");
      exit(1);
   }
   
   N = AlignedStrings->Strings[0].Length;
   
   for (i = 0; i < NumStrings; i++)
   {
      if (N != AlignedStrings->Strings[i].Length)
      {
         printf("ERROR: at least one string in the alignment has an incorrect length.\n");
         exit(1);
      }
   }
   
   outfile = fopen(OutputFileName, "w");
   if (outfile != NULL)
   {
      fprintf(outfile, "    %ld    %ld\n", NumStrings, N);
      while (Done == FALSE)
      {
         for (i = 0; i < NumStrings; i++)
         {
            for (j = 0; j < FILEIO_PHYLIP_HEADER_INFORMATION_LENGTH; j++)
            {
               if ((j < HeaderLines->Strings[i].Length) && (PrintStringName == TRUE))
               {
                  fprintf(outfile, "%c", HeaderLines->Strings[i].Characters[j]);
               }
               else
               {
                  fprintf(outfile, " ");
               }
            }
            fprintf(outfile, " ");
            
            for (j = 0; j < FILEIO_PHYLIP_BASEPAIRS_PER_LINE; j++)
            {
               if ((j + StringIndex) < N)
               {
                  if (((j % FILEIO_PHYLIP_LOCATION_OF_SPACES) == 0) && (j != 0))
                  {
                     fprintf(outfile, " ");
                  }
                  fprintf(outfile, "%c", AlignedStrings->Strings[i].Characters[j + StringIndex]);
                  if ((j + 1) >= FILEIO_PHYLIP_BASEPAIRS_PER_LINE)
                  {
                     fprintf(outfile, "\n");
                  }
               }
               else
               {
                  Done = TRUE;
                  j = FILEIO_PHYLIP_BASEPAIRS_PER_LINE;
                  fprintf(outfile, "\n");                  
               }
            }
         }
         PrintStringName = FALSE;
         StringIndex += FILEIO_PHYLIP_BASEPAIRS_PER_LINE;
         if (StringIndex >= N)
         {
            Done = TRUE;
         }
         fprintf(outfile, "\n");
      }
      fclose(outfile);
   }
   else
   {
      printf("Cannot create %s.\n", OutputFileName);
      exit(1);
   }
}

/*********************************************************************************/

void FILEIO_WriteAlignmentInFASTAFormat(STRINGLISTADT_STRINGLIST_TYPE *AlignedStrings,
                                          STRINGLISTADT_STRINGLIST_TYPE *HeaderLines,
                                          BYTE *OutputFileName)
{
   FILE *outfile;
   ULONG i;
   ULONG j;
   ULONG N;
   ULONG NumStrings;
   BOOLEAN LastCharacterWasAReturn = FALSE;
   
   NumStrings = AlignedStrings->NumberOfStrings;
   if (NumStrings != HeaderLines->NumberOfStrings)
   {
      printf("ERROR: missing the header information.\n");
      exit(1);
   }
   
   N = AlignedStrings->Strings[0].Length;
   
   for (i = 0; i < NumStrings; i++)
   {
      if (N != AlignedStrings->Strings[i].Length)
      {
         printf("ERROR: at least one string in the alignment has an incorrect length.\n");
         exit(1);
      }
   }
   
   outfile = fopen(OutputFileName, "w");
   if (outfile != NULL)
   {
      for (i = 0; i < NumStrings; i++)
      {
         fprintf(outfile, ">");
         for (j = 0; j < HeaderLines->Strings[i].Length; j++)
         {
            fprintf(outfile, "%c", HeaderLines->Strings[i].Characters[j]);
         }
         fprintf(outfile, "\n");
         
         for (j = 0; j < N; j++)
         {
            fprintf(outfile, "%c", AlignedStrings->Strings[i].Characters[j]);
            LastCharacterWasAReturn = FALSE;
            if (((j + 1) % FILEIO_PHYLIP_BASEPAIRS_PER_LINE) == 0)
            {
               fprintf(outfile, "\n");
               LastCharacterWasAReturn = TRUE;
            }
         }
         
         if (LastCharacterWasAReturn == FALSE)
         {
            fprintf(outfile, "\n");
         }
      }
      fclose(outfile);
   }
   else
   {
      printf("Cannot create %s.\n", OutputFileName);
      exit(1);
   }
}

/*********************************************************************************/

void FILEIO_WriteAlignmentInMSF_GCGFormat(STRINGLISTADT_STRINGLIST_TYPE *AlignedStrings,
                                          STRINGLISTADT_STRINGLIST_TYPE *HeaderLines,
                                          BYTE *OutputFileName)
{
   FILE *outfile;
   ULONG i;
   ULONG j;
   ULONG N;
   ULONG NumStrings;
   BOOLEAN Done = FALSE;
   ULONG StringIndex = 0;
   ULONG *SequenceChecksums;
   ULONG TotalChecksum;
   
   NumStrings = AlignedStrings->NumberOfStrings;
   if (NumStrings != HeaderLines->NumberOfStrings)
   {
      printf("ERROR: missing the header information.\n");
      exit(1);
   }
   
   N = AlignedStrings->Strings[0].Length;
   
   for (i = 0; i < NumStrings; i++)
   {
      if (N != AlignedStrings->Strings[i].Length)
      {
         printf("ERROR: at least one string in the alignment has an incorrect length.\n");
         exit(1);
      }
   }
   
   outfile = fopen(OutputFileName, "w");
   if (outfile != NULL)
   {
      SequenceChecksums = (ULONG *) malloc (sizeof(ULONG) * NumStrings);
      if (SequenceChecksums == NULL)
      {
         printf("Error: malloc failed in FILEIO_WriteAlignmentInMSF_GCGFormat.\n");
         exit(1);
      }
      
      TotalChecksum = 0;
      for (i = 0; i < NumStrings; i++)
      {
         SequenceChecksums[i] = FILEIO_CalculateGCGChecksum((BYTE *) AlignedStrings->Strings[i].Characters, N);
         TotalChecksum += SequenceChecksums[i];
      }
      TotalChecksum = TotalChecksum % FILEIO_GCG_MAXIMUM_CHECKSUM_VALUE;

      fprintf(outfile, "PileUp\n\n");
      fprintf(outfile, "\n\n   MSF:%5ld  Type: ", N);
      if(PARMS_GetSequenceType() == PARMS_SEQUENCE_DNA)
      {
         fprintf(outfile, "N");
      }
      else
      {
         fprintf(outfile, "P");
      }
      fprintf(outfile,"    Check:%6ld   .. \n\n", TotalChecksum);

      for(i = 0; i < NumStrings; i++)
      {
         fprintf(outfile, " Name: ");
         for (j = 0; j < FILEIO_MSF_GCG_HEADER_INFORMATION_LENGTH; j++)
         {
            if (j < HeaderLines->Strings[i].Length)
            {
               fprintf(outfile, "%c", HeaderLines->Strings[i].Characters[j]);
            }
            else
            {
               fprintf(outfile, " ");
            }
         }
         fprintf(outfile, " oo  Len:%5ld  Check:%6ld  Weight:  %.1f\n", N, SequenceChecksums[i], FILEIO_MSF_GCG_WEIGHT);
      }
      fprintf(outfile,"\n//\n");  

      while (Done == FALSE)
      {
         for (i = 0; i < NumStrings; i++)
         {
            for (j = 0; j < FILEIO_MSF_GCG_HEADER_INFORMATION_LENGTH; j++)
            {
               if (j < HeaderLines->Strings[i].Length)
               {
                  fprintf(outfile, "%c", HeaderLines->Strings[i].Characters[j]);
               }
               else
               {
                  fprintf(outfile, " ");
               }
            }
            fprintf(outfile, " ");
            
            for (j = 0; j < FILEIO_MSF_GCG_BASEPAIRS_PER_LINE; j++)
            {
               if ((j + StringIndex) < N)
               {
                  if (((j % FILEIO_MSF_GCG_LOCATION_OF_SPACES) == 0) && (j != 0))
                  {
                     fprintf(outfile, " ");
                  }
                  fprintf(outfile, "%c", AlignedStrings->Strings[i].Characters[j + StringIndex]);
                  if ((j + 1) >= FILEIO_MSF_GCG_BASEPAIRS_PER_LINE)
                  {
                     fprintf(outfile, "\n");
                  }
               }
               else
               {
                  Done = TRUE;
                  j = FILEIO_MSF_GCG_BASEPAIRS_PER_LINE;
                  fprintf(outfile, "\n");                  
               }
            }
         }
         StringIndex += FILEIO_MSF_GCG_BASEPAIRS_PER_LINE;
         if (StringIndex >= N)
         {
            Done = TRUE;
         }
         fprintf(outfile, "\n");
      }
      fclose(outfile);

      if (SequenceChecksums != NULL)
      {
         free(SequenceChecksums);
      }
   }
   else
   {
      printf("Cannot create %s.\n", OutputFileName);
      exit(1);
   }
}

/*********************************************************************************/

void FILEIO_WriteConsensusSequence(STRINGADT_STRING_TYPE *ConsensusString,
                                   FLOAT *ConsensusConfidence)
{
   FILE *outfile;
   ULONG j;
   BOOLEAN LastCharacterWasAReturn = FALSE;
   UBYTE Interval;
   
   outfile = fopen(PARMS_GetOutputFileName(), "w");
   if (outfile != NULL)
   {
      fprintf(outfile, "<html>\n");
      fprintf(outfile, "<head>\n");
      fprintf(outfile, "<title>GramAlign: Consensus Sequence</title>\n");
      fprintf(outfile, "<style type=\"text/css\">\n");
      fprintf(outfile, ".c00 {font-family: courier; font-size: 12px; background-color: #000000; color: #CCCCCC;}\n");
      fprintf(outfile, ".c01 {font-family: courier; font-size: 12px; background-color: #FF0000; color: #CCCCCC;}\n");
      fprintf(outfile, ".c10 {font-family: courier; font-size: 12px; background-color: #FF0099; color: #CCCCCC;}\n");
      fprintf(outfile, ".c20 {font-family: courier; font-size: 12px; background-color: #FF00FF; color: #CCCCCC;}\n");
      fprintf(outfile, ".c30 {font-family: courier; font-size: 12px; background-color: #0000FF; color: #CCCCCC;}\n");
      fprintf(outfile, ".c40 {font-family: courier; font-size: 12px; background-color: #0099FF; color: #333333;}\n");
      fprintf(outfile, ".c50 {font-family: courier; font-size: 12px; background-color: #00FFFF; color: #333333;}\n");
      fprintf(outfile, ".c60 {font-family: courier; font-size: 12px; background-color: #00FF00; color: #333333;}\n");
      fprintf(outfile, ".c70 {font-family: courier; font-size: 12px; background-color: #99FF00; color: #333333;}\n");
      fprintf(outfile, ".c80 {font-family: courier; font-size: 12px; background-color: #FFFF00; color: #333333;}\n");
      fprintf(outfile, ".c90 {font-family: courier; font-size: 12px; background-color: #FFFFFF; color: #333333;}\n");
      fprintf(outfile, "</style>\n");
      fprintf(outfile, "</head>\n");
      fprintf(outfile, "<body>\n");
      fprintf(outfile, "<br>\n");

      fprintf(outfile, "Legend (x is the confidence percentage of the column in the alignment):<br><br>\n");
      fprintf(outfile, "<span class=\"c00\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp x =&nbsp 0%% &nbsp</span><br>\n");
      fprintf(outfile, "<span class=\"c01\">&nbsp&nbsp 0%% &lt&nbsp x &lt 10%% &nbsp</span><br>\n");
      fprintf(outfile, "<span class=\"c10\">&nbsp 10%% &lt= x &lt 20%% &nbsp</span><br>\n");
      fprintf(outfile, "<span class=\"c20\">&nbsp 20%% &lt= x &lt 30%% &nbsp</span><br>\n");
      fprintf(outfile, "<span class=\"c30\">&nbsp 30%% &lt= x &lt 40%% &nbsp</span><br>\n");
      fprintf(outfile, "<span class=\"c40\">&nbsp 40%% &lt= x &lt 50%% &nbsp</span><br>\n");
      fprintf(outfile, "<span class=\"c50\">&nbsp 50%% &lt= x &lt 60%% &nbsp</span><br>\n");
      fprintf(outfile, "<span class=\"c60\">&nbsp 60%% &lt= x &lt 70%% &nbsp</span><br>\n");
      fprintf(outfile, "<span class=\"c70\">&nbsp 70%% &lt= x &lt 80%% &nbsp</span><br>\n");
      fprintf(outfile, "<span class=\"c80\">&nbsp 80%% &lt= x &lt 90%% &nbsp</span><br>\n");
      fprintf(outfile, "<span class=\"c90\">&nbsp 90%% &lt= x&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span><br><br>\n");

      fprintf(outfile, "Consensus sequence:<br><br>\n");
      for (j = 0; j < ConsensusString->Length; j++)
      {
         if (ConsensusConfidence[j] == 0)
         {
            Interval = 00;
         }
         else if (ConsensusConfidence[j] < 0.10)
         {
            Interval = 01;
         }
         else if (ConsensusConfidence[j] < 0.20)
         {
            Interval = 10;
         }
         else if (ConsensusConfidence[j] < 0.30)
         {
            Interval = 20;
         }
         else if (ConsensusConfidence[j] < 0.40)
         {
            Interval = 30;
         }
         else if (ConsensusConfidence[j] < 0.50)
         {
            Interval = 40;
         }
         else if (ConsensusConfidence[j] < 0.60)
         {
            Interval = 50;
         }
         else if (ConsensusConfidence[j] < 0.70)
         {
            Interval = 60;
         }
         else if (ConsensusConfidence[j] < 0.80)
         {
            Interval = 70;
         }
         else if (ConsensusConfidence[j] < 0.90)
         {
            Interval = 80;
         }
         else
         {
            Interval = 90;
         }
         
         fprintf(outfile, "<span class=\"c%2.2d\">%c</span>", Interval, ConsensusString->Characters[j]);

         LastCharacterWasAReturn = FALSE;
         if (((j + 1) % FILEIO_CONSENSUS_BASEPAIRS_PER_LINE) == 0)
         {
            fprintf(outfile, "<br>\n");
            LastCharacterWasAReturn = TRUE;
         }
      }
      
      if (LastCharacterWasAReturn == FALSE)
      {
         fprintf(outfile, "<br>\n");
      }

      fprintf(outfile, "</body>\n");
      fprintf(outfile, "</html>\n");
      fclose(outfile);
   }
   else
   {
      printf("Cannot create %s.\n", PARMS_GetOutputFileName());
      exit(1);
   }
}

/*********************************************************************************/

void FILEIO_WriteTraceBackMatrixPage(MATRIXUBYTEADT_MATRIX_TYPE *TraceBackMatrix, ULONG PageNumber)
{
   FILE *outfile;
   ULONG i;
   BYTE filename[FILEIO_TB_PAGE_FILENAME_MAX_LENGTH];
   ULONG NumBytes;
   
   sprintf(filename, FILEIO_TB_PAGE_FILENAME_TEMPLATE, PageNumber);

   outfile = fopen(filename, "wb");
   if (outfile != NULL)
   {
      NumBytes = fwrite(&(TraceBackMatrix->NumRows), sizeof(TraceBackMatrix->NumRows), 1, outfile);
      if (NumBytes != 1)
      {
         printf("Error writing to %s.\n", filename);
         exit(1);
      }
      else
      {
         NumBytes = fwrite(&(TraceBackMatrix->NumCols), sizeof(TraceBackMatrix->NumCols), 1, outfile);
         if (NumBytes != 1)
         {
            printf("Error writing to %s.\n", filename);
            exit(1);
         }
         else
         {         
            for (i = 0; i < TraceBackMatrix->NumRows; i++)
            {
               NumBytes = fwrite(&(TraceBackMatrix->Rows[i].Cols[0]), sizeof(UBYTE), TraceBackMatrix->NumCols, outfile);
               if (NumBytes != TraceBackMatrix->NumCols)
               {
                  printf("Error writing to %s.\n", filename);
                  exit(1);
               }
            }
            fclose(outfile);
         }
      }
   }
   else
   {
      printf("Cannot create %s.\n", filename);
      exit(1);
   }
}

/*********************************************************************************/

void FILEIO_ReadTraceBackMatrixPage(MATRIXUBYTEADT_MATRIX_TYPE *TraceBackMatrix, ULONG PageNumber)
{
   FILE *infile;
   ULONG i;
   BYTE filename[FILEIO_TB_PAGE_FILENAME_MAX_LENGTH];
   ULONG NumBytes;
   ULONG NumRows;
   ULONG NumCols;
   
   sprintf(filename, FILEIO_TB_PAGE_FILENAME_TEMPLATE, PageNumber);

   infile = fopen(filename, "rb");
   if (infile != NULL)
   {
      NumBytes = fread(&NumRows, sizeof(NumRows), 1, infile);
      if (NumBytes != 1)
      {
         printf("Error reading from %s.\n", filename);
         exit(1);
      }
      else
      {
         NumBytes = fread(&NumCols, sizeof(NumCols), 1, infile);
         if (NumBytes != 1)
         {
            printf("Error reading from %s.\n", filename);
            exit(1);
         }
         else
         {   
            if ((NumRows == TraceBackMatrix->NumRows) && (NumCols == TraceBackMatrix->NumCols))
            {
               for (i = 0; i < TraceBackMatrix->NumRows; i++)
               {
                  NumBytes = fread(&(TraceBackMatrix->Rows[i].Cols[0]), sizeof(UBYTE), TraceBackMatrix->NumCols, infile);
                  if (NumBytes != TraceBackMatrix->NumCols)
                  {
                     printf("Error reading from %s.\n", filename);
                     exit(1);
                  }
               }
               fclose(infile);
            }
            else
            {
               printf("TraceBack matrix must be allocated properly for paging.\n");
               exit(1);
            }
         }
      }
   }
   else
   {
      printf("Cannot open %s.\n", filename);
      exit(1);
   }
}

/*********************************************************************************/

void FILEIO_DeleteTraceBackMatrixPage(ULONG PageNumber)
{
   BYTE DeleteCommand[FILEIO_DELETE_TB_PAGE_MAX_LENGTH];

   sprintf(DeleteCommand, FILEIO_DELETE_TB_PAGE_TEMPLATE, PageNumber);

   system(DeleteCommand);
}

/*********************************************************************************/

void FILEIO_StateIdle(BYTE Ch)
{
   if (Ch == '>')
   {
      FILEIO_CreateNewHeaderLine();
      
      m_State = FILEIO_STATE_HEADER_LINE;
   }
}

/*********************************************************************************/

void FILEIO_StateHeaderLine(BYTE Ch)
{
   if ((Ch == '\n') || (Ch == '\r'))
   {
      FILEIO_StoreHeaderLine();
      FILEIO_CreateNewSequence();

      m_State = FILEIO_STATE_SEQUENCE;
   }
   else
   {
      if (Ch == ':')
      {
         STRINGADT_AppendStringFromChar(&m_CurrentHeaderLine, 
                                          '_', 
                                          "FILEIO_StateHeaderLine");
      }
      else
      {
         STRINGADT_AppendStringFromChar(&m_CurrentHeaderLine, 
                                          Ch, 
                                          "FILEIO_StateHeaderLine");
      }
   }
}

/*********************************************************************************/

void FILEIO_StateSequence(BYTE Ch)
{
   BYTE CH = toupper(Ch);
   
   switch(CH)
   {
      case '>':
         FILEIO_StoreLastSequencePart();
         FILEIO_CreateNewHeaderLine();
         m_State = FILEIO_STATE_HEADER_LINE;
         break;

      case 'A':
      case 'C':
      case 'G':
      case 'T':
      case 'U':
      case 'X':
         m_MergedAlphabetSequencePart[m_CurrentSequenceIndex] = PARMS_MergedAlphabetLookup[ALPHA_INDEX(CH)];
         m_SequencePart[m_CurrentSequenceIndex++] = CH;
         if (m_CurrentSequenceIndex >= FILEIO_SEQUENCE_PART_LENGTH)
         {
            FILEIO_StoreSequencePart();
         }
         break;

      case 'B':
      case 'D':
      case 'E':
      case 'F':
      case 'H':
      case 'I':
      case 'K':
      case 'L':
      case 'M':
      case 'N':
      case 'P':
      case 'Q':
      case 'R':
      case 'S':
      case 'V':
      case 'W':
      case 'Y':
      case 'Z':
         m_MergedAlphabetSequencePart[m_CurrentSequenceIndex] = PARMS_MergedAlphabetLookup[ALPHA_INDEX(CH)];
         m_SequencePart[m_CurrentSequenceIndex++] = CH;
         if (m_CurrentSequenceIndex >= FILEIO_SEQUENCE_PART_LENGTH)
         {
            FILEIO_StoreSequencePart();
         }

         if (PARMS_GetInputFileFormat() == PARMS_INPUT_SEQUENCE_AUTO_DETECT)
         {
            m_SequenceType = PARMS_SEQUENCE_AMINO_ACID;
         }
         break;

      case EOF:
         FILEIO_StoreLastSequencePart();
         break;

      default:
         break;
   }
}

/*********************************************************************************/

void FILEIO_StateIdleGrammarPieces(BYTE Ch)
{
   if (Ch == '>')
   {
      FILEIO_CreateNewHeaderLine();
      
      m_State = FILEIO_STATE_HEADER_LINE;
   }
}

/*********************************************************************************/

void FILEIO_StateHeaderLineGrammarPieces(BYTE Ch)
{
   if ((Ch == '\n') || (Ch == '\r'))
   {
      FILEIO_StoreHeaderLine();
      FILEIO_CreateNewGrammarPieceSequence();

      m_State = FILEIO_STATE_SEQUENCE;
   }
   else
   {
      if (Ch == ':')
      {
         STRINGADT_AppendStringFromChar(&m_CurrentHeaderLine, 
                                          '_', 
                                          "FILEIO_StateHeaderLine");
      }
      else
      {
         STRINGADT_AppendStringFromChar(&m_CurrentHeaderLine, 
                                          Ch, 
                                          "FILEIO_StateHeaderLine");
      }
   }
}

/*********************************************************************************/

void FILEIO_StateSequenceGrammarPieces(BYTE Ch)
{
   BYTE CH = toupper(Ch);
   
   switch(CH)
   {
      case '>':
         FILEIO_StoreLastGrammarPieceSequencePart();
         FILEIO_CreateNewHeaderLine();
         m_State = FILEIO_STATE_HEADER_LINE;
         break;
         
      case ',':
         m_SequencePart[m_CurrentSequenceIndex++] = m_CurrentGrammarPieceLength;
         m_CurrentGrammarPieceLength = 0;
         if (m_CurrentSequenceIndex >= FILEIO_SEQUENCE_PART_LENGTH)
         {
            FILEIO_StoreGrammarPieceSequencePart();
         }
         break;
         
      case '.':
         m_CurrentGrammarPieceLength = 0;
         break;

      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
         m_CurrentGrammarPieceLength = (10 * m_CurrentGrammarPieceLength) + (CH - '0');
         break;

      case EOF:
         FILEIO_StoreLastGrammarPieceSequencePart();
         break;

      default:
         break;
   }
}

/*********************************************************************************/

void FILEIO_StoreSequencePart(void)
{
   if (m_CurrentSequenceIndex > 0)
   {
      STRINGADT_AppendStringFromChars(&m_CurrentSequence, 
                                       (UBYTE *) m_SequencePart,
                                       m_CurrentSequenceIndex, 
                                       "FILEIO_StoreSequencePart");
      STRINGADT_AppendStringFromChars(&m_CurrentMergedAlphabetSequence, 
                                       (UBYTE *) m_MergedAlphabetSequencePart,
                                       m_CurrentSequenceIndex, 
                                       "FILEIO_StoreSequencePart");
      m_CurrentSequenceIndex = 0;
   }
}

/*********************************************************************************/

void FILEIO_CreateNewSequence(void)
{
   m_CurrentSequenceIndex = 0;
   STRINGADT_InitializeString(&m_CurrentSequence);
   STRINGADT_InitializeString(&m_CurrentMergedAlphabetSequence);
}

/*********************************************************************************/

void FILEIO_StoreLastSequencePart(void)
{
   FILEIO_StoreSequencePart();
   STRINGLISTADT_AppendStringToStringList(m_Sequences, 
                                          &m_CurrentSequence, 
                                          "FILEIO_StoreLastSequencePart");
   STRINGLISTADT_AppendStringToStringList(m_MergedAlphabetSequences, 
                                          &m_CurrentMergedAlphabetSequence, 
                                          "FILEIO_StoreLastSequencePart");
   STRINGADT_CleanUpString(&m_CurrentSequence);
   STRINGADT_CleanUpString(&m_CurrentMergedAlphabetSequence);
}

/*********************************************************************************/

void FILEIO_CreateNewHeaderLine(void)
{
   STRINGADT_InitializeString(&m_CurrentHeaderLine);
}

/*********************************************************************************/

void FILEIO_StoreHeaderLine(void)
{
   STRINGLISTADT_AppendStringToStringList(m_HeaderLines, 
                                          &m_CurrentHeaderLine, 
                                          "FILEIO_StoreHeaderLine");
   STRINGADT_CleanUpString(&m_CurrentHeaderLine);
}

/*********************************************************************************/

void FILEIO_CreateNewGrammarPieceSequence(void)
{
   m_CurrentSequenceIndex = 0;
   STRINGADT_InitializeString(&m_CurrentSequence);
   m_CurrentGrammarPieceLength = 0;
}

/*********************************************************************************/

void FILEIO_StoreLastGrammarPieceSequencePart(void)
{
   m_SequencePart[m_CurrentSequenceIndex++] = m_CurrentGrammarPieceLength;
   FILEIO_StoreGrammarPieceSequencePart();
   STRINGLISTADT_AppendStringToStringList(m_GrammarPieces, 
                                          &m_CurrentSequence, 
                                          "FILEIO_StoreLastGrammarPieceSequencePart");
   STRINGADT_CleanUpString(&m_CurrentSequence);
}

/*********************************************************************************/

void FILEIO_StoreGrammarPieceSequencePart(void)
{
   if (m_CurrentSequenceIndex > 0)
   {
      STRINGADT_AppendStringFromChars(&m_CurrentSequence, 
                                       (UBYTE *) m_SequencePart,
                                       m_CurrentSequenceIndex, 
                                       "FILEIO_StoreGrammarPieceSequencePart");
      m_CurrentSequenceIndex = 0;
   }
}

/*********************************************************************************/

ULONG FILEIO_CalculateGCGChecksum(BYTE *Sequence, ULONG Length)
{
   ULONG i;
   ULONG Checksum = 0;

   for(i = 0; i < Length; i++)
   {
      Checksum += ((i % FILEIO_GCG_MAXIMUM_LOOP_VALUE) + 1) * toupper(Sequence[i]);
   }

   Checksum = Checksum % FILEIO_GCG_MAXIMUM_CHECKSUM_VALUE;
   
   return (Checksum);
}
