/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _STRINGLISTADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "StringADT.h"
#include "StringListADT.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void STRINGLISTADT_InitializeStringList(STRINGLISTADT_STRINGLIST_TYPE *StringList)
{
   StringList->NumberOfStrings = 0;
   StringList->Strings = NULL;
}

/*********************************************************************************/

void STRINGLISTADT_CleanUpStringList(STRINGLISTADT_STRINGLIST_TYPE *StringList)
{
   ULONG i;

   for (i = 0; i < StringList->NumberOfStrings; i++)
   {
      STRINGADT_CleanUpString(&(StringList->Strings[i]));
   }

   StringList->NumberOfStrings = 0;
   StringList->Strings = NULL;
}

/*********************************************************************************/

void STRINGLISTADT_AppendStringToStringList(STRINGLISTADT_STRINGLIST_TYPE *StringList, 
                                             STRINGADT_STRING_TYPE *String, 
                                             BYTE *CallingFunction)
{
   if (StringList->NumberOfStrings == 0)
   {
      StringList->Strings = (STRINGADT_STRING_TYPE *) malloc(sizeof(STRINGADT_STRING_TYPE));
      if (StringList->Strings == NULL)
      {
         printf("Error: malloc failed in %s.\n", CallingFunction);
         exit(1);
      }
   }
   else
   {
      StringList->Strings = (STRINGADT_STRING_TYPE *) realloc(StringList->Strings, sizeof(STRINGADT_STRING_TYPE) * (StringList->NumberOfStrings + 1));
      if (StringList->Strings == NULL)
      {
         printf("Error: realloc failed in %s.\n", CallingFunction);
         exit(1);
      }
   }

   StringList->NumberOfStrings += 1;
   STRINGADT_InitializeString(&(StringList->Strings[StringList->NumberOfStrings - 1]));
   STRINGADT_AppendStringFromString(&(StringList->Strings[StringList->NumberOfStrings - 1]), String, CallingFunction);
}
