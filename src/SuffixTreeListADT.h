/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _STLADT
#define STLADT_EXTERN
#else
#define STLADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef struct
{
   ULONG NumberOfTreesAllocated;
   ULONG NumberOfTrees;
   STADT_TREE_TYPE **Trees;
} STLADT_TREELIST_TYPE;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/

void STLADT_InitializeTreeList(STLADT_TREELIST_TYPE *List);
void STLADT_CleanUpTreeList(STLADT_TREELIST_TYPE *List);
void STLADT_AppendStringToTreeList(STLADT_TREELIST_TYPE *List, 
                                    STRINGADT_STRING_TYPE *String);
