/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _MATRIXFLOATADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "MatrixFloatADT.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void MATRIXFLOATADT_InitializeMatrix(MATRIXFLOATADT_MATRIX_TYPE *Matrix)
{
   Matrix->NumRows = 0;
   Matrix->NumCols = 0;
   Matrix->Rows = NULL;
}

/*********************************************************************************/

void MATRIXFLOATADT_DefineMatrix(MATRIXFLOATADT_MATRIX_TYPE *Matrix, 
                                 ULONG Rows, 
                                 ULONG Cols, 
                                 FLOAT DefaultValue)
{
   ULONG i;
   ULONG j;
   
   Matrix->NumRows = Rows;
   Matrix->NumCols = Cols;
   Matrix->Rows = (MATRIXFLOATADT_ROW_TYPE *) malloc (sizeof(MATRIXFLOATADT_ROW_TYPE) * Rows);
   if (Matrix->Rows == NULL)
   {
      printf("Error: malloc failed in MATRIXFLOATADT_InitializeMatrix.\n");
      exit(1);
   }
   else
   {
      for (i = 0; i < Rows; i++)
      {
         Matrix->Rows[i].Cols = (FLOAT *) malloc (sizeof(FLOAT) * Cols);
         if (Matrix->Rows[i].Cols == NULL)
         {
            printf("Error: malloc failed in MATRIXFLOATADT_InitializeMatrix.\n");
            exit(1);
         }
         else
         {
            for (j = 0; j < Cols; j++)
            {
               Matrix->Rows[i].Cols[j] = DefaultValue;
            }
         }
      }
   }
}

/*********************************************************************************/

void MATRIXFLOATADT_CleanUpMatrix(MATRIXFLOATADT_MATRIX_TYPE *Matrix)
{
   ULONG i;

   for (i = 0; i < Matrix->NumRows; i++)
   {
      if (Matrix->Rows[i].Cols != NULL)
      {
         free(Matrix->Rows[i].Cols);
         Matrix->Rows[i].Cols = NULL;
      }
   }

   if (Matrix->Rows != NULL)
   {
      free(Matrix->Rows);
      Matrix->Rows = NULL;
   }

   Matrix->NumRows = 0;
   Matrix->NumCols = 0;
}

/*********************************************************************************/

void MATRIXFLOATADT_SetElement(MATRIXFLOATADT_MATRIX_TYPE *Matrix, 
                           FLOAT Element, 
                           ULONG Row, 
                           ULONG Col)
{
   if ((Row < Matrix->NumRows) && (Col < Matrix->NumCols))
   {
      Matrix->Rows[Row].Cols[Col] = Element;
   }
   else
   {
      printf("Error: either Row or Col is outside the dimension of the matrix.\n");
      MATRIXFLOATADT_CleanUpMatrix(Matrix);
      exit(1);
   }
}

/*********************************************************************************/

FLOAT MATRIXFLOATADT_GetElement(MATRIXFLOATADT_MATRIX_TYPE *Matrix, 
                           ULONG Row, 
                           ULONG Col)
{
   FLOAT Element;
   
   if ((Row < Matrix->NumRows) && (Col < Matrix->NumCols))
   {
      Element = Matrix->Rows[Row].Cols[Col];
   }
   else
   {
      printf("Error: either Row or Col is outside the dimension of the matrix.\n");
      MATRIXFLOATADT_CleanUpMatrix(Matrix);
      exit(1);
   }
   
   return (Element);
}
                           
/*********************************************************************************/

void MATRIXFLOATADT_PrintMatrix(MATRIXFLOATADT_MATRIX_TYPE *Matrix)
{
   ULONG i;
   ULONG j;
   
   for (i = 0; i < Matrix->NumRows; i++)
   {
      for (j = 0; j < Matrix->NumCols; j++)
      {
         printf("%2.2f\t", MATRIXFLOATADT_GetElement(Matrix, i, j));
      }
      printf("\n");
   }
}
