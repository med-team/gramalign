/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _MATRIXUBYTEADT
#define MATRIXUBYTEADT_EXTERN
#else
#define MATRIXUBYTEADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef struct
{
   UBYTE *Cols;
} MATRIXUBYTEADT_ROW_TYPE;

typedef struct
{
   ULONG NumRows;
   ULONG NumCols;
   MATRIXUBYTEADT_ROW_TYPE *Rows;
} MATRIXUBYTEADT_MATRIX_TYPE;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/

void MATRIXUBYTEADT_InitializeMatrix(MATRIXUBYTEADT_MATRIX_TYPE *Matrix);
void MATRIXUBYTEADT_DefineMatrix(MATRIXUBYTEADT_MATRIX_TYPE *Matrix, ULONG Rows, ULONG Cols);
void MATRIXUBYTEADT_CleanUpMatrix(MATRIXUBYTEADT_MATRIX_TYPE *Matrix);
void MATRIXUBYTEADT_SetElement(MATRIXUBYTEADT_MATRIX_TYPE *Matrix, 
                           UBYTE Element, 
                           ULONG Row, 
                           ULONG Col);
UBYTE MATRIXUBYTEADT_GetElement(MATRIXUBYTEADT_MATRIX_TYPE *Matrix, 
                           ULONG Row, 
                           ULONG Col);
void MATRIXUBYTEADT_PrintMatrix(MATRIXUBYTEADT_MATRIX_TYPE *Matrix);
