/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _SMF_ADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "SparseMatrixFloatADT.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/
#define SMF_ADT_CHUNK_SIZE    10

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/
static BOOLEAN SMF_ADT_FindElement(SMF_ADT_MATRIX_TYPE *Matrix, 
                                    ULONG Row, 
                                    ULONG Col,
                                    ULONG *Index);

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void SMF_ADT_InitializeMatrix(SMF_ADT_MATRIX_TYPE *Matrix)
{
   Matrix->NumRows = 0;
   Matrix->NumCols = 0;
   Matrix->DefaultValue = 0.0;
   Matrix->Rows = NULL;
}

/*********************************************************************************/

void SMF_ADT_DefineMatrix(SMF_ADT_MATRIX_TYPE *Matrix, 
                           ULONG Rows, 
                           ULONG Cols, 
                           FLOAT DefaultValue)
{
   ULONG i;
   
   Matrix->NumRows = Rows;
   Matrix->NumCols = Cols;
   Matrix->DefaultValue = DefaultValue;

   Matrix->Rows = (SMF_ADT_ROW_TYPE *) malloc (sizeof(SMF_ADT_ROW_TYPE) * Rows);
   if (Matrix->Rows == NULL)
   {
      printf("Error: malloc failed in SMF_ADT_DefineMatrix.\n");
      exit(1);
   }
   else
   {
      for (i = 0; i < Rows; i++)
      {
         Matrix->Rows[i].NumColsAllocated = 0;
         Matrix->Rows[i].NumCols = 0;
         Matrix->Rows[i].Cols = NULL;
      }
   }
}

/*********************************************************************************/

void SMF_ADT_CleanUpMatrix(SMF_ADT_MATRIX_TYPE *Matrix)
{
   ULONG i;

   for (i = 0; i < Matrix->NumRows; i++)
   {
      if (Matrix->Rows[i].Cols != NULL)
      {
         free(Matrix->Rows[i].Cols);
         Matrix->Rows[i].Cols = NULL;
      }
   }

   if (Matrix->Rows != NULL)
   {
      free(Matrix->Rows);
      Matrix->Rows = NULL;
   }

   Matrix->NumRows = 0;
   Matrix->NumCols = 0;
}

/*********************************************************************************/

void SMF_ADT_SetElement(SMF_ADT_MATRIX_TYPE *Matrix, 
                        FLOAT Element, 
                        ULONG Row, 
                        ULONG Col)
{
   ULONG Index;

   if (SMF_ADT_FindElement(Matrix, Row, Col, &Index) == TRUE)
   {
      Matrix->Rows[Row].Cols[Index].Value = Element;
   }
   else
   {
      if ((Matrix->Rows[Row].NumCols + 1) >= (Matrix->Rows[Row].NumColsAllocated))
      {
         if (Matrix->Rows[Row].NumColsAllocated == 0)
         {
            Matrix->Rows[Row].Cols = (SMF_ADT_CELL_TYPE *) malloc(sizeof (SMF_ADT_CELL_TYPE) * SMF_ADT_CHUNK_SIZE);
   
            if (Matrix->Rows[Row].Cols == NULL)
            {
               printf("Error: malloc failed in SMF_ADT_SetElement.\n");
               exit(1);
            }
            else
            {
               Matrix->Rows[Row].NumColsAllocated = SMF_ADT_CHUNK_SIZE;
            }
         }
         else
         {
            Matrix->Rows[Row].Cols = (SMF_ADT_CELL_TYPE *) realloc(Matrix->Rows[Row].Cols, sizeof (SMF_ADT_CELL_TYPE) * (Matrix->Rows[Row].NumColsAllocated + SMF_ADT_CHUNK_SIZE));
   
            if (Matrix->Rows[Row].Cols == NULL)
            {
               printf("Error: realloc failed in SMF_ADT_SetElement.\n");
               exit(1);
            }
            else
            {
               Matrix->Rows[Row].NumColsAllocated += SMF_ADT_CHUNK_SIZE;
            }
         }
      }

      if (Index < Matrix->Rows[Row].NumCols)
      {
         memmove (&(Matrix->Rows[Row].Cols[Index + 1]), &(Matrix->Rows[Row].Cols[Index]), sizeof (SMF_ADT_CELL_TYPE) * (Matrix->Rows[Row].NumColsAllocated - Index - 1));
      }

      Matrix->Rows[Row].Cols[Index].ColumnIndex = Col;
      Matrix->Rows[Row].Cols[Index].Value = Element;
      Matrix->Rows[Row].NumCols += 1;
   }
}

/*********************************************************************************/

FLOAT SMF_ADT_GetElement(SMF_ADT_MATRIX_TYPE *Matrix, 
                           ULONG Row, 
                           ULONG Col)
{
   FLOAT Element = Matrix->DefaultValue;
   ULONG Index;

   if (SMF_ADT_FindElement(Matrix, Row, Col, &Index) == TRUE)
   {
      Element = Matrix->Rows[Row].Cols[Index].Value;
   }
   
   return (Element);
}
                           
/*********************************************************************************/

void SMF_ADT_PrintMatrix(SMF_ADT_MATRIX_TYPE *Matrix)
{
   ULONG i;
   ULONG j;
   
   for (i = 0; i < Matrix->NumRows; i++)
   {
      for (j = 0; j < Matrix->NumCols; j++)
      {
         printf("%2.2f\t", SMF_ADT_GetElement(Matrix, i, j));
      }
      printf("\n");
   }
}

/*********************************************************************************/

BOOLEAN SMF_ADT_FindElement(SMF_ADT_MATRIX_TYPE *Matrix, 
                              ULONG Row, 
                              ULONG Col,
                              ULONG *Index)
{
   ULONG i;
   ULONG Start;
   ULONG End;
   ULONG MidPoint;
   BOOLEAN Found = FALSE;
   
   if ((Row < Matrix->NumRows) && (Col < Matrix->NumCols))
   {
      if (Matrix->Rows[Row].NumCols > 0)
      {
         Start = 0;
         End = Matrix->Rows[Row].NumCols - 1;
         MidPoint = Start + End / 2;
         
         while ((Found == FALSE) && (MidPoint > Start) && (MidPoint < End))
         {
            if (Matrix->Rows[Row].Cols[MidPoint].ColumnIndex == Col)
            {
               *Index = MidPoint;
               Found = TRUE;
            }
            else if (Matrix->Rows[Row].Cols[MidPoint].ColumnIndex < Col)
            {
               Start = MidPoint;
               MidPoint = Start + End / 2;
            }
            else
            {
               End = MidPoint;
               MidPoint = Start + End / 2;
            }
         }
         
         if (Found == FALSE)
         {
            *Index = Start;
            for (i = Start; (i <= End) && (Found == FALSE); i++)
            {
               if (Matrix->Rows[Row].Cols[i].ColumnIndex == Col)
               {
                  Found = TRUE;
               }
               else if (Matrix->Rows[Row].Cols[i].ColumnIndex < Col)
               {
                  *Index += 1;
               }
            }
         }
      }
      else
      {
         *Index = 0;
      }
   }
   else
   {
      printf("Error: either Row or Col is outside the dimension of the matrix.\n");
      SMF_ADT_CleanUpMatrix(Matrix);
      exit(1);
   }
   
   return (Found);
}

