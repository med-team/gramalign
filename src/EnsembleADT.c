/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _ENSEMBLEADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "StringADT.h"
#include "StringListADT.h"
#include "GroupArrayADT.h"
#include "EnsembleADT.h"
#include "Parameters.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

#define ENSEMBLEADT_GAP_RESIDUE              '-'
#define ENSEMBLEADT_TAIL_GAP_RESIDUE         '.'
#define ENSEMBLEADT_GAP_OPEN_RESIDUE         '*'
#define ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE    '!'

#define ENSEMBLEADT_GONNET250_SCALE          10

#define ENSEMBLEADT_MAX_GRAMMAR_PIECE_SIZE   255

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

typedef enum
{
   ENSEMBLEADT_DNA_A = 0,
   ENSEMBLEADT_DNA_C,
   ENSEMBLEADT_DNA_G,
   ENSEMBLEADT_DNA_T,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_MAX
} ENSEMBLEADT_DNA_INDEX;

typedef enum
{
   ENSEMBLEADT_AA_A = 0,
   ENSEMBLEADT_AA_B,
   ENSEMBLEADT_AA_C,
   ENSEMBLEADT_AA_D,
   ENSEMBLEADT_AA_E,
   ENSEMBLEADT_AA_F,
   ENSEMBLEADT_AA_G,
   ENSEMBLEADT_AA_H,
   ENSEMBLEADT_AA_I,
   ENSEMBLEADT_AA_K,
   ENSEMBLEADT_AA_L,
   ENSEMBLEADT_AA_M,
   ENSEMBLEADT_AA_N,
   ENSEMBLEADT_AA_P,
   ENSEMBLEADT_AA_Q,
   ENSEMBLEADT_AA_R,
   ENSEMBLEADT_AA_S,
   ENSEMBLEADT_AA_T,
   ENSEMBLEADT_AA_V,
   ENSEMBLEADT_AA_W,
   ENSEMBLEADT_AA_X,
   ENSEMBLEADT_AA_Y,
   ENSEMBLEADT_AA_Z,
   ENSEMBLEADT_AA_UNKNOWN,
   ENSEMBLEADT_AA_MAX
} ENSEMBLEADT_AA_INDEX;

typedef struct
{
   FLOAT Cells[ENSEMBLEADT_DNA_MAX][ENSEMBLEADT_DNA_MAX];
} ENSEMBLEADT_DNA_MATRIX_TYPE;

typedef struct
{
   FLOAT Cells[ENSEMBLEADT_AA_MAX][ENSEMBLEADT_AA_MAX];
} ENSEMBLEADT_AA_MATRIX_TYPE;

typedef void (* ENSEMBLEADT_UPDATE_ROW_SCORE_FUNCTION)(ENSEMBLEADT_TYPE *Ensemble, 
                                                       BYTE Residue, 
                                                       FLOAT *SubCostRow, 
                                                       ULONG NumberAddedInRound);

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/
static BOOLEAN ENSEMBLEADT_ScanColumnForResidues(ENSEMBLEADT_TYPE *Ensemble,
                                                 ENSEMBLEADT_NODE_TYPE **CurrentNode, 
                                                 ULONG CurrentColumn);
static BOOLEAN ENSEMBLEADT_FindNearbyColumnWithGapInRow(ENSEMBLEADT_TYPE *Ensemble,
                                                        ENSEMBLEADT_NODE_TYPE *BaseNode,
                                                        ULONG BaseColumn,
                                                        ULONG Row, 
                                                        ENSEMBLEADT_NODE_TYPE **ColumnWithGap,
                                                        BOOLEAN *Forward);
static void ENSEMBLEADT_MoveGapFromOneColumnToAnother(ENSEMBLEADT_NODE_TYPE *BaseNode,
                                                      ULONG Row, 
                                                      ENSEMBLEADT_NODE_TYPE *ColumnWithGap,
                                                      BOOLEAN Forward);
static void ENSEMBLEADT_UpdateDNARowScore(ENSEMBLEADT_TYPE *Ensemble, 
                                          BYTE Residue, 
                                          FLOAT *SubCostRow, 
                                          ULONG NumberAddedInRound);
static void ENSEMBLEADT_UpdateAARowScore(ENSEMBLEADT_TYPE *Ensemble, 
                                         BYTE Residue, 
                                         FLOAT *SubCostRow, 
                                         ULONG NumberAddedInRound);
static void ENSEMBLEADT_ChangeRowScoreDueToGapChange(ENSEMBLEADT_TYPE *Ensemble, 
                                                     FLOAT OldCost, 
                                                     FLOAT NewCost, 
                                                     FLOAT *SubCostRow, 
                                                     ULONG NumberAddedInRound);
static void ENSEMBLEADT_UpdateRowScoreWithCost(ENSEMBLEADT_TYPE *Ensemble, 
                                               FLOAT Cost, 
                                               FLOAT *SubCostRow, 
                                               ULONG NumberAddedInRound);
static void ENSEMBLEADT_InitializeRowScoreWithGaps(ENSEMBLEADT_TYPE *Ensemble,
                                                   ULONG NumberOpen, 
                                                   ULONG NumberExtension, 
                                                   ULONG NumberTailOpen, 
                                                   ULONG NumberTailExtension, 
                                                   FLOAT *SubCostRow);

static void ENSEMBLEADT_AllocateNewNode(ENSEMBLEADT_TYPE *Ensemble, ENSEMBLEADT_NODE_TYPE **NewNode);
static FLOAT ENSEMBLEADT_DNASubstitutionCost(ENSEMBLEADT_TYPE *Ensemble, BYTE Base);
static FLOAT ENSEMBLEADT_AASubstitutionCost(ENSEMBLEADT_TYPE *Ensemble, BYTE Base);

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/
static UBYTE m_SubCostRowMax;
static ULONG m_TotalNumberOfSequences;

static FLOAT m_InDelGapStartCost;
static ENSEMBLEADT_UPDATE_ROW_SCORE_FUNCTION m_UpdateRowScore;

static ENSEMBLEADT_DNA_MATRIX_TYPE *m_SubCostDNAMatrix;
static ENSEMBLEADT_AA_MATRIX_TYPE *m_SubCostAAMatrix;

static ULONG m_GapAdjustmentWindow;

static FLOAT m_GrammarPieceMismatchPenalty;
static FLOAT m_GrammarPieceMatchBenefit;

static const ENSEMBLEADT_DNA_MATRIX_TYPE m_IUBSubMatrix =
{
   {  /*   A,     C,     G,     T,   Unknown */
      {  1.0,  -0.9,  -0.9,  -0.9,  -0.9},   /* A */
      { -0.9,   1.0,  -0.9,  -0.9,  -0.9},   /* C */
      { -0.9,  -0.9,   1.0,  -0.9,  -0.9},   /* G */
      { -0.9,  -0.9,  -0.9,   1.0,  -0.9},   /* T */
      { -0.9,  -0.9,  -0.9,  -0.9,  -0.9},   /* Unknown */
   }
};

#include "Pairs_Gonnet250.h"
#include "Pairs_Blosum45.h"
#include "Pairs_Blosum62.h"
#include "Pairs_Blosum80.h"

static const ENSEMBLEADT_DNA_INDEX m_DNAIndexLookup[ALPHABETIC_CHARACTERS] = 
{
   ENSEMBLEADT_DNA_A,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_C,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_G,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_T,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN,
   ENSEMBLEADT_DNA_UNKNOWN
};

static const ENSEMBLEADT_AA_INDEX m_AminoAcidIndexLookup[ALPHABETIC_CHARACTERS] = 
{
   ENSEMBLEADT_AA_A,
   ENSEMBLEADT_AA_B,
   ENSEMBLEADT_AA_C,
   ENSEMBLEADT_AA_D,
   ENSEMBLEADT_AA_E,
   ENSEMBLEADT_AA_F,
   ENSEMBLEADT_AA_G,
   ENSEMBLEADT_AA_H,
   ENSEMBLEADT_AA_I,
   ENSEMBLEADT_AA_UNKNOWN,
   ENSEMBLEADT_AA_K,
   ENSEMBLEADT_AA_L,
   ENSEMBLEADT_AA_M,
   ENSEMBLEADT_AA_N,
   ENSEMBLEADT_AA_UNKNOWN,
   ENSEMBLEADT_AA_P,
   ENSEMBLEADT_AA_Q,
   ENSEMBLEADT_AA_R,
   ENSEMBLEADT_AA_S,
   ENSEMBLEADT_AA_T,
   ENSEMBLEADT_AA_UNKNOWN,
   ENSEMBLEADT_AA_V,
   ENSEMBLEADT_AA_W,
   ENSEMBLEADT_AA_X,
   ENSEMBLEADT_AA_Y,
   ENSEMBLEADT_AA_Z,
   ENSEMBLEADT_AA_UNKNOWN
};

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void ENSEMBLEADT_Initialize(ULONG NumberOfSequences)
{
   switch (PARMS_GetSequenceType())
   {
      case PARMS_SEQUENCE_DNA:
         m_InDelGapStartCost = -PARMS_GetDNAGapOpenPenalty();
         ENSEMBLEADT_InDelGapExtensionCost = -PARMS_GetDNAGapExtensionPenalty();
         ENSEMBLEADT_InDelTailGapStartCost = -PARMS_GetDNATailGapOpenPenalty();
         ENSEMBLEADT_InDelTailGapExtensionCost = -PARMS_GetDNATailGapExtensionPenalty();
         m_SubCostDNAMatrix = (ENSEMBLEADT_DNA_MATRIX_TYPE *) &m_IUBSubMatrix;
         m_SubCostRowMax = ENSEMBLEADT_DNA_MAX;
         m_UpdateRowScore = ENSEMBLEADT_UpdateDNARowScore;
         ENSEMBLEADT_SubstitutionCost = ENSEMBLEADT_DNASubstitutionCost;
         break;
         
      case PARMS_SEQUENCE_AMINO_ACID:
         m_InDelGapStartCost = -PARMS_GetAAGapOpenPenalty();
         ENSEMBLEADT_InDelGapExtensionCost = -PARMS_GetAAGapExtensionPenalty();
         ENSEMBLEADT_InDelTailGapStartCost = -PARMS_GetAATailGapOpenPenalty();
         ENSEMBLEADT_InDelTailGapExtensionCost = -PARMS_GetAATailGapExtensionPenalty();

         switch (PARMS_GetAASubstitutionMatrix())
         {
            case PARMS_AA_SUBSTITUTION_GONNET250:
               m_SubCostAAMatrix = (ENSEMBLEADT_AA_MATRIX_TYPE *) &m_GONNET250SubMatrix;
               m_InDelGapStartCost *= ENSEMBLEADT_GONNET250_SCALE;
               ENSEMBLEADT_InDelGapExtensionCost *= ENSEMBLEADT_GONNET250_SCALE;
               ENSEMBLEADT_InDelTailGapStartCost *= ENSEMBLEADT_GONNET250_SCALE;
               ENSEMBLEADT_InDelTailGapExtensionCost *= ENSEMBLEADT_GONNET250_SCALE;
               break;
               
            case PARMS_AA_SUBSTITUTION_BLOSUM45:
               m_SubCostAAMatrix = (ENSEMBLEADT_AA_MATRIX_TYPE *) &m_BLOSUM45SubMatrix;
               break;
               
            case PARMS_AA_SUBSTITUTION_BLOSUM62:
               m_SubCostAAMatrix = (ENSEMBLEADT_AA_MATRIX_TYPE *) &m_BLOSUM62SubMatrix;
               break;
               
            case PARMS_AA_SUBSTITUTION_BLOSUM80:
               m_SubCostAAMatrix = (ENSEMBLEADT_AA_MATRIX_TYPE *) &m_BLOSUM80SubMatrix;
               break;
         }
         m_SubCostRowMax = ENSEMBLEADT_AA_MAX;
         m_UpdateRowScore = ENSEMBLEADT_UpdateAARowScore;
         ENSEMBLEADT_SubstitutionCost = ENSEMBLEADT_AASubstitutionCost;
         break;
   }

   m_TotalNumberOfSequences = NumberOfSequences;
   m_GrammarPieceMismatchPenalty = PARMS_GetGrammarPieceMismatchPenalty();
   m_GrammarPieceMatchBenefit = PARMS_GetGrammarPieceMatchBenefit();
}

/*********************************************************************************/

void ENSEMBLEADT_InitializeEnsemble(ENSEMBLEADT_TYPE *Ensemble,
                                    STRINGADT_STRING_TYPE *FirstSequence,
                                    STRINGADT_STRING_TYPE *FirstGrammarPieceSequence,
                                    ULONG FirstSequenceNumber)
{
   ULONG i;
   ENSEMBLEADT_NODE_TYPE *NewNode;
   ENSEMBLEADT_NODE_TYPE *CurrentNode = NULL;

   Ensemble->CurrentLength = 0;
   Ensemble->NumberOfSequences = 0;

   if (FirstSequence->Length != FirstGrammarPieceSequence->Length)
   {
      printf("ERROR: the grammar piece sequences do not match the input sequences.\n");
      exit(1);
   }
   
   for (i = 0; i < FirstSequence->Length; i++)
   {
      ENSEMBLEADT_AllocateNewNode(Ensemble, &NewNode);

      NewNode->Info.Characters[FirstSequenceNumber] = FirstSequence->Characters[i];
      m_UpdateRowScore(Ensemble, FirstSequence->Characters[i], NewNode->Info.SubCostRow, 1);
      NewNode->Info.GrammarPieces[FirstSequenceNumber] = FirstGrammarPieceSequence->Characters[i];
      NewNode->Info.MaxGrammarPieceLength = MAX(NewNode->Info.MaxGrammarPieceLength, 
                                                FirstGrammarPieceSequence->Characters[i]);
      NewNode->Info.IxGapCost = m_InDelGapStartCost;      
      NewNode->Info.IyGapCost = m_InDelGapStartCost;
      NewNode->Info.NumberOfGaps -= 1;

      if (i == 0)
      {
         Ensemble->Start = NewNode;
         NewNode->Prev = Ensemble->Start;
      }
      else
      {
         NewNode->Prev = CurrentNode;
         CurrentNode->Next = NewNode;

         if (i == FirstSequence->Length - 1)
         {
            Ensemble->End = NewNode;
            NewNode->Next = Ensemble->End;
         }
      }

      CurrentNode = NewNode;
   }
   
   Ensemble->TBNode = NULL;
   Ensemble->TFNode = NULL;
   Ensemble->NumberOfSequences = 1;
}

/*********************************************************************************/

void ENSEMBLEADT_CleanUp(ENSEMBLEADT_TYPE *Ensemble)
{
   ENSEMBLEADT_NODE_TYPE *CurrentNode;

   if (Ensemble->End != NULL)
   {
      CurrentNode = Ensemble->End;

      while (CurrentNode != Ensemble->Start)
      {
         free(CurrentNode->Info.SubCostRow);
         free(CurrentNode->Info.Characters);
         free(CurrentNode->Info.GrammarPieces);
         CurrentNode = CurrentNode->Prev;
         free(CurrentNode->Next);
      }

      free(CurrentNode->Info.SubCostRow);
      free(CurrentNode->Info.Characters);
      free(CurrentNode->Info.GrammarPieces);
      free(CurrentNode);
      
      Ensemble->Start = NULL;
      Ensemble->End = NULL;
      Ensemble->CurrentLength = 0;
      Ensemble->TBNode = NULL;
      Ensemble->TFNode = NULL;
      Ensemble->NumberOfSequences = 0;
   }
}

/*********************************************************************************/

void ENSEMBLEADT_PerformTraceBackM(ENSEMBLEADT_TYPE *Ensemble,
                                   BYTE Residue, 
                                   UBYTE PieceLength, 
                                   ULONG IndexInEnsemble,
                                   ULONG NumberAddedInRound)
{
   /*
      This is saying the current ensemble is aligned with a character in the new sequence.
   */
   Ensemble->TBNode->Info.Characters[IndexInEnsemble] = Residue;
   Ensemble->TBNode->Info.GrammarPieces[IndexInEnsemble] = PieceLength;
   switch (Residue)
   {
      case ENSEMBLEADT_TAIL_GAP_RESIDUE:
      case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
      case ENSEMBLEADT_GAP_RESIDUE:
      case ENSEMBLEADT_GAP_OPEN_RESIDUE:
         break;

      default:
         Ensemble->TBNode->Info.NumberOfGaps -= 1;
         break;
   }
   m_UpdateRowScore(Ensemble, Residue, Ensemble->TBNode->Info.SubCostRow, NumberAddedInRound);
   Ensemble->TBNode->Info.MaxGrammarPieceLength = MAX(Ensemble->TBNode->Info.MaxGrammarPieceLength, 
                                                      PieceLength);
   if (Ensemble->TBNode != Ensemble->End)
   {
      if (Ensemble->TBNode->Next->Info.Characters[IndexInEnsemble] == ENSEMBLEADT_GAP_RESIDUE)
      {
         Ensemble->TBNode->Next->Info.Characters[IndexInEnsemble] = ENSEMBLEADT_GAP_OPEN_RESIDUE;
         ENSEMBLEADT_ChangeRowScoreDueToGapChange(Ensemble,
                                                  ENSEMBLEADT_InDelGapExtensionCost, 
                                                  m_InDelGapStartCost,
                                                  Ensemble->TBNode->Next->Info.SubCostRow,
                                                  NumberAddedInRound);

         Ensemble->TBNode->Next->Info.IxGapCost = ((Ensemble->TBNode->Next->Info.IxGapCost * 
                                                    (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + 
                                                   m_InDelGapStartCost) / 
                                                  (Ensemble->NumberOfSequences + NumberAddedInRound);
      }
      else if (Ensemble->TBNode->Next->Info.Characters[IndexInEnsemble] == ENSEMBLEADT_TAIL_GAP_RESIDUE)
      {
         Ensemble->TBNode->Next->Info.Characters[IndexInEnsemble] = ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE;
         ENSEMBLEADT_ChangeRowScoreDueToGapChange(Ensemble,
                                                  ENSEMBLEADT_InDelTailGapExtensionCost, 
                                                  ENSEMBLEADT_InDelTailGapStartCost,
                                                  Ensemble->TBNode->Next->Info.SubCostRow,
                                                  NumberAddedInRound);

         Ensemble->TBNode->Next->Info.IxGapCost = ((Ensemble->TBNode->Next->Info.IxGapCost * 
                                                    (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + 
                                                   ENSEMBLEADT_InDelTailGapStartCost) / 
                                                  (Ensemble->NumberOfSequences + NumberAddedInRound);
      }
      else
      {
         Ensemble->TBNode->Next->Info.IxGapCost = ((Ensemble->TBNode->Next->Info.IxGapCost * 
                                                    (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + 
                                                   m_InDelGapStartCost) / 
                                                  (Ensemble->NumberOfSequences + NumberAddedInRound);
      }
   }
   Ensemble->TBNode->Info.IyGapCost = ((Ensemble->TBNode->Info.IyGapCost * 
                                        (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + 
                                       m_InDelGapStartCost) / 
                                      (Ensemble->NumberOfSequences + NumberAddedInRound);
}

/*********************************************************************************/

void ENSEMBLEADT_PerformTraceBackIy(ENSEMBLEADT_TYPE *Ensemble, 
                                    ULONG IndexInEnsemble,
                                    ULONG NumberAddedInRound)
{
   /*
      This is saying the current ensemble is aligned with a gap in the new sequence.
   */
   if (Ensemble->TBNode == Ensemble->End)
   {
      Ensemble->TBNode->Info.Characters[IndexInEnsemble] = ENSEMBLEADT_TAIL_GAP_RESIDUE;
      ENSEMBLEADT_UpdateRowScoreWithCost(Ensemble,
                                         ENSEMBLEADT_InDelTailGapExtensionCost, 
                                         Ensemble->TBNode->Info.SubCostRow,
                                         NumberAddedInRound);
   }
   else
   {
      if (Ensemble->TBNode->Next->Info.Characters[IndexInEnsemble] == ENSEMBLEADT_TAIL_GAP_RESIDUE)
      {
         Ensemble->TBNode->Info.Characters[IndexInEnsemble] = ENSEMBLEADT_TAIL_GAP_RESIDUE;
         ENSEMBLEADT_UpdateRowScoreWithCost(Ensemble,
                                            ENSEMBLEADT_InDelTailGapExtensionCost, 
                                            Ensemble->TBNode->Info.SubCostRow,
                                            NumberAddedInRound);
         Ensemble->TBNode->Next->Info.IxGapCost = ((Ensemble->TBNode->Next->Info.IxGapCost * 
                                                    (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + 
                                                   ENSEMBLEADT_InDelTailGapExtensionCost) / 
                                                  (Ensemble->NumberOfSequences + NumberAddedInRound);
      }
      else
      {
         Ensemble->TBNode->Info.Characters[IndexInEnsemble] = ENSEMBLEADT_GAP_RESIDUE;
         ENSEMBLEADT_UpdateRowScoreWithCost(Ensemble,
                                            ENSEMBLEADT_InDelGapExtensionCost, 
                                            Ensemble->TBNode->Info.SubCostRow,
                                            NumberAddedInRound);
         Ensemble->TBNode->Next->Info.IxGapCost = ((Ensemble->TBNode->Next->Info.IxGapCost * 
                                                    (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + 
                                                   ENSEMBLEADT_InDelGapExtensionCost) / 
                                                  (Ensemble->NumberOfSequences + NumberAddedInRound);
      }
   }
   Ensemble->TBNode->Info.MaxGrammarPieceLength = MAX(Ensemble->TBNode->Info.MaxGrammarPieceLength, 0);
   /* s(-,-) = 0 */
   Ensemble->TBNode->Info.IyGapCost = (Ensemble->TBNode->Info.IyGapCost * 
                                       (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) / 
                                      (Ensemble->NumberOfSequences + NumberAddedInRound);
}

/*********************************************************************************/

void ENSEMBLEADT_PerformTraceBackIyFinal(ENSEMBLEADT_TYPE *Ensemble, 
                                         ULONG IndexInEnsemble,
                                         ULONG NumberAddedInRound)
{
   /*
      This is saying the current ensemble is aligned with a gap in the new sequence.
   */
   if (Ensemble->TBNode->Next->Info.Characters[IndexInEnsemble] == ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE)
   {
      Ensemble->TBNode->Next->Info.Characters[IndexInEnsemble] = ENSEMBLEADT_TAIL_GAP_RESIDUE;
      ENSEMBLEADT_ChangeRowScoreDueToGapChange(Ensemble,
                                               ENSEMBLEADT_InDelTailGapStartCost,
                                               ENSEMBLEADT_InDelTailGapExtensionCost,
                                               Ensemble->TBNode->Next->Info.SubCostRow,
                                               NumberAddedInRound);
   }
   else
   {
      Ensemble->TBNode->Next->Info.IxGapCost = ((Ensemble->TBNode->Next->Info.IxGapCost * 
                                                 (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + 
                                                ENSEMBLEADT_InDelTailGapExtensionCost) / 
                                               (Ensemble->NumberOfSequences + NumberAddedInRound);
   }
      
   Ensemble->TBNode->Info.Characters[IndexInEnsemble] = ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE;
   ENSEMBLEADT_UpdateRowScoreWithCost(Ensemble,
                                      ENSEMBLEADT_InDelTailGapStartCost, 
                                      Ensemble->TBNode->Info.SubCostRow,
                                      NumberAddedInRound);
   Ensemble->TBNode->Info.MaxGrammarPieceLength = MAX(Ensemble->TBNode->Info.MaxGrammarPieceLength, 0);
   Ensemble->TBNode->Info.IxGapCost = ((Ensemble->TBNode->Info.IxGapCost * 
                                        (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + 
                                       ENSEMBLEADT_InDelTailGapExtensionCost) / 
                                      (Ensemble->NumberOfSequences + NumberAddedInRound);
   /* s(-,-) = 0 */
   Ensemble->TBNode->Info.IyGapCost = (Ensemble->TBNode->Info.IyGapCost * 
                                       (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) / 
                                      (Ensemble->NumberOfSequences + NumberAddedInRound);
}

/*********************************************************************************/

void ENSEMBLEADT_PerformTraceBackIx(ENSEMBLEADT_TYPE *Ensemble, 
                                    BYTE Residue, 
                                    UBYTE PieceLength, 
                                    ULONG IndexInEnsemble)
{
   /*
      This is saying the new sequence is aligned with a gap in the ensemble.
      And so, we need to insert a new column in the ensemble, fix all the scores
      up to this row, and then add the new residue to this row and score it properly.
   */

   ENSEMBLEADT_NODE_TYPE *NewNode;
   ULONG i;
   ULONG GapOpen;
   ULONG GapExtension;
   ULONG TailGapOpen;
   ULONG TailGapExtension;
   ULONG TailOpenCorrection;
   ULONG OpenCorrection;

   ENSEMBLEADT_AllocateNewNode(Ensemble, &NewNode);

   NewNode->Prev = Ensemble->TBNode;

   if (Ensemble->TBNode == Ensemble->End)
   {
      Ensemble->End = NewNode;
      NewNode->Next = Ensemble->End;
      Ensemble->TBNode->Next = NewNode;

      /*
         Fix all scores up to the new row.
      */
      TailGapOpen = 0;
      TailGapExtension = 0;
      for (i = 0; i < Ensemble->NumberOfSequences; i++)
      {
         if ((Ensemble->TBNode->Info.Characters[i] == ENSEMBLEADT_TAIL_GAP_RESIDUE) ||
             (Ensemble->TBNode->Info.Characters[i] == ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE))
         {
            NewNode->Info.Characters[i] = ENSEMBLEADT_TAIL_GAP_RESIDUE;
            TailGapExtension++;
         }
         else
         {
            NewNode->Info.Characters[i] = ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE;
            TailGapOpen++;
         }
      }
      ENSEMBLEADT_InitializeRowScoreWithGaps(Ensemble,
                                             0, 
                                             0, 
                                             TailGapOpen, 
                                             TailGapExtension, 
                                             NewNode->Info.SubCostRow);
      NewNode->Info.IxGapCost = ((ENSEMBLEADT_InDelTailGapStartCost * TailGapOpen) + 
                                 (ENSEMBLEADT_InDelTailGapExtensionCost * TailGapExtension)) / 
                                Ensemble->NumberOfSequences;
   }
   else
   {
      NewNode->Next = Ensemble->TBNode->Next;
      Ensemble->TBNode->Next->Prev = NewNode;
      Ensemble->TBNode->Next = NewNode;

      /*
         Fix all scores up to the new row.
      */
      TailGapOpen = 0;
      TailGapExtension = 0;
      GapOpen = 0;
      GapExtension = 0;
      TailOpenCorrection = 0;
      OpenCorrection = 0;
      for (i = 0; i < Ensemble->NumberOfSequences; i++)
      {
         switch (Ensemble->TBNode->Info.Characters[i])
         {
            case ENSEMBLEADT_TAIL_GAP_RESIDUE:
            case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
               NewNode->Info.Characters[i] = ENSEMBLEADT_TAIL_GAP_RESIDUE;
               TailGapExtension++;
               break;
               
            case ENSEMBLEADT_GAP_RESIDUE:
            case ENSEMBLEADT_GAP_OPEN_RESIDUE:
               NewNode->Info.Characters[i] = ENSEMBLEADT_GAP_RESIDUE;
               GapExtension++;
               break;

            default:
               switch (NewNode->Next->Info.Characters[i])
               {
                  case ENSEMBLEADT_TAIL_GAP_RESIDUE:
                     NewNode->Info.Characters[i] = ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE;
                     TailGapOpen++;
                     break;

                  case ENSEMBLEADT_GAP_RESIDUE:
                     NewNode->Info.Characters[i] = ENSEMBLEADT_GAP_OPEN_RESIDUE;
                     GapOpen++;
                     break;
                     
                  case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
                     NewNode->Info.Characters[i] = ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE;
                     NewNode->Next->Info.Characters[i] = ENSEMBLEADT_TAIL_GAP_RESIDUE;
                     TailGapOpen++;
                     TailOpenCorrection++;
                     break;
                     
                  case ENSEMBLEADT_GAP_OPEN_RESIDUE:
                     NewNode->Info.Characters[i] = ENSEMBLEADT_GAP_OPEN_RESIDUE;
                     NewNode->Next->Info.Characters[i] = ENSEMBLEADT_GAP_RESIDUE;
                     GapOpen++;
                     OpenCorrection++;
                     break;

                  default:
                     NewNode->Info.Characters[i] = ENSEMBLEADT_GAP_OPEN_RESIDUE;
                     GapOpen++;
                     break;
               }
               break;
         }
      }
      ENSEMBLEADT_ChangeRowScoreDueToGapChange(Ensemble,
                                               (ENSEMBLEADT_InDelTailGapStartCost * TailOpenCorrection) + 
                                               (m_InDelGapStartCost * OpenCorrection), 
                                               (ENSEMBLEADT_InDelTailGapExtensionCost * TailOpenCorrection) + 
                                               (ENSEMBLEADT_InDelGapExtensionCost * OpenCorrection), 
                                               NewNode->Next->Info.SubCostRow,
                                               1);
      ENSEMBLEADT_InitializeRowScoreWithGaps(Ensemble,
                                             GapOpen, 
                                             GapExtension, 
                                             TailGapOpen, 
                                             TailGapExtension, 
                                             NewNode->Info.SubCostRow);
      NewNode->Info.IxGapCost = NewNode->Next->Info.IxGapCost; 

      if (NewNode->Next->Info.Characters[IndexInEnsemble] == ENSEMBLEADT_GAP_RESIDUE)
      {
         NewNode->Next->Info.Characters[IndexInEnsemble] = ENSEMBLEADT_GAP_OPEN_RESIDUE;
         ENSEMBLEADT_ChangeRowScoreDueToGapChange(Ensemble,
                                                  ENSEMBLEADT_InDelGapExtensionCost, 
                                                  m_InDelGapStartCost,
                                                  NewNode->Next->Info.SubCostRow,
                                                  1);
      
         NewNode->Next->Info.IxGapCost = ((ENSEMBLEADT_InDelTailGapExtensionCost * 
                                           (TailGapOpen + TailGapExtension)) + 
                                          (ENSEMBLEADT_InDelGapExtensionCost * (GapOpen + GapExtension)) +
                                          m_InDelGapStartCost) / 
                                         (Ensemble->NumberOfSequences + 1);
      }
      else if (NewNode->Next->Info.Characters[IndexInEnsemble] == ENSEMBLEADT_TAIL_GAP_RESIDUE)
      {
         NewNode->Next->Info.Characters[IndexInEnsemble] = ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE;
         ENSEMBLEADT_ChangeRowScoreDueToGapChange(Ensemble,
                                                  ENSEMBLEADT_InDelTailGapExtensionCost, 
                                                  ENSEMBLEADT_InDelTailGapStartCost,
                                                  NewNode->Next->Info.SubCostRow,
                                                  1);

         NewNode->Next->Info.IxGapCost = ((ENSEMBLEADT_InDelTailGapExtensionCost * 
                                           (TailGapOpen + TailGapExtension)) + 
                                          (ENSEMBLEADT_InDelGapExtensionCost * (GapOpen + GapExtension)) +
                                          ENSEMBLEADT_InDelTailGapStartCost) / 
                                         (Ensemble->NumberOfSequences + 1);
      }
      else
      {
         NewNode->Next->Info.IxGapCost = ((ENSEMBLEADT_InDelTailGapExtensionCost * 
                                           (TailGapOpen + TailGapExtension)) + 
                                          (ENSEMBLEADT_InDelGapExtensionCost * (GapOpen + GapExtension)) +
                                          m_InDelGapStartCost) / 
                                         (Ensemble->NumberOfSequences + 1);
      }
   }
   
   /*
      Add the new residue to the row.
   */
   NewNode->Info.Characters[IndexInEnsemble] = Residue;
   NewNode->Info.GrammarPieces[IndexInEnsemble] = PieceLength; 
   switch (Residue)
   {
      case ENSEMBLEADT_TAIL_GAP_RESIDUE:
      case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
      case ENSEMBLEADT_GAP_RESIDUE:
      case ENSEMBLEADT_GAP_OPEN_RESIDUE:
         break;

      default:
         NewNode->Info.NumberOfGaps -= 1;   
         break;
   }
   m_UpdateRowScore(Ensemble, Residue, NewNode->Info.SubCostRow, 1);
   NewNode->Info.MaxGrammarPieceLength = MAX(NewNode->Info.MaxGrammarPieceLength, PieceLength);
   /* s(-,-) = 0 for all previous rows */
   NewNode->Info.IyGapCost = m_InDelGapStartCost / (Ensemble->NumberOfSequences + 1);
}

/*********************************************************************************/

void ENSEMBLEADT_PerformTraceBackIxFinal(ENSEMBLEADT_TYPE *Ensemble, 
                                         BYTE Residue, 
                                         UBYTE PieceLength, 
                                         ULONG IndexInEnsemble)
{
   /*
      This is saying the new sequence is aligned with a gap in the ensemble.
      And so, we need to insert a new column in the ensemble, fix all the scores
      up to this row, and then add the new residue to this row and score it properly.
      At this point, we will just be extending all rows with tail-gaps.
   */

   ENSEMBLEADT_NODE_TYPE *NewNode;
   ULONG i;
   ULONG TailOpenCorrection;

   ENSEMBLEADT_AllocateNewNode(Ensemble, &NewNode);

   NewNode->Next = Ensemble->TBNode;
   Ensemble->TBNode->Prev = NewNode;
   Ensemble->Start = NewNode;
   NewNode->Prev = Ensemble->Start;
   
   /*
      Fix all scores up to the new row.
   */
   TailOpenCorrection = 0;
   for (i = 0; i < Ensemble->NumberOfSequences; i++)
   {
      if (NewNode->Next->Info.Characters[i] == ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE)
      {
         NewNode->Next->Info.Characters[i] = ENSEMBLEADT_TAIL_GAP_RESIDUE;
         TailOpenCorrection++;
      }

      NewNode->Info.Characters[i] = ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE;
   }
   ENSEMBLEADT_ChangeRowScoreDueToGapChange(Ensemble,
                                            (ENSEMBLEADT_InDelTailGapStartCost * TailOpenCorrection), 
                                            (ENSEMBLEADT_InDelTailGapExtensionCost * TailOpenCorrection), 
                                            NewNode->Next->Info.SubCostRow,
                                            1);
   ENSEMBLEADT_InitializeRowScoreWithGaps(Ensemble, 
                                          0, 
                                          0, 
                                          Ensemble->NumberOfSequences, 
                                          0, 
                                          NewNode->Info.SubCostRow);
   NewNode->Info.IxGapCost = ENSEMBLEADT_InDelTailGapStartCost;
   NewNode->Next->Info.IxGapCost = ((ENSEMBLEADT_InDelTailGapExtensionCost * Ensemble->NumberOfSequences) +
                                    m_InDelGapStartCost) / 
                                   (Ensemble->NumberOfSequences + 1);

   /*
      Add the new residue to the row.
   */
   NewNode->Info.Characters[IndexInEnsemble] = Residue;
   NewNode->Info.GrammarPieces[IndexInEnsemble] = PieceLength;
   switch (Residue)
   {
      case ENSEMBLEADT_TAIL_GAP_RESIDUE:
      case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
      case ENSEMBLEADT_GAP_RESIDUE:
      case ENSEMBLEADT_GAP_OPEN_RESIDUE:
         break;

      default:
         NewNode->Info.NumberOfGaps -= 1;   
         break;
   }
   m_UpdateRowScore(Ensemble, Residue, NewNode->Info.SubCostRow, 1);
   NewNode->Info.MaxGrammarPieceLength = MAX(NewNode->Info.MaxGrammarPieceLength, PieceLength);
   /* s(-,-) = 0 for all previous rows */
   NewNode->Info.IyGapCost = m_InDelGapStartCost / (Ensemble->NumberOfSequences + 1);
}

/*********************************************************************************/

void ENSEMBLEADT_GetPrintableAlignment(ENSEMBLEADT_TYPE *Ensemble, 
                                       STRINGLISTADT_STRINGLIST_TYPE *AlignedStrings,
                                       STRINGLISTADT_STRINGLIST_TYPE *AlignedGrammarPieces,
                                       GROUPARRAYADT_TYPE *SequenceAlignmentOrder,
                                       BYTE GapCharacter)
{
   ULONG i;
   ULONG j;
   ULONG k;
   STRINGADT_STRING_TYPE EmptyString;
   ENSEMBLEADT_NODE_TYPE *CurrentNode;

   STRINGADT_InitializeString(&EmptyString);
   for (j = 0; j < Ensemble->NumberOfSequences; j++)
   {
      STRINGLISTADT_AppendStringToStringList(AlignedStrings, 
                                             &EmptyString, 
                                             "ENSEMBLEADT_GetPrintableAlignment");
      STRINGLISTADT_AppendStringToStringList(AlignedGrammarPieces, 
                                             &EmptyString, 
                                             "ENSEMBLEADT_GetPrintableAlignment");
   }
   STRINGADT_CleanUpString(&EmptyString);

   CurrentNode = Ensemble->Start;
   for (i = 0; i < Ensemble->CurrentLength; i++)
   {
      for (j = 0; j < Ensemble->NumberOfSequences; j++)
      {
         k = SequenceAlignmentOrder->GroupRuns[j].ElementId;

         switch(CurrentNode->Info.Characters[j])
         {
            case ENSEMBLEADT_GAP_RESIDUE:
            case ENSEMBLEADT_TAIL_GAP_RESIDUE:
            case ENSEMBLEADT_GAP_OPEN_RESIDUE:
            case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
               STRINGADT_AppendStringFromChar(&(AlignedStrings->Strings[k]),
                                              GapCharacter, 
                                              "ENSEMBLEADT_GetPrintableAlignment");
               STRINGADT_AppendStringFromChar(&(AlignedGrammarPieces->Strings[k]),
                                              '.', 
                                              "ENSEMBLEADT_GetPrintableAlignment");
               break;

            default:
               STRINGADT_AppendStringFromChar(&(AlignedStrings->Strings[k]),
                                              CurrentNode->Info.Characters[j], 
                                              "ENSEMBLEADT_GetPrintableAlignment");
               if (CurrentNode->Info.GrammarPieces[j] > 9)
               {
                  STRINGADT_AppendStringFromChar(&(AlignedGrammarPieces->Strings[k]),
                                                 '*', 
                                                 "ENSEMBLEADT_GetPrintableAlignment");
               }
               else if (CurrentNode->Info.GrammarPieces[j] == 0)
               {
                  STRINGADT_AppendStringFromChar(&(AlignedGrammarPieces->Strings[k]),
                                                 '.', 
                                                 "ENSEMBLEADT_GetPrintableAlignment");
               }
               else
               {
                  STRINGADT_AppendStringFromChar(&(AlignedGrammarPieces->Strings[k]),
                                                 CurrentNode->Info.GrammarPieces[j] + '0', 
                                                 "ENSEMBLEADT_GetPrintableAlignment");
               }
               break;

         }
      }
      CurrentNode = CurrentNode->Next;
   }
}

/*********************************************************************************/

void ENSEMBLEADT_AdjustMSAGaps(ENSEMBLEADT_TYPE *Ensemble)
{
   ULONG i;
   ENSEMBLEADT_NODE_TYPE *CurrentNode;
   BOOLEAN DeletedColumn;
   ULONG GapsInColumnThreshold;

   GapsInColumnThreshold = (ULONG) (((FLOAT) m_TotalNumberOfSequences) * PARMS_GetGapsInColumnThreshold());
   m_GapAdjustmentWindow = PARMS_GetGapAdjustmentWindow();

   CurrentNode = Ensemble->Start;
   for (i = 0; i < Ensemble->CurrentLength; i++)
   {
      DeletedColumn = FALSE;
      if (CurrentNode->Info.NumberOfGaps >= GapsInColumnThreshold)
      {
         DeletedColumn = ENSEMBLEADT_ScanColumnForResidues(Ensemble, &CurrentNode, i);
      }

      if (DeletedColumn == TRUE)
      {
         i--;
      }
      else
      {
         CurrentNode = CurrentNode->Next;
      }
   }
}                        

/*********************************************************************************/

void ENSEMBLEADT_GetConsensusString(ENSEMBLEADT_TYPE *Ensemble, 
                                    STRINGADT_STRING_TYPE *ConsensusString,
                                    FLOAT **ConsensusConfidence,
                                    BOOLEAN IncludeGaps,
                                    BYTE GapCharacter,
                                    STRINGADT_STRING_TYPE *ConsensusGrammarPieces,
                                    BOOLEAN GetConsensusGrammarPieces)
{
   ULONG i;
   ULONG j;
   BYTE MaxCharacter;
   ULONG MaxCount;
   BOOLEAN DefiniteMax;
   ULONG LastCharacter;
   ULONG CharacterCounts[ALPHABETIC_CHARACTERS];
   ENSEMBLEADT_NODE_TYPE *CurrentNode;
   UBYTE MaxGrammarPiece;
   ULONG MaxGrammarPieceCount;
   ULONG GrammarPieceCounts[ENSEMBLEADT_MAX_GRAMMAR_PIECE_SIZE];

   *ConsensusConfidence = (FLOAT *) malloc (sizeof(FLOAT) * Ensemble->CurrentLength);

   if (*ConsensusConfidence == NULL)
   {
      printf("Error: malloc failed in ENSEMBLEADT_GetConsensusString.\n");
      exit(1);
   }
   
   if (IncludeGaps == TRUE)
   {
      LastCharacter = ALPHABETIC_CHARACTERS;
   }
   else
   {
      LastCharacter = ALPHABETIC_UNKNOWN_INDEX;
   }
   
   CurrentNode = Ensemble->Start;
   for (i = 0; i < Ensemble->CurrentLength; i++)
   {
      memset (CharacterCounts, 0, sizeof(ULONG) * ALPHABETIC_CHARACTERS);
      memset (GrammarPieceCounts, 0, sizeof(ULONG) * ENSEMBLEADT_MAX_GRAMMAR_PIECE_SIZE);

      for (j = 0; j < Ensemble->NumberOfSequences; j++)
      {
         CharacterCounts[ALPHA_INDEX(CurrentNode->Info.Characters[j])] += 1;
         GrammarPieceCounts[CurrentNode->Info.GrammarPieces[j]] += 1;
      }
      
      MaxCharacter = 0;
      MaxCount = CharacterCounts[0];
      DefiniteMax = TRUE;
      for (j = 1; j < LastCharacter; j++)
      {
         if (CharacterCounts[j] > MaxCount)
         {
            MaxCharacter = j;
            MaxCount = CharacterCounts[j];
            DefiniteMax = TRUE;
         }
         else if (CharacterCounts[j] == MaxCount)
         {
            DefiniteMax = FALSE;
         }
      }
      
      if (DefiniteMax == TRUE)
      {
         if (MaxCharacter == ALPHABETIC_UNKNOWN_INDEX)
         {
            STRINGADT_AppendStringFromChar(ConsensusString,
                                           GapCharacter, 
                                           "ENSEMBLEADT_GetConsensusString");
         }
         else
         {
            STRINGADT_AppendStringFromChar(ConsensusString,
                                           MaxCharacter + 'A', 
                                           "ENSEMBLEADT_GetConsensusString");
         }
         (*ConsensusConfidence)[i] = (((FLOAT) MaxCount) / ((FLOAT) Ensemble->NumberOfSequences));
      }
      else
      {
         STRINGADT_AppendStringFromChar(ConsensusString,
                                        GapCharacter, 
                                        "ENSEMBLEADT_GetConsensusString");
         (*ConsensusConfidence)[i] = 0;
      }
      
      if (GetConsensusGrammarPieces == TRUE)
      {
         MaxGrammarPiece = 0;
         MaxGrammarPieceCount = GrammarPieceCounts[0];
         for (j = 1; j < ENSEMBLEADT_MAX_GRAMMAR_PIECE_SIZE; j++)
         {
            if (GrammarPieceCounts[j] > MaxGrammarPieceCount)
            {
               MaxGrammarPiece = j;
               MaxGrammarPieceCount = GrammarPieceCounts[j];
            }
         }

         STRINGADT_AppendStringFromChar(ConsensusGrammarPieces,
                                        MaxGrammarPiece, 
                                        "ENSEMBLEADT_GetConsensusString");
      }
                     
      CurrentNode = CurrentNode->Next;
   }
}                        

/*********************************************************************************/

BOOLEAN ENSEMBLEADT_ScanColumnForResidues(ENSEMBLEADT_TYPE *Ensemble,
                                          ENSEMBLEADT_NODE_TYPE **CurrentNode, 
                                          ULONG CurrentColumn)
{
   ULONG j;
   ENSEMBLEADT_NODE_TYPE *NodeWithGap = NULL;
   ENSEMBLEADT_NODE_TYPE *TempNode;
   BOOLEAN Forward = FALSE;
   BOOLEAN DeletedColumn = FALSE;
   
   for (j = 0; j < Ensemble->NumberOfSequences; j++)
   {
      switch((*CurrentNode)->Info.Characters[j])
      {
         case ENSEMBLEADT_GAP_RESIDUE:
         case ENSEMBLEADT_TAIL_GAP_RESIDUE:
         case ENSEMBLEADT_GAP_OPEN_RESIDUE:
         case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
            break;

         default:
            if (ENSEMBLEADT_FindNearbyColumnWithGapInRow(Ensemble,
                                                         *CurrentNode, 
                                                         CurrentColumn, 
                                                         j, 
                                                         &NodeWithGap, 
                                                         &Forward) == TRUE)
            {
               ENSEMBLEADT_MoveGapFromOneColumnToAnother(*CurrentNode, j, NodeWithGap, Forward);
            }
            break;
      }
   }

   if ((*CurrentNode)->Info.NumberOfGaps >= Ensemble->NumberOfSequences)
   {
      if ((*CurrentNode) == Ensemble->End)
      {
         Ensemble->End = (*CurrentNode)->Prev;
         
         free((*CurrentNode)->Info.SubCostRow);
         free((*CurrentNode)->Info.Characters);
         free((*CurrentNode)->Info.GrammarPieces);
         (*CurrentNode) = (*CurrentNode)->Prev;
         free((*CurrentNode)->Next);

         (*CurrentNode)->Next = Ensemble->End;
      }
      else if ((*CurrentNode) == Ensemble->Start)
      {
         Ensemble->Start = (*CurrentNode)->Next;
         
         free((*CurrentNode)->Info.SubCostRow);
         free((*CurrentNode)->Info.Characters);
         free((*CurrentNode)->Info.GrammarPieces);
         (*CurrentNode) = (*CurrentNode)->Next;
         free((*CurrentNode)->Prev);

         (*CurrentNode)->Prev = Ensemble->Start;
      }
      else
      {
         (*CurrentNode)->Prev->Next = (*CurrentNode)->Next;
         (*CurrentNode)->Next->Prev = (*CurrentNode)->Prev;
         
         free((*CurrentNode)->Info.SubCostRow);
         free((*CurrentNode)->Info.Characters);
         free((*CurrentNode)->Info.GrammarPieces);
         TempNode = (*CurrentNode);
         (*CurrentNode) = (*CurrentNode)->Next;
         free(TempNode);
      }

      Ensemble->CurrentLength -= 1;
      DeletedColumn = TRUE;
   }
   
   return (DeletedColumn);
}

/*********************************************************************************/

BOOLEAN ENSEMBLEADT_FindNearbyColumnWithGapInRow(ENSEMBLEADT_TYPE *Ensemble,
                                                 ENSEMBLEADT_NODE_TYPE *BaseNode,
                                                 ULONG BaseColumn,
                                                 ULONG Row, 
                                                 ENSEMBLEADT_NODE_TYPE **ColumnWithGap,
                                                 BOOLEAN *Forward)
{
   BOOLEAN Done = FALSE;
   BOOLEAN Found = FALSE;
   BOOLEAN GapInColumn;
   ULONG k = 0;
   ENSEMBLEADT_NODE_TYPE *CheckNodeForward = BaseNode;
   ENSEMBLEADT_NODE_TYPE *CheckNodeBackward = BaseNode;
   
   while ((Done == FALSE) && (Found == FALSE))
   {
      k++;
      if (k > m_GapAdjustmentWindow)
      {
         Done = TRUE;
      }
      else
      {
         if ((BaseColumn + k) < Ensemble->CurrentLength)
         {
            CheckNodeForward = CheckNodeForward->Next;

            switch(CheckNodeForward->Info.Characters[Row])
            {
               case ENSEMBLEADT_GAP_RESIDUE:
               case ENSEMBLEADT_TAIL_GAP_RESIDUE:
               case ENSEMBLEADT_GAP_OPEN_RESIDUE:
               case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
                  GapInColumn = TRUE;
                  break;

               default:
                  GapInColumn = FALSE;
                  break;
            }
            
            if (GapInColumn == TRUE)
            {
               if (CheckNodeForward->Info.NumberOfGaps < BaseNode->Info.NumberOfGaps)
               {
                  Found = TRUE;
                  *ColumnWithGap = CheckNodeForward;
                  *Forward = TRUE;
               }
            }
         }
         
         if (Found == FALSE)
         {
            if (BaseColumn >= k)
            {
               CheckNodeBackward = CheckNodeBackward->Prev;
               
               switch(CheckNodeBackward->Info.Characters[Row])
               {
                  case ENSEMBLEADT_GAP_RESIDUE:
                  case ENSEMBLEADT_TAIL_GAP_RESIDUE:
                  case ENSEMBLEADT_GAP_OPEN_RESIDUE:
                  case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
                     GapInColumn = TRUE;
                     break;

                  default:
                     GapInColumn = FALSE;
                     break;
               }
               
               if (GapInColumn == TRUE)
               {
                  if (CheckNodeBackward->Info.NumberOfGaps < BaseNode->Info.NumberOfGaps)
                  {
                     Found = TRUE;
                     *ColumnWithGap = CheckNodeBackward;
                     *Forward = FALSE;
                  }
               }
            }
         }
      }
   }
   
   return (Found);
}

/*********************************************************************************/

void ENSEMBLEADT_MoveGapFromOneColumnToAnother(ENSEMBLEADT_NODE_TYPE *BaseNode,
                                               ULONG Row, 
                                               ENSEMBLEADT_NODE_TYPE *ColumnWithGap,
                                               BOOLEAN Forward)
{
   ENSEMBLEADT_NODE_TYPE *CurrentNode = ColumnWithGap;
   
   while (CurrentNode != BaseNode)
   {
      if (Forward == TRUE)
      {
         switch (CurrentNode->Info.Characters[Row])
         {
            case ENSEMBLEADT_TAIL_GAP_RESIDUE:
            case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
            case ENSEMBLEADT_GAP_RESIDUE:
            case ENSEMBLEADT_GAP_OPEN_RESIDUE:
               switch (CurrentNode->Prev->Info.Characters[Row])
               {
                  case ENSEMBLEADT_TAIL_GAP_RESIDUE:
                  case ENSEMBLEADT_GAP_RESIDUE:
                  case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
                  case ENSEMBLEADT_GAP_OPEN_RESIDUE:
                     break;

                  default:
                     CurrentNode->Info.NumberOfGaps -= 1;
                     break;
               }
               break;

            default:
               switch (CurrentNode->Prev->Info.Characters[Row])
               {
                  case ENSEMBLEADT_TAIL_GAP_RESIDUE:
                  case ENSEMBLEADT_GAP_RESIDUE:
                  case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
                  case ENSEMBLEADT_GAP_OPEN_RESIDUE:
                     CurrentNode->Info.NumberOfGaps += 1;
                     break;

                  default:
                     break;
               }
               break;
         }
         CurrentNode->Info.Characters[Row] = CurrentNode->Prev->Info.Characters[Row];
         CurrentNode->Info.GrammarPieces[Row] = CurrentNode->Prev->Info.GrammarPieces[Row];
         CurrentNode = CurrentNode->Prev;
      }
      else
      {
         switch (CurrentNode->Info.Characters[Row])
         {
            case ENSEMBLEADT_TAIL_GAP_RESIDUE:
            case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
            case ENSEMBLEADT_GAP_RESIDUE:
            case ENSEMBLEADT_GAP_OPEN_RESIDUE:
               switch (CurrentNode->Next->Info.Characters[Row])
               {
                  case ENSEMBLEADT_TAIL_GAP_RESIDUE:
                  case ENSEMBLEADT_GAP_RESIDUE:
                  case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
                  case ENSEMBLEADT_GAP_OPEN_RESIDUE:
                     break;

                  default:
                     CurrentNode->Info.NumberOfGaps -= 1;
                     break;
               }
               break;

            default:
               switch (CurrentNode->Next->Info.Characters[Row])
               {
                  case ENSEMBLEADT_TAIL_GAP_RESIDUE:
                  case ENSEMBLEADT_GAP_RESIDUE:
                  case ENSEMBLEADT_TAIL_GAP_OPEN_RESIDUE:
                  case ENSEMBLEADT_GAP_OPEN_RESIDUE:
                     CurrentNode->Info.NumberOfGaps += 1;
                     break;

                  default:
                     break;
               }
               break;
         }
         CurrentNode->Info.Characters[Row] = CurrentNode->Next->Info.Characters[Row];
         CurrentNode->Info.GrammarPieces[Row] = CurrentNode->Next->Info.GrammarPieces[Row];
         CurrentNode = CurrentNode->Next;
      }
   }
   BaseNode->Info.Characters[Row] = ENSEMBLEADT_GAP_RESIDUE;
   BaseNode->Info.GrammarPieces[Row] = 0;
   BaseNode->Info.NumberOfGaps += 1;
}

/*********************************************************************************/

void ENSEMBLEADT_UpdateDNARowScore(ENSEMBLEADT_TYPE *Ensemble, 
                                   BYTE Residue, 
                                   FLOAT *SubCostRow, 
                                   ULONG NumberAddedInRound)
{
   UBYTE j;
   UBYTE LookupRow;
   
   LookupRow = m_DNAIndexLookup[ALPHA_INDEX(Residue)];
   for (j = 0; j < m_SubCostRowMax; j++)
   {
      SubCostRow[j] = ((SubCostRow[j] * (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + 
                       m_SubCostDNAMatrix->Cells[LookupRow][j]) / 
                      (Ensemble->NumberOfSequences + NumberAddedInRound);
   }
}

/*********************************************************************************/

void ENSEMBLEADT_UpdateAARowScore(ENSEMBLEADT_TYPE *Ensemble, 
                                  BYTE Residue, 
                                  FLOAT *SubCostRow, 
                                  ULONG NumberAddedInRound)
{
   UBYTE j;
   UBYTE LookupRow;
   
   LookupRow = m_AminoAcidIndexLookup[ALPHA_INDEX(Residue)];
   for (j = 0; j < m_SubCostRowMax; j++)
   {
      SubCostRow[j] = ((SubCostRow[j] * (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + 
                       m_SubCostAAMatrix->Cells[LookupRow][j]) / 
                      (Ensemble->NumberOfSequences + NumberAddedInRound);
   }
}

/*********************************************************************************/

void ENSEMBLEADT_ChangeRowScoreDueToGapChange(ENSEMBLEADT_TYPE *Ensemble, 
                                              FLOAT OldCost, 
                                              FLOAT NewCost, 
                                              FLOAT *SubCostRow, 
                                              ULONG NumberAddedInRound)
{
   UBYTE j;
   
   for (j = 0; j < m_SubCostRowMax; j++)
   {
      SubCostRow[j] += ((NewCost - OldCost) / (Ensemble->NumberOfSequences + NumberAddedInRound));
   }
}

/*********************************************************************************/

void ENSEMBLEADT_UpdateRowScoreWithCost(ENSEMBLEADT_TYPE *Ensemble, 
                                        FLOAT Cost, 
                                        FLOAT *SubCostRow, 
                                        ULONG NumberAddedInRound)
{
   UBYTE j;
   
   for (j = 0; j < m_SubCostRowMax; j++)
   {
      SubCostRow[j] = ((SubCostRow[j] * (Ensemble->NumberOfSequences + NumberAddedInRound - 1)) + Cost) / 
                      (Ensemble->NumberOfSequences + NumberAddedInRound);
   }
}

/*********************************************************************************/

void ENSEMBLEADT_InitializeRowScoreWithGaps(ENSEMBLEADT_TYPE *Ensemble,
                                            ULONG NumberOpen, 
                                            ULONG NumberExtension, 
                                            ULONG NumberTailOpen, 
                                            ULONG NumberTailExtension, 
                                            FLOAT *SubCostRow)
{
   UBYTE j;
   
   for (j = 0; j < m_SubCostRowMax; j++)
   {
      SubCostRow[j] = ((m_InDelGapStartCost * NumberOpen) + 
                       (ENSEMBLEADT_InDelGapExtensionCost * NumberExtension) +
                       (ENSEMBLEADT_InDelTailGapStartCost * NumberTailOpen) +
                       (ENSEMBLEADT_InDelTailGapExtensionCost * NumberTailExtension)) / 
                      Ensemble->NumberOfSequences;
   }
}

/*********************************************************************************/

void ENSEMBLEADT_AllocateNewNode(ENSEMBLEADT_TYPE *Ensemble, ENSEMBLEADT_NODE_TYPE **NewNode)
{
   ULONG j;
   
   *NewNode = (ENSEMBLEADT_NODE_TYPE *) malloc (sizeof(ENSEMBLEADT_NODE_TYPE));

   if (*NewNode == NULL)
   {
      printf("Error: malloc failed in ENSEMBLEADT_AllocateNewNode.\n");
      exit(1);
   }
   else
   {
      (*NewNode)->Info.Characters = (BYTE *) malloc (sizeof(BYTE) * m_TotalNumberOfSequences);
      
      if ((*NewNode)->Info.Characters == NULL)
      {
         printf("Error: malloc failed in ENSEMBLEADT_AllocateNewNode.\n");
         exit(1);
      }
      else
      {
         (*NewNode)->Info.GrammarPieces = (UBYTE *) malloc (sizeof(UBYTE) * m_TotalNumberOfSequences);
         
         if ((*NewNode)->Info.GrammarPieces == NULL)
         {
            printf("Error: malloc failed in ENSEMBLEADT_AllocateNewNode.\n");
            exit(1);
         }
         else
         {
            (*NewNode)->Info.SubCostRow = (FLOAT *) malloc (sizeof(FLOAT) * m_SubCostRowMax);
            
            if ((*NewNode)->Info.SubCostRow == NULL)
            {
               printf("Error: malloc failed in ENSEMBLEADT_AllocateNewNode.\n");
               exit(1);
            }
            else
            {
               memset((*NewNode)->Info.Characters, 0, sizeof(BYTE) * m_TotalNumberOfSequences);
               memset((*NewNode)->Info.GrammarPieces, 0, sizeof(UBYTE) * m_TotalNumberOfSequences);
               for (j = 0; j < m_SubCostRowMax; j++)
               {
                  (*NewNode)->Info.SubCostRow[j] = 0;
               }
               (*NewNode)->Info.NumberOfGaps = m_TotalNumberOfSequences;
            }
         }
      }
   }

   Ensemble->CurrentLength += 1;
}

/*********************************************************************************/

FLOAT ENSEMBLEADT_DNASubstitutionCost(ENSEMBLEADT_TYPE *Ensemble, BYTE Base)
{
   return (Ensemble->TFNode->Info.SubCostRow[m_DNAIndexLookup[ALPHA_INDEX(Base)]]);
}

/*********************************************************************************/

FLOAT ENSEMBLEADT_AASubstitutionCost(ENSEMBLEADT_TYPE *Ensemble, BYTE Base)
{
   return (Ensemble->TFNode->Info.SubCostRow[m_AminoAcidIndexLookup[ALPHA_INDEX(Base)]]);
}

/*********************************************************************************/

FLOAT ENSEMBLEADT_GrammarPieceCost(ENSEMBLEADT_TYPE *Ensemble, UBYTE GrammarPieceLength)
{
   FLOAT Cost;

   if ((GrammarPieceLength == 0) && (Ensemble->TFNode->Info.MaxGrammarPieceLength == 0))
   {
      Cost = 0.0;
   }
   else if (((GrammarPieceLength == 0) && (Ensemble->TFNode->Info.MaxGrammarPieceLength > 0)) ||
            ((GrammarPieceLength > 0) && (Ensemble->TFNode->Info.MaxGrammarPieceLength == 0)))
   {
      Cost = -m_GrammarPieceMismatchPenalty;
   }
   else
   {
      Cost = m_GrammarPieceMatchBenefit;
   }
   
   return (Cost);
}
