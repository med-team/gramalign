/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _SMF_ADT
#define SMF_ADT_EXTERN
#else
#define SMF_ADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef struct
{
   ULONG ColumnIndex;
   FLOAT Value;
} SMF_ADT_CELL_TYPE;

typedef struct
{
   ULONG NumColsAllocated;
   ULONG NumCols;
   SMF_ADT_CELL_TYPE *Cols;
} SMF_ADT_ROW_TYPE;

typedef struct
{
   ULONG NumRows;
   ULONG NumCols;
   FLOAT DefaultValue;
   SMF_ADT_ROW_TYPE *Rows;
} SMF_ADT_MATRIX_TYPE;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/

void SMF_ADT_InitializeMatrix(SMF_ADT_MATRIX_TYPE *Matrix);
void SMF_ADT_DefineMatrix(SMF_ADT_MATRIX_TYPE *Matrix, 
                           ULONG Rows, 
                           ULONG Cols, 
                           FLOAT DefaultValue);
void SMF_ADT_CleanUpMatrix(SMF_ADT_MATRIX_TYPE *Matrix);
void SMF_ADT_SetElement(SMF_ADT_MATRIX_TYPE *Matrix, 
                        FLOAT Element, 
                        ULONG Row, 
                        ULONG Col);
FLOAT SMF_ADT_GetElement(SMF_ADT_MATRIX_TYPE *Matrix, 
                           ULONG Row, 
                           ULONG Col);
void SMF_ADT_PrintMatrix(SMF_ADT_MATRIX_TYPE *Matrix);
