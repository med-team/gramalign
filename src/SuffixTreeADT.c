/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _STADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "SuffixTreeADT.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/
#define STADT_TERMINATOR_LENGTH  1

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/
typedef struct
{
   STADT_NODE_TYPE *NodePointer;
   ULONG DownEdgeCount;
} STADT_LOCATION_TYPE;

typedef struct
{
   ULONG Start;
   ULONG Length;
} STADT_GAMMA_TYPE;

typedef enum
{
   STADT_RULE1,
   STADT_RULE2,
   STADT_RULE3
} STADT_RULE_TYPE;

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/
static void STADT_LoadAllSuffixes (STADT_TREE_TYPE *Tree);
static void STADT_AddNewLeaf (STADT_TREE_TYPE *Tree, 
                              STADT_NODE_TYPE *PreviousSibling,
                              STADT_NODE_TYPE *NextSibling,
                              STADT_NODE_TYPE *Parent,
                              ULONG EdgeStart,
                              STADT_LOCATION_TYPE *NewLocation);
static void STADT_SplitEdge (STADT_TREE_TYPE *Tree, 
                              STADT_LOCATION_TYPE *CurrentLocation,
                              STADT_LOCATION_TYPE *NewLocation);
static BOOLEAN STADT_NodeIsRoot (STADT_NODE_TYPE *Node);
static BOOLEAN STADT_NodeIsLeaf (STADT_NODE_TYPE *Node);
static BOOLEAN STADT_NodeIsInternal (STADT_NODE_TYPE *Node);
static ULONG STADT_EdgeLength (STADT_NODE_TYPE *Node);
static STADT_RULE_TYPE STADT_FindNodeWithCorrectStartOfEdge (STADT_TREE_TYPE *Tree, 
                                                               STADT_NODE_TYPE *ParentNode, 
                                                               UBYTE Character, 
                                                               STADT_NODE_TYPE **Location,
                                                               STADT_NODE_TYPE **PrevLocation);
static void STADT_WalkUp (STADT_LOCATION_TYPE *CurrentLocation,
                           STADT_GAMMA_TYPE *Gamma,
                           ULONG StartOfSuffix,
                           ULONG EndOfSuffix);
static void STADT_WalkDown (STADT_TREE_TYPE *Tree, 
                              STADT_LOCATION_TYPE *CurrentLocation,
                              STADT_GAMMA_TYPE *Gamma);
static STADT_RULE_TYPE STADT_CheckExtensionRules (STADT_TREE_TYPE *Tree, 
                                                   STADT_LOCATION_TYPE *CurrentLocation,
                                                   ULONG NewIndex,
                                                   BOOLEAN *NeedSuffixLinkCompletion);
static STADT_RULE_TYPE STADT_ExtendSymbolFromNode (STADT_TREE_TYPE *Tree, 
                                                   STADT_NODE_TYPE *Node, 
                                                   ULONG Symbol,
                                                   STADT_LOCATION_TYPE *NewLocation);
static void STADT_UpdateSuffixLink (STADT_NODE_TYPE *Root,
                                    STADT_NODE_TYPE *CurrentInternalNode,
                                    BOOLEAN NeedSuffixLinkCompletion);
static void STADT_DeleteTreeFromNode (STADT_NODE_TYPE **Tree);
static void STADT_PrintTreeFromNode (STADT_TREE_TYPE *Tree, 
                                       STADT_NODE_TYPE *Node,
                                       UBYTE *String,
                                       ULONG StringLength,
                                       ULONG SuffixLength);

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/
static STADT_NODE_TYPE *m_PreviouslyCreatedInternalNode;
static BOOLEAN m_NeedToFinishSuffixLink;

/*********************************************************************************/
/* Pseudocode */
/*********************************************************************************/

/*
   As described in Dan Gusfield, "Algorithms on Strings, Trees, and Sequences".
   
   Let N be the length of string S.
   Let CNP be the Current Node Pointer used for moving around the suffix links.
   Let LCINP be the Last Created Internal Node Pointer used for managing the suffix link
      creation.
   
   Construct Initial Tree
      Attach leaf-0 to the root.
      Set a global pointer to leaf-0.
      Set j_star = 0.
      
   for (i = 0; i < (N - 1); i++)
   {
      Begin phase i+1 
      
      for (j = j_star; j <= i; j++)
      {
         Begin extension j 
         
         Find path from root in current tree labeled S[j..i]. 
         if (j == 0)
         {
            Apply Rule 1.
            Set CNP to leaf-0.
         }
         else
         {
            Set CNP to the first internal node at or above the CNP
               that has a suffix link from it, or is the root.
            Set Gamma to the sub-string from the new CNP to the previous CNP.
         
            if (CNP == Root)
            {
               Follow path for S[j..i] from root.
            }
            else
            {
               Follow path from CNP along the suffix link.
               Follow path for Gamma from the suffix link end.
            }
                     
            Rule 1 
            if (S[j..i] ends at a leaf)
            {
               Add S[i+1] to label of leaf.
               Increase j_star by one.
            }
            else
            {
               Rule 3 
               if (S[i+1] already exists in a path after S[j..i])
               {
                  Do nothing since S[i+1] is already in a path label.
                  Stop performing current extensions (all remaining will be Rule 3).
               }
               else
               {
                  Rule 2 
                  Create a new leaf labeled j.
                  if (S[j..i] ends at an internal node)
                  {
                     Add the new leaf as a sibling to the other nodes exiting
                        the current internal node.
                  }
                  else
                  {
                     Split the current edge into two pieces connected at a new
                        internal node.
                     Add the new leaf as a sibling to the newly created branch
                        exiting the newly created internal node.
                     if (Rule 2 was applied in j-1)
                     {
                        Set the LCINP's suffix link to point to the newly created
                           internal node.
                     }
                     Set LCINP to the newly created internal node.
                  }
                  Increase j_star by one.
               }
            }
         }
      }
   }
*/ 

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void STADT_CreateTree (STADT_TREE_TYPE *Tree, 
                        UBYTE *String, 
                        ULONG Length,
                        UBYTE Terminator)
{
   ULONG TotalLength;

   Tree->Tree = (STADT_NODE_TYPE *) malloc (sizeof(STADT_NODE_TYPE));
   
   if (Tree->Tree == NULL)
   {
      printf("Error: malloc failed in STADT_CreateTree.\n");
      exit(1);
   }
   else
   {
      TotalLength = Length + STADT_TERMINATOR_LENGTH;

      Tree->String = (UBYTE *) malloc (sizeof(UBYTE) * TotalLength);

      if (Tree->String == NULL)
      {
         printf("Error: malloc failed in STADT_CreateTree.\n");
         exit(1);
      }

      Tree->Reproducible = (BOOLEAN *) malloc (sizeof(BOOLEAN) * Length);

      if (Tree->Reproducible == NULL)
      {
         printf("Error: malloc failed in STADT_CreateTree.\n");
         exit(1);
      }
      else
      {

         memcpy (Tree->String, String, sizeof(UBYTE) * Length);
         Tree->String[Length] = Terminator;
         memset (Tree->Reproducible, FALSE, sizeof(BOOLEAN) * Length);
         Tree->Length = TotalLength;
         Tree->Tree->Info.EdgeStart = 0;
         Tree->Tree->Info.EdgeStop = 0;
         Tree->Tree->Info.Depth = 0;
         Tree->Tree->Child = NULL;
         Tree->Tree->PreviousSibling = NULL;
         Tree->Tree->NextSibling = NULL;
         Tree->Tree->Parent = NULL;
         Tree->Tree->SuffixLink = NULL;
         
         STADT_LoadAllSuffixes (Tree);
      }
   }
}

/*********************************************************************************/

void STADT_LoadAllSuffixes (STADT_TREE_TYPE *Tree)
{
   ULONG i;
   ULONG j;
   STADT_LOCATION_TYPE CurrentLocation;
   STADT_LOCATION_TYPE PreviousLocation;
   STADT_GAMMA_TYPE Gamma;
   BOOLEAN NeedSuffixLinkCompletion;
   STADT_RULE_TYPE Rule = STADT_RULE1;
   ULONG FirstExplicitExtension;

   /* Rule 2 applied on the very first node. */
   STADT_AddNewLeaf (Tree, NULL, NULL, Tree->Tree, 0, &CurrentLocation);
   FirstExplicitExtension = 1;
   
   for (i = 0; i < (Tree->Length - 1); i++)
   {      
      /* Begin phase i + 1 */

      Rule = STADT_RULE1;
      m_NeedToFinishSuffixLink = FALSE;
      m_PreviouslyCreatedInternalNode = NULL;

      j = FirstExplicitExtension;
      while ((j <= (i + 1)) && (Rule != STADT_RULE3))
      {
         /* Begin extension j */

         PreviousLocation = CurrentLocation;
         
         if (j == (i + 1))
         {
            Rule = STADT_ExtendSymbolFromNode (Tree, Tree->Tree, j, &CurrentLocation);
            STADT_UpdateSuffixLink (Tree->Tree, NULL, FALSE);
         }
         else
         {
            STADT_WalkUp (&CurrentLocation, &Gamma, j, i);
            STADT_WalkDown (Tree, &CurrentLocation, &Gamma);   
            Rule = STADT_CheckExtensionRules (Tree, 
                                                &CurrentLocation, 
                                                i + 1, 
                                                &NeedSuffixLinkCompletion);
            STADT_UpdateSuffixLink (Tree->Tree, CurrentLocation.NodePointer, NeedSuffixLinkCompletion);
            
            if (Rule == STADT_RULE3)
            {
               CurrentLocation = PreviousLocation;
            }
         }

         if (Rule == STADT_RULE2)
         {
            FirstExplicitExtension += 1;
         }

         j += 1;
      }
   }

   if (Rule == STADT_RULE3)
   {
      /* Need to convert the final tree into a true suffix tree. */

      m_NeedToFinishSuffixLink = FALSE;
      m_PreviouslyCreatedInternalNode = NULL;

      for (j = FirstExplicitExtension; j < Tree->Length; j++)
      {
         /* Begin extension j */

         PreviousLocation = CurrentLocation;
            
         STADT_WalkUp (&CurrentLocation, &Gamma, j, Tree->Length - 1);
         STADT_WalkDown (Tree, &CurrentLocation, &Gamma);   
         Rule = STADT_CheckExtensionRules (Tree, 
                                             &CurrentLocation, 
                                             Tree->Length, 
                                             &NeedSuffixLinkCompletion);
         STADT_UpdateSuffixLink (Tree->Tree, CurrentLocation.NodePointer, NeedSuffixLinkCompletion);

         if (Rule == STADT_RULE3)
         {
            printf("Error: Rule 3 should not happen here.\n");
            exit(1);
         }
      }
   }
}

/*********************************************************************************/

void STADT_AddNewLeaf (STADT_TREE_TYPE *Tree, 
                        STADT_NODE_TYPE *PreviousSibling,
                        STADT_NODE_TYPE *NextSibling,
                        STADT_NODE_TYPE *Parent,
                        ULONG EdgeStart,
                        STADT_LOCATION_TYPE *NewLocation)
{
   STADT_NODE_TYPE *NewNode;
   
   NewNode = (STADT_NODE_TYPE *) malloc (sizeof(STADT_NODE_TYPE));

   if (NewNode == NULL)
   {
      printf("Error: malloc failed in STADT_AddNewLeaf.\n");
      exit(1);
   }
   else
   {
      NewNode->Info.EdgeStart = EdgeStart;
      NewNode->Info.EdgeStop = Tree->Length;
      NewNode->Info.Depth = Parent->Info.Depth + STADT_EdgeLength(NewNode);
      NewNode->Child = NULL;
      NewNode->PreviousSibling = PreviousSibling;
      NewNode->NextSibling = NextSibling;
      NewNode->Parent = Parent;
      NewNode->SuffixLink = NULL;
   }

   if (PreviousSibling == NULL)
   {
      Parent->Child = NewNode;
   }
   else
   {
      PreviousSibling->NextSibling = NewNode;
   }   

   if (NextSibling != NULL)
   {
      NextSibling->PreviousSibling = NewNode;
   }

   NewLocation->NodePointer = NewNode;
   NewLocation->DownEdgeCount = NewNode->Info.EdgeStop - NewNode->Info.EdgeStart + 1;
}

/*********************************************************************************/

void STADT_SplitEdge (STADT_TREE_TYPE *Tree, 
                        STADT_LOCATION_TYPE *CurrentLocation,
                        STADT_LOCATION_TYPE *NewLocation)
{
   STADT_NODE_TYPE *NewNode;
   
   NewNode = (STADT_NODE_TYPE *) malloc (sizeof(STADT_NODE_TYPE));

   if (NewNode == NULL)
   {
      printf("Error: malloc failed in STADT_SplitEdge.\n");
      exit(1);
   }
   else
   {
      NewNode->Info.EdgeStart = CurrentLocation->NodePointer->Info.EdgeStart;
      NewNode->Info.EdgeStop = CurrentLocation->NodePointer->Info.EdgeStart + CurrentLocation->DownEdgeCount - 1;
      NewNode->Info.Depth = CurrentLocation->NodePointer->Parent->Info.Depth + STADT_EdgeLength(NewNode);
      NewNode->Child = CurrentLocation->NodePointer;
      NewNode->PreviousSibling = CurrentLocation->NodePointer->PreviousSibling;
      NewNode->NextSibling = CurrentLocation->NodePointer->NextSibling;
      NewNode->Parent = CurrentLocation->NodePointer->Parent;
      NewNode->SuffixLink = NULL;
   }

   if (NewNode->PreviousSibling == NULL)
   {
      NewNode->Parent->Child = NewNode;
   }
   else
   {
      NewNode->PreviousSibling->NextSibling = NewNode;
   }   

   if (NewNode->NextSibling != NULL)
   {
      NewNode->NextSibling->PreviousSibling = NewNode;
   }

   CurrentLocation->NodePointer->Info.EdgeStart = NewNode->Info.EdgeStop + 1;
   CurrentLocation->NodePointer->PreviousSibling = NULL;
   CurrentLocation->NodePointer->NextSibling = NULL;
   CurrentLocation->NodePointer->Parent = NewNode;
   
   NewLocation->NodePointer = NewNode;
   NewLocation->DownEdgeCount = NewNode->Info.EdgeStop - NewNode->Info.EdgeStart + 1;
}

/*********************************************************************************/

BOOLEAN STADT_NodeIsRoot (STADT_NODE_TYPE *Node)
{
   BOOLEAN Root = FALSE;
   
   if (Node->Parent == NULL)
   {
      Root = TRUE;
   }
   
   return (Root);
}

/*********************************************************************************/

BOOLEAN STADT_NodeIsLeaf (STADT_NODE_TYPE *Node)
{
   BOOLEAN Leaf = FALSE;
   
   if (Node->Child == NULL)
   {
      Leaf = TRUE;
   }
   
   return (Leaf);
}

/*********************************************************************************/

BOOLEAN STADT_NodeIsInternal (STADT_NODE_TYPE *Node)
{
   BOOLEAN Internal = FALSE;
   
   if ((Node->Parent != NULL) && (Node->Child != NULL))
   {
      Internal = TRUE;
   }
   
   return (Internal);
}

/*********************************************************************************/

ULONG STADT_EdgeLength (STADT_NODE_TYPE *Node)
{
   ULONG EdgeLength;
   
   EdgeLength = Node->Info.EdgeStop - Node->Info.EdgeStart + 1;
   
   return (EdgeLength);
}

/*********************************************************************************/

STADT_RULE_TYPE STADT_FindNodeWithCorrectStartOfEdge (STADT_TREE_TYPE *Tree, 
                                                      STADT_NODE_TYPE *ParentNode, 
                                                      UBYTE Character, 
                                                      STADT_NODE_TYPE **Location,
                                                      STADT_NODE_TYPE **PrevLocation)
{
   STADT_RULE_TYPE Rule = STADT_RULE2;
   BOOLEAN Stop = FALSE;
      
   if (STADT_NodeIsLeaf(ParentNode) == FALSE)
   {
      *PrevLocation = NULL;
      *Location = ParentNode->Child;
      
      while ((*Location != NULL) && (Stop == FALSE))
      {
         if (Character > Tree->String[(*Location)->Info.EdgeStart])
         {
            *PrevLocation = *Location;
            *Location = (*Location)->NextSibling;
         }
         else
         {
            Stop = TRUE;
         }
      }

      if (Stop == TRUE)
      {
         if (Character == Tree->String[(*Location)->Info.EdgeStart])
         {
            Rule = STADT_RULE3;
         }
      }
   }
      
   return (Rule);
}

/*********************************************************************************/

void STADT_WalkUp (STADT_LOCATION_TYPE *CurrentLocation,
                     STADT_GAMMA_TYPE *Gamma,
                     ULONG StartOfSuffix,
                     ULONG EndOfSuffix)
{
   ULONG SuffixLength = EndOfSuffix - StartOfSuffix + 1;
   
   if (STADT_NodeIsRoot (CurrentLocation->NodePointer) == TRUE)
   {
      printf ("Error: trying to walk up from root.\n");
      exit (1);
   }
   else
   {
      if ((STADT_NodeIsInternal (CurrentLocation->NodePointer) == TRUE) &&
          (CurrentLocation->DownEdgeCount == STADT_EdgeLength(CurrentLocation->NodePointer)) &&
          (CurrentLocation->NodePointer->SuffixLink != NULL))
      {
         Gamma->Start = StartOfSuffix + (CurrentLocation->NodePointer->Info.Depth - 1);
         Gamma->Length = SuffixLength - (CurrentLocation->NodePointer->Info.Depth - 1);
         CurrentLocation->NodePointer = CurrentLocation->NodePointer->SuffixLink;
         CurrentLocation->DownEdgeCount = STADT_EdgeLength(CurrentLocation->NodePointer);
      }
      else
      {
         CurrentLocation->NodePointer = CurrentLocation->NodePointer->Parent;
         CurrentLocation->DownEdgeCount = STADT_EdgeLength(CurrentLocation->NodePointer);
         
         if (STADT_NodeIsRoot(CurrentLocation->NodePointer) == TRUE)
         {
            Gamma->Start = StartOfSuffix;
            Gamma->Length = EndOfSuffix - StartOfSuffix + 1;
         }
         else
         {
            Gamma->Start = StartOfSuffix + (CurrentLocation->NodePointer->Info.Depth - 1);
            Gamma->Length = SuffixLength - (CurrentLocation->NodePointer->Info.Depth - 1);
            CurrentLocation->NodePointer = CurrentLocation->NodePointer->SuffixLink;
            CurrentLocation->DownEdgeCount = STADT_EdgeLength(CurrentLocation->NodePointer);
            if (CurrentLocation->NodePointer == NULL)
            {
               printf ("Error: suffix links are incomplete.\n");
               exit (1);
            }

         }
      }
   }
}

/*********************************************************************************/

void STADT_WalkDown (STADT_TREE_TYPE *Tree, 
                     STADT_LOCATION_TYPE *CurrentLocation,
                     STADT_GAMMA_TYPE *Gamma)
{
   STADT_NODE_TYPE *Location = NULL;
   STADT_NODE_TYPE *PrevLocation = NULL;
   ULONG k = Gamma->Start;
   ULONG h = 0;
   STADT_RULE_TYPE Rule;
      
   while (h < Gamma->Length)
   {
      Rule = STADT_FindNodeWithCorrectStartOfEdge (Tree, 
                                                   CurrentLocation->NodePointer, 
                                                   Tree->String[k], 
                                                   &Location,
                                                   &PrevLocation);

      if (Rule == STADT_RULE3)
      {
         CurrentLocation->NodePointer = Location;
         CurrentLocation->DownEdgeCount = MIN(Gamma->Length - h, STADT_EdgeLength (CurrentLocation->NodePointer));
         k += CurrentLocation->DownEdgeCount;
         h += CurrentLocation->DownEdgeCount;
      }            
      else
      {
         printf("Error: missing an edge that is supposed to exist.\n");
         exit(1);
      }                                                
   }
}

/*********************************************************************************/

STADT_RULE_TYPE STADT_CheckExtensionRules (STADT_TREE_TYPE *Tree, 
                                             STADT_LOCATION_TYPE *CurrentLocation,
                                             ULONG NewIndex,
                                             BOOLEAN *NeedSuffixLinkCompletion)
{
   STADT_LOCATION_TYPE NewLocation;
   STADT_RULE_TYPE Rule;

   *NeedSuffixLinkCompletion = FALSE;
   
   if (CurrentLocation->DownEdgeCount < STADT_EdgeLength (CurrentLocation->NodePointer))
   {
      if (Tree->String[NewIndex] == 
          Tree->String[CurrentLocation->NodePointer->Info.EdgeStart + CurrentLocation->DownEdgeCount])
      {
         Rule = STADT_RULE3;
      }
      else
      {
         STADT_SplitEdge (Tree, CurrentLocation, &NewLocation);

         *CurrentLocation = NewLocation;
         *NeedSuffixLinkCompletion = TRUE;   

         Rule = STADT_ExtendSymbolFromNode (Tree, CurrentLocation->NodePointer, NewIndex, &NewLocation);

         if (Rule == STADT_RULE3)
         {
            printf("Error: Rule 3 should not happen here.\n");
            exit(1);
         }
      }
   }
   else
   {
      Rule = STADT_ExtendSymbolFromNode (Tree, CurrentLocation->NodePointer, NewIndex, &NewLocation);
   }

   return (Rule);
}

/*********************************************************************************/

STADT_RULE_TYPE STADT_ExtendSymbolFromNode (STADT_TREE_TYPE *Tree, 
                                             STADT_NODE_TYPE *Node, 
                                             ULONG Symbol,
                                             STADT_LOCATION_TYPE *NewLocation)
{
   STADT_NODE_TYPE *Location = NULL;
   STADT_NODE_TYPE *PrevLocation = NULL;
   STADT_RULE_TYPE Rule;
   
   Rule = STADT_FindNodeWithCorrectStartOfEdge (Tree, Node, Tree->String[Symbol], &Location, &PrevLocation);

   if (Rule == STADT_RULE2)
   {
      STADT_AddNewLeaf (Tree, PrevLocation, Location, Node, Symbol, NewLocation);
   }
   
   return (Rule);
}

/*********************************************************************************/

void STADT_UpdateSuffixLink (STADT_NODE_TYPE *Root,
                              STADT_NODE_TYPE *CurrentInternalNode,
                              BOOLEAN NeedSuffixLinkCompletion)
{
   /*
      Finish off the previously created internal node.
   */
   if (m_NeedToFinishSuffixLink == TRUE)
   {
      if (CurrentInternalNode != NULL)
      {
         m_PreviouslyCreatedInternalNode->SuffixLink = CurrentInternalNode;
      }
      else
      {
         m_PreviouslyCreatedInternalNode->SuffixLink = Root;
      }
   }
   
   /*
      Save the fact that there is a newly created internal node.
   */
   if (NeedSuffixLinkCompletion == TRUE)
   {
      m_PreviouslyCreatedInternalNode = CurrentInternalNode;
      m_NeedToFinishSuffixLink = TRUE;
   }
   else
   {
      m_PreviouslyCreatedInternalNode = NULL;
      m_NeedToFinishSuffixLink = FALSE;
   }
}

/*********************************************************************************/

void STADT_DeleteTree (STADT_TREE_TYPE *Tree)
{
   STADT_DeleteTreeFromNode (&(Tree->Tree));

   if (Tree->String != NULL)
   {
      free(Tree->String);
   }

   if (Tree->Reproducible != NULL)
   {
      free(Tree->Reproducible);
   }

   Tree->String = NULL;
   Tree->Reproducible = NULL;
}

/*********************************************************************************/

void STADT_DeleteTreeFromNode (STADT_NODE_TYPE **Tree)
{
   if (*Tree != NULL)
   {
      STADT_DeleteTreeFromNode (&((*Tree)->Child));
      STADT_DeleteTreeFromNode (&((*Tree)->NextSibling));
      free (*Tree);
      *Tree = NULL;
   }
}

/*********************************************************************************/

void STADT_PrintTree (STADT_TREE_TYPE *Tree)
{
   UBYTE *String;
   
   /*
      Allocate enough for worst case result that has a '.' between every
      character of the original string.
   */
   String = (UBYTE *) malloc (sizeof(UBYTE) * ((Tree->Length * 2) + 1));

   if (String == NULL)
   {
      printf("Error: malloc failed in STADT_PrintTree.\n");
      exit(1);
   }
   else
   {
      STADT_PrintTreeFromNode (Tree, Tree->Tree, String, 0, 0);
   
      free(String);
   }
}

/*********************************************************************************/

void STADT_PrintTreeFromNode (STADT_TREE_TYPE *Tree, 
                              STADT_NODE_TYPE *Node,
                              UBYTE *String,
                              ULONG StringLength,
                              ULONG SuffixLength)
{
   ULONG i;
   ULONG ParentStringLength = StringLength;
   ULONG ParentSuffixLength = SuffixLength;
   
   if (Node != NULL)
   {
      if (STADT_NodeIsRoot (Node) == TRUE)
      {
         String[0] = '.';
         StringLength = 1;
      }
      else if (STADT_NodeIsInternal (Node) == TRUE)
      {
         for (i = Node->Info.EdgeStart; i <= Node->Info.EdgeStop; i++)
         {
            if (i < Tree->Length)
            {
               String[StringLength] = Tree->String[i];
               StringLength++;
            }
            else
            {
               printf ("Error: internal tree node has incorrect stop index\n");
               exit (1);
            }
         }
         String[StringLength] = '.';
         StringLength++;
         
         SuffixLength += STADT_EdgeLength(Node);
      }
      else
      {
         for (i = Node->Info.EdgeStart; i <= Node->Info.EdgeStop; i++)
         {
            if (i < Tree->Length)
            {
               String[StringLength] = Tree->String[i];
               StringLength++;
               
               SuffixLength++;
            }
            else if (i == Tree->Length)
            {
               /*
                  This is a leaf node.
               */
            }
            else
            {
               printf ("Error: leaf node has incorrect stop index\n");
               exit (1);
            }
         }
         if (Node->Info.EdgeStart < Tree->Length)
         {
            String[StringLength] = '.';
            StringLength++;
         }

         printf("Suffix (%3.3lu): ", SuffixLength);
         for (i = 0; i < StringLength; i++)
         {
            printf("%c", String[i]);
         }
         printf("\n");
      }
            
      STADT_PrintTreeFromNode (Tree, Node->Child, String, StringLength, SuffixLength);
      STADT_PrintTreeFromNode (Tree, Node->NextSibling, String, ParentStringLength, ParentSuffixLength);
   }
}

/*********************************************************************************/

BOOLEAN STADT_FindSubstring (STADT_TREE_TYPE *Tree, 
                              UBYTE *String, 
                              ULONG Length)
{
   BOOLEAN Found = TRUE;
   STADT_NODE_TYPE *Parent = Tree->Tree;
   STADT_NODE_TYPE *Location;
   STADT_NODE_TYPE *PrevLocation;
   BOOLEAN Done = FALSE;
   ULONG index = 0;
   ULONG i;
   
   while (Done == FALSE)
   {
      if (STADT_FindNodeWithCorrectStartOfEdge (Tree, 
                                                Parent, 
                                                String[index], 
                                                &Location,
                                                &PrevLocation) == STADT_RULE3)
      {
         i = Location->Info.EdgeStart;
         while ((i <= Location->Info.EdgeStop) && (index < Length) && (Done == FALSE))
         {
            if (i < Tree->Length)
            {
               if (String[index] != Tree->String[i])
               {
                  Done = TRUE;
                  Found = FALSE;
               }
               else
               {
                  index++;
               }
            }
            else
            {
               Done = TRUE;
               Found = FALSE;
            }
            
            i++;
         }
         
         if (index >= Length)
         {
            Found = TRUE;
            Done = TRUE;
         }
         else
         {
            Parent = Location;
         }
      }
      else
      {
         Found = FALSE;
         Done = TRUE;
      }
   }
                                    
   return (Found);
}
