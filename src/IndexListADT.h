/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _INDEXLISTADT
#define INDEXLISTADT_EXTERN
#else
#define INDEXLISTADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef enum
{
   INDEXLISTADT_MINMAX_MIN,
   INDEXLISTADT_MINMAX_MAX
} INDEXLISTADT_MINMAX_TYPE;

typedef struct
{
   INDEXLISTADT_MINMAX_TYPE MinMaxType;
   ULONG MaxLength;
   ULONG Length;
   FLOAT *Scores;
   ULONG *Indicies;
} INDEXLISTADT_INDEXLIST_TYPE;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/

void INDEXLISTADT_InitializeIndexList(INDEXLISTADT_INDEXLIST_TYPE *IndexList, 
                                       ULONG MaxLength,
                                       INDEXLISTADT_MINMAX_TYPE MinMaxType,
                                       BYTE *CallingFunction);
void INDEXLISTADT_CleanUpIndexList(INDEXLISTADT_INDEXLIST_TYPE *IndexList);
void INDEXLISTADT_InsertIndexIntoList(INDEXLISTADT_INDEXLIST_TYPE *IndexList, 
                                       ULONG Index,
                                       FLOAT Score);
