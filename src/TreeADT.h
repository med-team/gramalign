/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _TREEADT
#define TREEADT_EXTERN
#else
#define TREEADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef ULONG TREEADT_KEY_TYPE;

typedef struct
{
   TREEADT_KEY_TYPE Key;
} TREEADT_ELEMENT_TYPE;

typedef struct TREEADT_node
{
   TREEADT_ELEMENT_TYPE Info;
   struct TREEADT_node *Child;
   struct TREEADT_node *Sibling;
   struct TREEADT_node *Parent;
} TREEADT_NODE_TYPE, *TREEADT_NODE_POINTER_TYPE;

typedef TREEADT_NODE_POINTER_TYPE TREEADT_TREE_TYPE;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/
void TREEADT_CreateTree(TREEADT_TREE_TYPE *Tree, TREEADT_ELEMENT_TYPE FirstElement);
void TREEADT_PrintTree(TREEADT_TREE_TYPE Tree);
void TREEADT_AddNodeToTree(TREEADT_TREE_TYPE Tree, 
                           TREEADT_ELEMENT_TYPE NewElement, 
                           TREEADT_ELEMENT_TYPE ParentElement);
void TREEADT_DeleteTree(TREEADT_TREE_TYPE *Tree);
BOOLEAN TREEADT_FindNode(TREEADT_TREE_TYPE Tree, 
                        TREEADT_KEY_TYPE KeyValue, 
                        TREEADT_NODE_POINTER_TYPE *NodePointer);

