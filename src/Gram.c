/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _GRAM

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "StringADT.h"
#include "StringListADT.h"
#include "SparseMatrixFloatADT.h"
#include "IndexListADT.h"
#include "SuffixTreeADT.h"
#include "SuffixTreeListADT.h"
#include "Gram.h"
#include "Parameters.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

#define GRAM_MAXIMUM_GRAMMAR_DISTANCE  2.0
#define GRAM_NUMBER_OF_CLUSTERS        2
#define GRAM_CLUSTER_OVERLAP_AMOUNT    1
#define GRAM_CLUSTER_MINIMUM_LENGTH    2
#define GRAM_PROGRESS_LENGTH_MIN       500

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/
static void GRAM_InitializeDictionaries(ULONG N);
static void GRAM_CalculateCompleteDistanceMatrix(ULONG N);
static void GRAM_CalculatePartialDistanceMatrix(ULONG N);
static void GRAM_DistanceRecursion (ULONG Col, INDEXLISTADT_INDEXLIST_TYPE *Rows);
static FLOAT GRAM_CalculateSingleDistance (ULONG Col, ULONG Row);
static void GRAM_CreateLZDictionary(STADT_TREE_TYPE *Sequence, 
                                    FLOAT *DictionarySize);
static BOOLEAN GRAM_LZAdd(BYTE Character, 
                           FLOAT *CurrentDictionarySize, 
                           STRINGADT_STRING_TYPE *CurrentFragment,
                           STRINGADT_STRING_TYPE *CurrentSequence,
                           ULONG *CurrentPositionIndex);
static FLOAT GRAM_LZComparePart(STADT_TREE_TYPE *Sequence1, 
                                 FLOAT DictionarySize1,
                                 STADT_TREE_TYPE *Sequence2);
static BOOLEAN GRAM_LZScan(STADT_TREE_TYPE *Sequence1,
                           STADT_TREE_TYPE *Sequence2,
                           STRINGADT_STRING_TYPE *CurrentFragment,
                           ULONG Index);

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/
static STRINGLISTADT_STRINGLIST_TYPE *m_Sequences;
static SMF_ADT_MATRIX_TYPE *m_DistanceMatrix;
static FLOAT *m_DictionarySizes;
static STLADT_TREELIST_TYPE m_SuffixTrees;

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void GRAM_CalculateDistances(STRINGLISTADT_STRINGLIST_TYPE *Sequences, 
                              SMF_ADT_MATRIX_TYPE *DistanceMatrix)
{
   ULONG N = Sequences->NumberOfStrings;

   m_Sequences = Sequences;
   m_DistanceMatrix = DistanceMatrix;

   SMF_ADT_DefineMatrix(m_DistanceMatrix, N, N, GRAM_MAXIMUM_GRAMMAR_DISTANCE);

   /*
      Begin by making all of the individual dictionaries.  In this case, we do not
      actually use the dictionary anywhere, so we will just keep track of the 
      number of entries in each dictionary.
   */
   m_DictionarySizes = (FLOAT *) malloc (sizeof(FLOAT) * N);
   if (m_DictionarySizes == NULL)
   {
      printf("Error: malloc failed in GRAM_CalculateDistances.\n");
      exit(1);
   }
   
   STLADT_InitializeTreeList(&m_SuffixTrees);

   GRAM_InitializeDictionaries(N);

   if (PARMS_GetGenerateCompleteDistanceMatrix() == TRUE)
   {
      GRAM_CalculateCompleteDistanceMatrix(N);
   }
   else
   {
      GRAM_CalculatePartialDistanceMatrix(N);
   }

   STLADT_CleanUpTreeList(&m_SuffixTrees);
   
   /*
      Deallocate all of the dictionaries.
   */
   if (m_DictionarySizes != NULL)
   {
      free(m_DictionarySizes);
   }
}

/*********************************************************************************/

void GRAM_InitializeDictionaries(ULONG N)
{
   ULONG i;

   for (i = 0; i < N; i++)
   {
      STLADT_AppendStringToTreeList(&m_SuffixTrees, &(m_Sequences->Strings[i]));

      m_DictionarySizes[i] = 0;
      GRAM_CreateLZDictionary(m_SuffixTrees.Trees[i], &(m_DictionarySizes[i]));

      /*
         Initialize the diagonal to 0.0.
      */
      SMF_ADT_SetElement(m_DistanceMatrix, 0.0, i, i);

      PARMS_PrintfProgress(i, N);
   }
   PARMS_PrintfProgress(i, N);
   
}

/*********************************************************************************/

void GRAM_CalculateCompleteDistanceMatrix(ULONG N)
{
   ULONG i;
   ULONG j;

   /*
      See how many additional entries are needed between each pair of sequences.
   */
   for (i = 0; i < (N - 1); i++)
   {
      for (j = (i + 1); j < N; j++)
      {
         GRAM_CalculateSingleDistance (i, j);
      }
      PARMS_PrintfProgress(i, (N - 1));
   }
   PARMS_PrintfProgress(i, (N - 1));
}

/*********************************************************************************/

void GRAM_CalculatePartialDistanceMatrix(ULONG N)
{
   ULONG i;
   INDEXLISTADT_INDEXLIST_TYPE FirstDistanceList;

   INDEXLISTADT_InitializeIndexList(&FirstDistanceList, N, INDEXLISTADT_MINMAX_MIN, "GRAM_CalculatePartialDistanceMatrix");

   for (i = 0; i < N; i++)
   {
      INDEXLISTADT_InsertIndexIntoList(&FirstDistanceList, i, 0.0);

      PARMS_PrintfProgress(i, N);
   }
   PARMS_PrintfProgress(i, N);

   GRAM_DistanceRecursion (0, &FirstDistanceList);

   INDEXLISTADT_CleanUpIndexList(&FirstDistanceList);
}

/*********************************************************************************/

void GRAM_DistanceRecursion (ULONG Col, INDEXLISTADT_INDEXLIST_TYPE *Rows)
{
   INDEXLISTADT_INDEXLIST_TYPE MinList;
   INDEXLISTADT_INDEXLIST_TYPE MaxList;
   ULONG i;
   ULONG j;
   FLOAT MaxScore = 0.0;
   FLOAT MinScore = 0.0;
   ULONG MaxCol = 0;
   ULONG MinCol = 0;
   BOOLEAN FirstEntry;
   FLOAT Distance;
   ULONG NewListSize;
   
   if (Rows->MaxLength > GRAM_CLUSTER_MINIMUM_LENGTH)
   {
      NewListSize = MAX((Rows->MaxLength / GRAM_NUMBER_OF_CLUSTERS) + GRAM_CLUSTER_OVERLAP_AMOUNT, 
                        GRAM_CLUSTER_MINIMUM_LENGTH);
      
      INDEXLISTADT_InitializeIndexList(&MinList, 
                                       NewListSize, 
                                       INDEXLISTADT_MINMAX_MIN, 
                                       "GRAM_DistanceRecursion");
      INDEXLISTADT_InitializeIndexList(&MaxList, 
                                       NewListSize, 
                                       INDEXLISTADT_MINMAX_MAX, 
                                       "GRAM_DistanceRecursion");

      FirstEntry = TRUE;
      for (i = 0; i < Rows->Length; i++)
      {
         if (Col != Rows->Indicies[i])
         {
            Distance = GRAM_CalculateSingleDistance (Col, Rows->Indicies[i]);

            INDEXLISTADT_InsertIndexIntoList(&MinList, Rows->Indicies[i], Distance);
            INDEXLISTADT_InsertIndexIntoList(&MaxList, Rows->Indicies[i], Distance);
            
            if (FirstEntry == TRUE)
            {
               FirstEntry = FALSE;
               
               MaxScore = Distance;
               MinScore = Distance;
               MaxCol = Rows->Indicies[i];
               MinCol = Rows->Indicies[i];   
            }
            else
            {
               if (Distance > MaxScore)
               {
                  MaxScore = Distance;
                  MaxCol = Rows->Indicies[i];
               }
               
               if (Distance < MinScore)
               {
                  MinScore = Distance;
                  MinCol = Rows->Indicies[i];
               }
            }
         }
         if (Rows->Length > GRAM_PROGRESS_LENGTH_MIN)
         {
            PARMS_PrintfProgress(i, Rows->Length);
         }
      }
      if (Rows->Length > GRAM_PROGRESS_LENGTH_MIN)
      {
         PARMS_PrintfProgress(i, Rows->Length);
      }
      
      GRAM_DistanceRecursion (MinCol, &MinList);
      GRAM_DistanceRecursion (MaxCol, &MaxList);
      
      INDEXLISTADT_CleanUpIndexList(&MinList);
      INDEXLISTADT_CleanUpIndexList(&MaxList);
   }
   else
   {
      for (i = 0; i < (Rows->Length - 1); i++)
      {
         for (j = (i + 1); j < Rows->Length; j++)
         {
            GRAM_CalculateSingleDistance (Rows->Indicies[i], Rows->Indicies[j]);
         }
      }
   }
}

/*********************************************************************************/

FLOAT GRAM_CalculateSingleDistance (ULONG Col, ULONG Row)
{
   FLOAT ComplexitySQ;
   FLOAT ComplexityQS;
   FLOAT ComplexityS;
   FLOAT ComplexityQ;
   FLOAT Distance;

   ComplexitySQ = GRAM_LZComparePart(m_SuffixTrees.Trees[Col], m_DictionarySizes[Col], m_SuffixTrees.Trees[Row]);
   ComplexityQS = GRAM_LZComparePart(m_SuffixTrees.Trees[Row], m_DictionarySizes[Row], m_SuffixTrees.Trees[Col]);
   ComplexityS = m_DictionarySizes[Col];
   ComplexityQ = m_DictionarySizes[Row];
   
   Distance = (ComplexitySQ - ComplexityS + ComplexityQS - ComplexityQ) / ((ComplexitySQ + ComplexityQS) / 2);

   SMF_ADT_SetElement(m_DistanceMatrix, Distance, Col, Row);
   SMF_ADT_SetElement(m_DistanceMatrix, Distance, Row, Col);

   return (Distance);
}

/*********************************************************************************/

void GRAM_CreateLZDictionary(STADT_TREE_TYPE *Sequence, 
                              FLOAT *DictionarySize)
{
   ULONG i;
   STRINGADT_STRING_TYPE BuildSequence;
   STRINGADT_STRING_TYPE Fragment;
   ULONG PositionIndex;
   
   STRINGADT_InitializeString(&BuildSequence);
   STRINGADT_InitializeString(&Fragment);
   PositionIndex = 0;

   /*
      Subtract one due to the extra terminator in the Suffix Tree string.
   */   
   for (i = 0; i < (Sequence->Length - 1); i++)
   {
      Sequence->Reproducible[i] = GRAM_LZAdd(Sequence->String[i],
                                             DictionarySize,
                                             &Fragment,
                                             &BuildSequence,
                                             &PositionIndex);
   }

   STRINGADT_CleanUpString(&BuildSequence);
   STRINGADT_CleanUpString(&Fragment);
}

/*********************************************************************************/

BOOLEAN GRAM_LZAdd(BYTE Character, 
                  FLOAT *CurrentDictionarySize, 
                  STRINGADT_STRING_TYPE *CurrentFragment,
                  STRINGADT_STRING_TYPE *CurrentSequence,
                  ULONG *CurrentPositionIndex)
{
   BOOLEAN Reproducible = FALSE;

   STRINGADT_AppendStringFromChar(CurrentFragment, Character, "GRAM_LZAdd");

   if (*CurrentDictionarySize == 0)
   {
      *CurrentDictionarySize = (*CurrentDictionarySize) + 1;      
      STRINGADT_CleanUpString(CurrentFragment);
      STRINGADT_InitializeString(CurrentFragment);
      *CurrentPositionIndex = 0;
   }
   else
   {
      Reproducible = STRINGADT_FindString(CurrentSequence, CurrentFragment, CurrentPositionIndex);
      if (Reproducible == FALSE)
      {
         *CurrentDictionarySize = (*CurrentDictionarySize) + 1;      
         STRINGADT_CleanUpString(CurrentFragment);
         STRINGADT_InitializeString(CurrentFragment);
         *CurrentPositionIndex = 0;
      }
   }
   
   STRINGADT_AppendStringFromChar(CurrentSequence, Character, "GRAM_LZAdd");

   return (Reproducible);
}

/*********************************************************************************/

FLOAT GRAM_LZComparePart(STADT_TREE_TYPE *Sequence1, 
                           FLOAT DictionarySize1,
                           STADT_TREE_TYPE *Sequence2)
{
   ULONG i;
   FLOAT DictionarySize;
   STRINGADT_STRING_TYPE Fragment;

   STRINGADT_InitializeString(&Fragment);
   DictionarySize = DictionarySize1;

   for (i = 0; i < Sequence2->Length; i++)
   {
      if (GRAM_LZScan(Sequence1, Sequence2, &Fragment, i) == FALSE)
      {
         DictionarySize++;
      }
   }

   STRINGADT_CleanUpString(&Fragment);

   return (DictionarySize);
}

/*********************************************************************************/

BOOLEAN GRAM_LZScan(STADT_TREE_TYPE *Sequence1,
                     STADT_TREE_TYPE *Sequence2,
                     STRINGADT_STRING_TYPE *CurrentFragment,
                     ULONG Index)
{
   BOOLEAN Reproducible = TRUE;

   STRINGADT_AppendStringFromChar(CurrentFragment, Sequence2->String[Index], "GRAM_LZScan");

   if (Sequence2->Reproducible[Index] == FALSE)
   {
      Reproducible = STADT_FindSubstring (Sequence1, CurrentFragment->Characters, CurrentFragment->Length);

      STRINGADT_CleanUpString(CurrentFragment);
      STRINGADT_InitializeString(CurrentFragment);
   }
   
   return (Reproducible);
}

