/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _STRINGLISTADT
#define STRINGLISTADT_EXTERN
#else
#define STRINGLISTADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef struct
{
   ULONG NumberOfStrings;
   STRINGADT_STRING_TYPE *Strings;
} STRINGLISTADT_STRINGLIST_TYPE;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/

void STRINGLISTADT_InitializeStringList(STRINGLISTADT_STRINGLIST_TYPE *StringList);
void STRINGLISTADT_CleanUpStringList(STRINGLISTADT_STRINGLIST_TYPE *StringList);
void STRINGLISTADT_AppendStringToStringList(STRINGLISTADT_STRINGLIST_TYPE *StringList, 
                                             STRINGADT_STRING_TYPE *String, 
                                             BYTE *CallingFunction);
