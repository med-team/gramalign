/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _FILEIO
#define FILEIO_EXTERN
#else
#define FILEIO_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/
void FILEIO_ReadFASTASequences(STRINGLISTADT_STRINGLIST_TYPE *Sequences,
                                 STRINGLISTADT_STRINGLIST_TYPE *MergedAlphabetSequences,
                                 STRINGLISTADT_STRINGLIST_TYPE *HeaderLines);
void FILEIO_ReadFASTAGrammarPieces(STRINGLISTADT_STRINGLIST_TYPE *GrammarPieces,
                                    STRINGLISTADT_STRINGLIST_TYPE *HeaderLines);
void FILEIO_WriteDistanceMatrix(SMF_ADT_MATRIX_TYPE *DistanceMatrix);
void FILEIO_WriteAlignmentInPHYLIPFormat(STRINGLISTADT_STRINGLIST_TYPE *AlignedStrings,
                                          STRINGLISTADT_STRINGLIST_TYPE *HeaderLines,
                                          BYTE *OutputFileName);
void FILEIO_WriteAlignmentInFASTAFormat(STRINGLISTADT_STRINGLIST_TYPE *AlignedStrings,
                                          STRINGLISTADT_STRINGLIST_TYPE *HeaderLines,
                                          BYTE *OutputFileName);
void FILEIO_WriteAlignmentInMSF_GCGFormat(STRINGLISTADT_STRINGLIST_TYPE *AlignedStrings,
                                          STRINGLISTADT_STRINGLIST_TYPE *HeaderLines,
                                          BYTE *OutputFileName);
void FILEIO_WriteConsensusSequence(STRINGADT_STRING_TYPE *ConsensusString,
                                   FLOAT *ConsensusConfidence);
void FILEIO_WriteTraceBackMatrixPage(MATRIXUBYTEADT_MATRIX_TYPE *TraceBackMatrix, 
                                       ULONG PageNumber);
void FILEIO_ReadTraceBackMatrixPage(MATRIXUBYTEADT_MATRIX_TYPE *TraceBackMatrix, 
                                    ULONG PageNumber);
void FILEIO_DeleteTraceBackMatrixPage(ULONG PageNumber);

