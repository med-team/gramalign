/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _STLADT

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "StringADT.h"
#include "SuffixTreeADT.h"
#include "SuffixTreeListADT.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/
#define STLADT_CHUNK_SIZE 64

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void STLADT_InitializeTreeList(STLADT_TREELIST_TYPE *List)
{
   List->NumberOfTreesAllocated = 0;
   List->NumberOfTrees = 0;
   List->Trees = NULL;
}

/*********************************************************************************/

void STLADT_CleanUpTreeList(STLADT_TREELIST_TYPE *List)
{
   ULONG i;

   for (i = 0; i < List->NumberOfTrees; i++)
   {
      if (List->Trees[i] != NULL)
      {
         STADT_DeleteTree (List->Trees[i]);
         free(List->Trees[i]);
      }
   }

   if (List->Trees != NULL)
   {
      free(List->Trees);
   }
   
   List->NumberOfTreesAllocated = 0;
   List->NumberOfTrees = 0;
   List->Trees = NULL;
}

/*********************************************************************************/

void STLADT_AppendStringToTreeList(STLADT_TREELIST_TYPE *List, 
                                    STRINGADT_STRING_TYPE *String)
{
   if (List->NumberOfTrees == 0)
   {
      List->Trees = (STADT_TREE_TYPE **) malloc(sizeof(STADT_TREE_TYPE *) * STLADT_CHUNK_SIZE);
      if (List->Trees == NULL)
      {
         printf("Error: malloc failed in STLADT_AppendStringToTreeList.\n");
         exit(1);
      }
      List->NumberOfTreesAllocated = STLADT_CHUNK_SIZE;
   }
   else if (List->NumberOfTrees >= List->NumberOfTreesAllocated)
   {
      List->Trees = (STADT_TREE_TYPE **) realloc(List->Trees, sizeof(STADT_TREE_TYPE *) * (List->NumberOfTreesAllocated + STLADT_CHUNK_SIZE));
      if (List->Trees == NULL)
      {
         printf("Error: realloc failed in STLADT_AppendStringToTreeList.\n");
         exit(1);
      }
      List->NumberOfTreesAllocated += STLADT_CHUNK_SIZE;
   }

   List->NumberOfTrees += 1;

   List->Trees[List->NumberOfTrees - 1] = (STADT_TREE_TYPE *) malloc (sizeof (STADT_TREE_TYPE));
   if (List->Trees[List->NumberOfTrees - 1] == NULL)
   {
      printf("Error: malloc failed in STLADT_AppendStringToTreeList.\n");
      exit(1);
   }

   STADT_CreateTree (List->Trees[List->NumberOfTrees - 1], 
                        String->Characters, 
                        String->Length,
                        '\0');
}
