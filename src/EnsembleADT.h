/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _ENSEMBLEADT
#define ENSEMBLEADT_EXTERN
#else
#define ENSEMBLEADT_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef struct
{
   BYTE *Characters;
   FLOAT *SubCostRow;
   FLOAT IxGapCost;
   FLOAT IyGapCost;
   ULONG NumberOfGaps;

   UBYTE *GrammarPieces;
   UBYTE MaxGrammarPieceLength;
} ENSEMBLEADT_ELEMENT_TYPE;

typedef struct ENSEMBLEADT_node
{
   ENSEMBLEADT_ELEMENT_TYPE Info;
   struct ENSEMBLEADT_node *Next;
   struct ENSEMBLEADT_node *Prev;
} ENSEMBLEADT_NODE_TYPE;

typedef struct
{
   ENSEMBLEADT_NODE_TYPE *Start;
   ENSEMBLEADT_NODE_TYPE *End;
   ULONG NumberOfSequences;
   ULONG CurrentLength;
   ENSEMBLEADT_NODE_TYPE *TBNode;
   ENSEMBLEADT_NODE_TYPE *TFNode;
} ENSEMBLEADT_TYPE;

typedef FLOAT (* ENSEMBLEADT_SUBSTITUTION_COST_FUNCTION)(ENSEMBLEADT_TYPE *Ensemble, BYTE Residue);

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/
ENSEMBLEADT_EXTERN FLOAT ENSEMBLEADT_InDelGapExtensionCost;
ENSEMBLEADT_EXTERN FLOAT ENSEMBLEADT_InDelTailGapStartCost;
ENSEMBLEADT_EXTERN FLOAT ENSEMBLEADT_InDelTailGapExtensionCost;
ENSEMBLEADT_EXTERN ENSEMBLEADT_SUBSTITUTION_COST_FUNCTION ENSEMBLEADT_SubstitutionCost;

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/
void ENSEMBLEADT_Initialize(ULONG NumberOfSequences);
void ENSEMBLEADT_InitializeEnsemble(ENSEMBLEADT_TYPE *Ensemble,
                                    STRINGADT_STRING_TYPE *FirstSequence,
                                    STRINGADT_STRING_TYPE *FirstGrammarPieceSequence,
                                    ULONG FirstSequenceNumber);
void ENSEMBLEADT_CleanUp(ENSEMBLEADT_TYPE *Ensemble);
void ENSEMBLEADT_PerformTraceBackM(ENSEMBLEADT_TYPE *Ensemble,
                                   BYTE Residue, 
                                   UBYTE PieceLength, 
                                   ULONG IndexInEnsemble,
                                   ULONG NumberAddedInRound);
void ENSEMBLEADT_PerformTraceBackIy(ENSEMBLEADT_TYPE *Ensemble, 
                                    ULONG IndexInEnsemble,
                                    ULONG NumberAddedInRound);
void ENSEMBLEADT_PerformTraceBackIyFinal(ENSEMBLEADT_TYPE *Ensemble, 
                                         ULONG IndexInEnsemble,
                                         ULONG NumberAddedInRound);
void ENSEMBLEADT_PerformTraceBackIx(ENSEMBLEADT_TYPE *Ensemble, 
                                    BYTE Residue, 
                                    UBYTE PieceLength, 
                                    ULONG IndexInEnsemble);
void ENSEMBLEADT_PerformTraceBackIxFinal(ENSEMBLEADT_TYPE *Ensemble, 
                                         BYTE Residue, 
                                         UBYTE PieceLength, 
                                         ULONG IndexInEnsemble);
void ENSEMBLEADT_GetPrintableAlignment(ENSEMBLEADT_TYPE *Ensemble, 
                                       STRINGLISTADT_STRINGLIST_TYPE *AlignedStrings,
                                       STRINGLISTADT_STRINGLIST_TYPE *AlignedGrammarPieces,
                                       GROUPARRAYADT_TYPE *SequenceAlignmentOrder,
                                       BYTE GapCharacter);
void ENSEMBLEADT_AdjustMSAGaps(ENSEMBLEADT_TYPE *Ensemble);
void ENSEMBLEADT_GetConsensusString(ENSEMBLEADT_TYPE *Ensemble, 
                                    STRINGADT_STRING_TYPE *ConsensusString,
                                    FLOAT **ConsensusConfidence,
                                    BOOLEAN IncludeGaps,
                                    BYTE GapCharacter,
                                    STRINGADT_STRING_TYPE *ConsensusGrammarPieces,
                                    BOOLEAN GetConsensusGrammarPieces);
FLOAT ENSEMBLEADT_GrammarPieceCost(ENSEMBLEADT_TYPE *Ensemble, UBYTE GrammarPieceLength);
