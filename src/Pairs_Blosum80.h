/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/

/*
   Obtained from ftp://ftp.ncbi.nlm.nih.gov/blast/matrices/BLOSUM80
*/
static const ENSEMBLEADT_AA_MATRIX_TYPE m_BLOSUM80SubMatrix =
{
   { /*    A,    B,    C,    D,    E,    F,    G,    H,    I,    K,    L,    M,    N,    P,    Q,    R,    S,    T,    V,    W,    X,    Y,    Z,  Unknown */
      {  7.0, -3.0, -1.0, -3.0, -2.0, -4.0,  0.0, -3.0, -3.0, -1.0, -3.0, -2.0, -3.0, -1.0, -2.0, -3.0,  2.0,  0.0, -1.0, -5.0, -1.0, -4.0, -2.0, -8.0},   /* A */
      { -3.0,  6.0, -6.0,  6.0,  1.0, -6.0, -2.0, -1.0, -6.0, -1.0, -7.0, -5.0,  5.0, -4.0, -1.0, -2.0,  0.0, -1.0, -6.0, -8.0, -3.0, -5.0,  0.0, -8.0},   /* B */
      { -1.0, -6.0, 13.0, -7.0, -7.0, -4.0, -6.0, -7.0, -2.0, -6.0, -3.0, -3.0, -5.0, -6.0, -5.0, -6.0, -2.0, -2.0, -2.0, -5.0, -4.0, -5.0, -7.0, -8.0},   /* C */
      { -3.0,  6.0, -7.0, 10.0,  2.0, -6.0, -3.0, -2.0, -7.0, -2.0, -7.0, -6.0,  2.0, -3.0, -1.0, -3.0, -1.0, -2.0, -6.0, -8.0, -3.0, -6.0,  1.0, -8.0},   /* D */
      { -2.0,  1.0, -7.0,  2.0,  8.0, -6.0, -4.0,  0.0, -6.0,  1.0, -6.0, -4.0, -1.0, -2.0,  3.0, -1.0, -1.0, -2.0, -4.0, -6.0, -2.0, -5.0,  6.0, -8.0},   /* E */
      { -4.0, -6.0, -4.0, -6.0, -6.0, 10.0, -6.0, -2.0, -1.0, -5.0,  0.0,  0.0, -6.0, -6.0, -5.0, -5.0, -4.0, -4.0, -2.0,  0.0, -3.0,  4.0, -6.0, -8.0},   /* F */
      {  0.0, -2.0, -6.0, -3.0, -4.0, -6.0,  9.0, -4.0, -7.0, -3.0, -7.0, -5.0, -1.0, -5.0, -4.0, -4.0, -1.0, -3.0, -6.0, -6.0, -3.0, -6.0, -4.0, -8.0},   /* G */
      { -3.0, -1.0, -7.0, -2.0,  0.0, -2.0, -4.0, 12.0, -6.0, -1.0, -5.0, -4.0,  1.0, -4.0,  1.0,  0.0, -2.0, -3.0, -5.0, -4.0, -2.0,  3.0,  0.0, -8.0},   /* H */
      { -3.0, -6.0, -2.0, -7.0, -6.0, -1.0, -7.0, -6.0,  7.0, -5.0,  2.0,  2.0, -6.0, -5.0, -5.0, -5.0, -4.0, -2.0,  4.0, -5.0, -2.0, -3.0, -6.0, -8.0},   /* I */
      { -1.0, -1.0, -6.0, -2.0,  1.0, -5.0, -3.0, -1.0, -5.0,  8.0, -4.0, -3.0,  0.0, -2.0,  2.0,  3.0, -1.0, -1.0, -4.0, -6.0, -2.0, -4.0,  1.0, -8.0},   /* K */
      { -3.0, -7.0, -3.0, -7.0, -6.0,  0.0, -7.0, -5.0,  2.0, -4.0,  6.0,  3.0, -6.0, -5.0, -4.0, -4.0, -4.0, -3.0,  1.0, -4.0, -2.0, -2.0, -5.0, -8.0},   /* L */
      { -2.0, -5.0, -3.0, -6.0, -4.0,  0.0, -5.0, -4.0,  2.0, -3.0,  3.0,  9.0, -4.0, -4.0, -1.0, -3.0, -3.0, -1.0,  1.0, -3.0, -2.0, -3.0, -3.0, -8.0},   /* M */
      { -3.0,  5.0, -5.0,  2.0, -1.0, -6.0, -1.0,  1.0, -6.0,  0.0, -6.0, -4.0,  9.0, -4.0,  0.0, -1.0,  1.0,  0.0, -5.0, -7.0, -2.0, -4.0, -1.0, -8.0},   /* N */
      { -1.0, -4.0, -6.0, -3.0, -2.0, -6.0, -5.0, -4.0, -5.0, -2.0, -5.0, -4.0, -4.0, 12.0, -3.0, -3.0, -2.0, -3.0, -4.0, -7.0, -3.0, -6.0, -2.0, -8.0},   /* P */
      { -2.0, -1.0, -5.0, -1.0,  3.0, -5.0, -4.0,  1.0, -5.0,  2.0, -4.0, -1.0,  0.0, -3.0,  9.0,  1.0, -1.0, -1.0, -4.0, -4.0, -2.0, -3.0,  5.0, -8.0},   /* Q */
      { -3.0, -2.0, -6.0, -3.0, -1.0, -5.0, -4.0,  0.0, -5.0,  3.0, -4.0, -3.0, -1.0, -3.0,  1.0,  9.0, -2.0, -2.0, -4.0, -5.0, -2.0, -4.0,  0.0, -8.0},   /* R */
      {  2.0,  0.0, -2.0, -1.0, -1.0, -4.0, -1.0, -2.0, -4.0, -1.0, -4.0, -3.0,  1.0, -2.0, -1.0, -2.0,  7.0,  2.0, -3.0, -6.0, -1.0, -3.0, -1.0, -8.0},   /* S */
      {  0.0, -1.0, -2.0, -2.0, -2.0, -4.0, -3.0, -3.0, -2.0, -1.0, -3.0, -1.0,  0.0, -3.0, -1.0, -2.0,  2.0,  8.0,  0.0, -5.0, -1.0, -3.0, -2.0, -8.0},   /* T */
      { -1.0, -6.0, -2.0, -6.0, -4.0, -2.0, -6.0, -5.0,  4.0, -4.0,  1.0,  1.0, -5.0, -4.0, -4.0, -4.0, -3.0,  0.0,  7.0, -5.0, -2.0, -3.0, -4.0, -8.0},   /* V */
      { -5.0, -8.0, -5.0, -8.0, -6.0,  0.0, -6.0, -4.0, -5.0, -6.0, -4.0, -3.0, -7.0, -7.0, -4.0, -5.0, -6.0, -5.0, -5.0, 16.0, -5.0,  3.0, -5.0, -8.0},   /* W */
      { -1.0, -3.0, -4.0, -3.0, -2.0, -3.0, -3.0, -2.0, -2.0, -2.0, -2.0, -2.0, -2.0, -3.0, -2.0, -2.0, -1.0, -1.0, -2.0, -5.0, -2.0, -3.0, -1.0, -8.0},   /* X */
      { -4.0, -5.0, -5.0, -6.0, -5.0,  4.0, -6.0,  3.0, -3.0, -4.0, -2.0, -3.0, -4.0, -6.0, -3.0, -4.0, -3.0, -3.0, -3.0,  3.0, -3.0, 11.0, -4.0, -8.0},   /* Y */
      { -2.0,  0.0, -7.0,  1.0,  6.0, -6.0, -4.0,  0.0, -6.0,  1.0, -5.0, -3.0, -1.0, -2.0,  5.0,  0.0, -1.0, -2.0, -4.0, -5.0, -1.0, -4.0,  6.0, -8.0},   /* Z */
      { -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0, -8.0,  1.0},   /* Unknown */
   }
};























