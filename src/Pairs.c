/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#define _PAIRS

/*********************************************************************************/
/* Included Header Files */
/*********************************************************************************/
#include "Globals.h"
#include "CoinFlip.h"
#include "MatrixUByteADT.h"
#include "MatrixFloatADT.h"
#include "SparseMatrixFloatADT.h"
#include "StringADT.h"
#include "StringListADT.h"
#include "GroupArrayADT.h"
#include "EnsembleADT.h"
#include "Pairs.h"
#include "FileIO.h"
#include "Parameters.h"

/*********************************************************************************/
/* Private Module Constants */
/*********************************************************************************/

#define PAIRS_TWO_ROW_MATRIX           2
#define PAIRS_GAP_COL_ROW_AMOUNT       1

#define PAIRS_PAGE_ONE_MILLION_BYTES   (FLOAT) 1000000

#define PAIRS_M_POS_FINAL              0x00
#define PAIRS_M_POS_M                  0x01
#define PAIRS_M_POS_IX                 0x02
#define PAIRS_M_POS_IY                 0x03

#define PAIRS_IX_POS_FINAL             0x00
#define PAIRS_IX_POS_M                 0x04
#define PAIRS_IX_POS_IX                0x08

#define PAIRS_IY_POS_FINAL             0x00
#define PAIRS_IY_POS_M                 0x10
#define PAIRS_IY_POS_IY                0x30

#define PAIRS_MASK_M                   0x03
#define PAIRS_SHIFT_IX                 2
#define PAIRS_MASK_IX                  0x0C
#define PAIRS_SHIFT_IY                 4
#define PAIRS_MASK_IY                  0x30

#define PAIRS_GET_M_VALUE(a)           (a & PAIRS_MASK_M)
#define PAIRS_GET_IX_VALUE(a)          ((a & PAIRS_MASK_IX) >> PAIRS_SHIFT_IX)
#define PAIRS_GET_IY_VALUE(a)          ((a & PAIRS_MASK_IY) >> PAIRS_SHIFT_IY)

#define PAIRS_SET_VALUE(a,v)           (a |= v)
#define PAIRS_DP_SET_VALUE(Cost1,Cost2,ScoreCell,TBCell,Direction1,Direction2)   \
{                                                                                \
   if (Cost1 > Cost2)                                                            \
   {                                                                             \
      ScoreCell = Cost1;                                                         \
      (TBCell |= Direction1);                                                    \
   }                                                                             \
   else if (Cost1 < Cost2)                                                       \
   {                                                                             \
      ScoreCell = Cost2;                                                         \
      (TBCell |= Direction2);                                                    \
   }                                                                             \
   else                                                                          \
   {                                                                             \
      ScoreCell = Cost1;                                                         \
      if (COIN_FLIP)                                                             \
      {                                                                          \
         (TBCell |= Direction2);                                                 \
      }                                                                          \
      else                                                                       \
      {                                                                          \
         (TBCell |= Direction1);                                                 \
      }                                                                          \
   }                                                                             \
}

/*********************************************************************************/
/* Private Type Definitions */
/*********************************************************************************/
typedef enum
{
   PAIRS_FINAL = 0,
   PAIRS_M,
   PAIRS_IX,
   PAIRS_IY
} PAIRS_DIRECTION;

/*********************************************************************************/
/* Private Module Function Prototypes */
/*********************************************************************************/
static void PAIRS_Initialize(ENSEMBLEADT_TYPE *Ensemble, 
                             STRINGADT_STRING_TYPE *StringToAdd,
                             FLOAT WindowOverhangPercentage,
                             ULONG NumberOfSequencesAdded);
static void PAIRS_CleanUp(void);
static void PAIRS_DynamicProgrammingAlignmentWithAffineGapScoresForward(ENSEMBLEADT_TYPE *Ensemble,
                                                                        STRINGADT_STRING_TYPE *StringToAdd, 
                                                                        STRINGADT_STRING_TYPE *GrammarPieceStringToAdd);
static void PAIRS_DynamicProgrammingDetermineMCellValue(ENSEMBLEADT_TYPE *Ensemble,
                                                        STRINGADT_STRING_TYPE *StringToAdd, 
                                                        STRINGADT_STRING_TYPE *GrammarPieceStringToAdd,
                                                        ULONG Col,
                                                        ULONG TBRow,
                                                        ULONG RealRow,
                                                        UBYTE WriteRow,
                                                        UBYTE ReadRow);
static void PAIRS_DynamicProgrammingDetermineIxCellValue(ENSEMBLEADT_TYPE *Ensemble,
                                                         ULONG Col,
                                                         ULONG TBRow,
                                                         ULONG RealRow,
                                                         UBYTE WriteRow);
static void PAIRS_DynamicProgrammingDetermineIyCellValue(ENSEMBLEADT_TYPE *Ensemble,
                                                         ULONG Col,
                                                         ULONG TBRow,
                                                         ULONG RealRow,
                                                         UBYTE WriteRow,
                                                         UBYTE ReadRow);
static void PAIRS_DynamicProgrammingAlignmentWithAffineGapScoresBackward(ENSEMBLEADT_TYPE *Ensemble,
                                                                         ULONG Id2,
                                                                         STRINGADT_STRING_TYPE *StringToAdd, 
                                                                         STRINGADT_STRING_TYPE *GrammarPieceStringToAdd);
static void PAIRS_DynamicProgrammingAlignmentWithAffineGapScoresBackwardOnEnsembles(ENSEMBLEADT_TYPE *Ensemble,
                                                                                    ENSEMBLEADT_TYPE *LocalGroupEnsemble);
static PAIRS_DIRECTION PAIRS_DynamicProgrammingDetermineStartingCell(ULONG FinalCol, ULONG FinalRow);

/*********************************************************************************/
/* Private Module Variables */
/*********************************************************************************/
static MATRIXFLOATADT_MATRIX_TYPE m_M;
static MATRIXFLOATADT_MATRIX_TYPE m_Ix;
static MATRIXFLOATADT_MATRIX_TYPE m_Iy;
static MATRIXUBYTEADT_MATRIX_TYPE m_TB;

static ULONG m_Rows;
static ULONG m_Cols;

static ULONG m_Start;
static ULONG m_End;
static ULONG m_WindowOverhang;
static ULONG m_RowStart;
static ULONG m_RowEnd;

static BOOLEAN m_UsingPaging;
static ULONG m_CurrentPage;
static ULONG m_RowsPerPage;

static FLOAT m_WeightIxGap;
static FLOAT m_WeightIyGap;
static FLOAT m_WeightIxGapExt;
static FLOAT m_WeightIyGapExt;

/*********************************************************************************/
/* Public and Private Module Functions */
/*********************************************************************************/

void PAIRS_ConstructPairwiseAlignment(ENSEMBLEADT_TYPE *Ensemble,
                                      STRINGADT_STRING_TYPE *StringToAdd,
                                      STRINGADT_STRING_TYPE *GrammarPieceStringToAdd,
                                      ULONG Id,
                                      FLOAT WindowOverhangPercentage)
{
   PAIRS_Initialize(Ensemble, StringToAdd, WindowOverhangPercentage, 1);
   PAIRS_DynamicProgrammingAlignmentWithAffineGapScoresForward(Ensemble, 
                                                               StringToAdd, 
                                                               GrammarPieceStringToAdd);
   PAIRS_DynamicProgrammingAlignmentWithAffineGapScoresBackward(Ensemble,
                                                                Id, 
                                                                StringToAdd, 
                                                                GrammarPieceStringToAdd);
   PAIRS_CleanUp();
}

/*********************************************************************************/

void PAIRS_MergeEnsembleAlignments(ENSEMBLEADT_TYPE *Ensemble,
                                   ENSEMBLEADT_TYPE *LocalGroupEnsemble,
                                   GROUPARRAYADT_TYPE *SequenceAlignmentOrder,
                                   FLOAT WindowOverhangPercentage)
{
   STRINGADT_STRING_TYPE ConsensusString;
   FLOAT *ConsensusConfidence;
   STRINGADT_STRING_TYPE ConsensusGrammarPieces;

   STRINGADT_InitializeString(&ConsensusString);
   STRINGADT_InitializeString(&ConsensusGrammarPieces);

   ENSEMBLEADT_GetConsensusString(LocalGroupEnsemble,
                                  &ConsensusString, 
                                  &ConsensusConfidence, 
                                  FALSE, 
                                  '-',
                                  &ConsensusGrammarPieces,
                                  TRUE);

   PAIRS_Initialize(Ensemble, 
                    &ConsensusString, 
                    WindowOverhangPercentage, 
                    LocalGroupEnsemble->NumberOfSequences);
   PAIRS_DynamicProgrammingAlignmentWithAffineGapScoresForward(Ensemble, 
                                                               &ConsensusString, 
                                                               &ConsensusGrammarPieces);
   PAIRS_DynamicProgrammingAlignmentWithAffineGapScoresBackwardOnEnsembles(Ensemble,
                                                                           LocalGroupEnsemble);
   PAIRS_CleanUp();

   STRINGADT_CleanUpString(&ConsensusString);
   STRINGADT_CleanUpString(&ConsensusGrammarPieces);
   if (ConsensusConfidence != NULL)
   {
      free(ConsensusConfidence);
   }
}

/*********************************************************************************/

void PAIRS_Initialize(ENSEMBLEADT_TYPE *Ensemble, 
                      STRINGADT_STRING_TYPE *StringToAdd,
                      FLOAT WindowOverhangPercentage,
                      ULONG NumberOfSequencesAdded)
{
   m_Rows = Ensemble->CurrentLength + PAIRS_GAP_COL_ROW_AMOUNT;
   m_Cols = StringToAdd->Length + PAIRS_GAP_COL_ROW_AMOUNT;

   if (m_Rows < m_Cols)
   {
      m_WindowOverhang = (ULONG) (((FLOAT) m_Rows) * WindowOverhangPercentage);
      m_WindowOverhang = MAX(m_WindowOverhang, 2);
      m_Start = m_WindowOverhang;
      m_End = m_Cols - m_Rows + m_WindowOverhang;
   }
   else
   {
      m_WindowOverhang = (ULONG) (((FLOAT) m_Cols) * WindowOverhangPercentage);
      m_WindowOverhang = MAX(m_WindowOverhang, 2);
      m_Start = m_Rows - m_Cols + m_WindowOverhang;
      m_End = m_WindowOverhang;
   }

   if (NumberOfSequencesAdded > Ensemble->NumberOfSequences)
   {
      m_WeightIxGap = log10(((FLOAT) Ensemble->NumberOfSequences) / ((FLOAT) NumberOfSequencesAdded));
      m_WeightIyGap = -log10(((FLOAT) NumberOfSequencesAdded) / ((FLOAT) Ensemble->NumberOfSequences));
   }
   else
   {
      m_WeightIxGap = -log10(((FLOAT) Ensemble->NumberOfSequences) / ((FLOAT) NumberOfSequencesAdded));
      m_WeightIyGap = log10(((FLOAT) NumberOfSequencesAdded) / ((FLOAT) Ensemble->NumberOfSequences));
   }
   m_WeightIxGapExt = m_WeightIxGap / 10;
   m_WeightIyGapExt = m_WeightIyGap / 10;

   m_RowsPerPage = (ULONG) (((FLOAT) PARMS_GetMaxStorageSizePerTraceBackMatrix()) * 
                            PAIRS_PAGE_ONE_MILLION_BYTES) / 
                           ((FLOAT) m_Cols);
   
   MATRIXUBYTEADT_InitializeMatrix(&m_TB);
   if (m_Rows > m_RowsPerPage)
   {
      m_UsingPaging = TRUE;
      m_CurrentPage = 0;
      MATRIXUBYTEADT_DefineMatrix(&m_TB, m_RowsPerPage, m_Cols);
   }
   else
   {
      m_UsingPaging = FALSE;
      MATRIXUBYTEADT_DefineMatrix(&m_TB, m_Rows, m_Cols);
   }

   MATRIXFLOATADT_InitializeMatrix(&m_M);
   MATRIXFLOATADT_DefineMatrix(&m_M, PAIRS_TWO_ROW_MATRIX, m_Cols, 0.0);

   MATRIXFLOATADT_InitializeMatrix(&m_Ix);
   MATRIXFLOATADT_DefineMatrix(&m_Ix, PAIRS_TWO_ROW_MATRIX, m_Cols, 0.0);

   MATRIXFLOATADT_InitializeMatrix(&m_Iy);
   MATRIXFLOATADT_DefineMatrix(&m_Iy, PAIRS_TWO_ROW_MATRIX, m_Cols, 0.0);
}

/*********************************************************************************/

void PAIRS_CleanUp(void)
{
   m_Rows = 0;
   m_Cols = 0;

   MATRIXFLOATADT_CleanUpMatrix(&m_M);
   MATRIXFLOATADT_CleanUpMatrix(&m_Ix);
   MATRIXFLOATADT_CleanUpMatrix(&m_Iy);
   MATRIXUBYTEADT_CleanUpMatrix(&m_TB);
}

/*********************************************************************************/

void PAIRS_DynamicProgrammingAlignmentWithAffineGapScoresForward(ENSEMBLEADT_TYPE *Ensemble,
                                                                 STRINGADT_STRING_TYPE *StringToAdd, 
                                                                 STRINGADT_STRING_TYPE *GrammarPieceStringToAdd)
{
   ULONG i;
   ULONG j;
   UBYTE WriteRow;
   UBYTE ReadRow;
   ULONG TBRow;

   if ((m_Rows > 1) && (m_Cols > 1))
   {
      /* 
         Reset for trace forward
      */
      Ensemble->TFNode = Ensemble->Start;
      
      for (j = 0; j < m_Rows; j++)
      {
         WriteRow = j % PAIRS_TWO_ROW_MATRIX;
         ReadRow = (j + 1) % PAIRS_TWO_ROW_MATRIX;

         if (m_UsingPaging)
         {
            TBRow = j % m_RowsPerPage;
            if (TBRow == 0)
            {
               FILEIO_WriteTraceBackMatrixPage(&m_TB, m_CurrentPage);
               m_CurrentPage++;
            }
         }
         else
         {
            TBRow = j;
         }
         
         if (m_Start > j)
         {
            m_RowStart = 0;
         }
         else
         {
            m_RowStart = (j - m_Start);
         }

         if ((m_End + j) < m_Cols)
         {
            m_RowEnd = (m_End + j);
         }
         else
         {
            m_RowEnd = m_Cols;
         }
         
         for (i = m_RowStart; i < m_RowEnd; i++)
         {
            m_TB.Rows[TBRow].Cols[i] = 0;

            PAIRS_DynamicProgrammingDetermineMCellValue(Ensemble, 
                                                        StringToAdd, 
                                                        GrammarPieceStringToAdd, 
                                                        i, 
                                                        TBRow, 
                                                        j, 
                                                        WriteRow, 
                                                        ReadRow);

            PAIRS_DynamicProgrammingDetermineIxCellValue(Ensemble, i, TBRow, j, WriteRow);

            PAIRS_DynamicProgrammingDetermineIyCellValue(Ensemble, i, TBRow, j, WriteRow, ReadRow);
         }

         if (j > 0)
         {
            /*
               Move trace to next row
            */
            Ensemble->TFNode = Ensemble->TFNode->Next;
         }
      }
   }
}

/*********************************************************************************/

void PAIRS_DynamicProgrammingDetermineMCellValue(ENSEMBLEADT_TYPE *Ensemble,
                                                 STRINGADT_STRING_TYPE *StringToAdd, 
                                                 STRINGADT_STRING_TYPE *GrammarPieceStringToAdd,
                                                 ULONG Col,
                                                 ULONG TBRow,
                                                 ULONG RealRow,
                                                 UBYTE WriteRow,
                                                 UBYTE ReadRow)
{
   FLOAT CostM;
   FLOAT CostIx;
   FLOAT CostIy;
   FLOAT CompareCost;

   if (Col == 0)
   {
      /* M = -infinity */
      if (RealRow == 0)
      {
         PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_FINAL);
      }
   }
   else 
   {
      CompareCost = ENSEMBLEADT_SubstitutionCost(Ensemble, StringToAdd->Characters[Col - 1]) + 
                    ENSEMBLEADT_GrammarPieceCost(Ensemble, GrammarPieceStringToAdd->Characters[Col - 1]);

      if (Col == 1)
      {
         if (RealRow == 0)
         {
            /* M = -infinity */
         }
         else if (RealRow == 1)
         {
            m_M.Rows[WriteRow].Cols[Col] = CompareCost;
            PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_M);
         }
         else
         {
            m_M.Rows[WriteRow].Cols[Col] = m_Iy.Rows[ReadRow].Cols[Col - 1] + CompareCost;
            PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_IY);
         }
      }
      else
      {
         if (RealRow == 0)
         {
            /* M = -infinity */
         }
         else if (RealRow == 1)
         {
            m_M.Rows[WriteRow].Cols[Col] = m_Ix.Rows[ReadRow].Cols[Col - 1] + CompareCost;
            PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_IX);
         }
         else
         {
            CostM = m_M.Rows[ReadRow].Cols[Col - 1] + CompareCost;
            CostIx = m_Ix.Rows[ReadRow].Cols[Col - 1] + CompareCost;
            CostIy = m_Iy.Rows[ReadRow].Cols[Col - 1] + CompareCost;

            if ((Col == 2) && (RealRow == 2))
            {
               m_M.Rows[WriteRow].Cols[Col] = CostM;
               PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_M);
            }
            else
            {
               if ((Col == 2) || (Col == m_RowStart))
               {
                  PAIRS_DP_SET_VALUE(CostM,
                                     CostIy,
                                     m_M.Rows[WriteRow].Cols[Col],
                                     m_TB.Rows[TBRow].Cols[Col],
                                     PAIRS_M_POS_M,
                                     PAIRS_M_POS_IY);
               }
               else if ((Col == (m_RowEnd - 1)) || (RealRow == 2))
               {
                  PAIRS_DP_SET_VALUE(CostM,
                                     CostIx,
                                     m_M.Rows[WriteRow].Cols[Col],
                                     m_TB.Rows[TBRow].Cols[Col],
                                     PAIRS_M_POS_M,
                                     PAIRS_M_POS_IX);
               }
               else
               {
                  if (CostM > CostIx)
                  {
                     PAIRS_DP_SET_VALUE(CostM,
                                        CostIy,
                                        m_M.Rows[WriteRow].Cols[Col],
                                        m_TB.Rows[TBRow].Cols[Col],
                                        PAIRS_M_POS_M,
                                        PAIRS_M_POS_IY);
                  }
                  else if (CostM < CostIx)
                  {
                     PAIRS_DP_SET_VALUE(CostIx,
                                        CostIy,
                                        m_M.Rows[WriteRow].Cols[Col],
                                        m_TB.Rows[TBRow].Cols[Col],
                                        PAIRS_M_POS_IX,
                                        PAIRS_M_POS_IY);
                  }
                  else
                  {
                     if (CostM > CostIy)
                     {
                        m_M.Rows[WriteRow].Cols[Col] = CostM;
                        if (COIN_FLIP)
                        {
                           PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_IX);
                        }
                        else
                        {
                           PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_M);
                        }
                     }
                     else if (CostM < CostIy)
                     {
                        m_M.Rows[WriteRow].Cols[Col] = CostIy;
                        PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_IY);
                     }
                     else
                     {
                        m_M.Rows[WriteRow].Cols[Col] = CostM;
                        if (COIN_FLIP)
                        {
                           PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_IY);
                        }
                        else if (COIN_FLIP)
                        {
                           PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_IX);
                        }
                        else
                        {
                           PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_M_POS_M);
                        }
                     }
                  }
               }
            }
         }
      }
   }
}

/*********************************************************************************/

void PAIRS_DynamicProgrammingDetermineIxCellValue(ENSEMBLEADT_TYPE *Ensemble,
                                                  ULONG Col,
                                                  ULONG TBRow,
                                                  ULONG RealRow,
                                                  UBYTE WriteRow)
{
   FLOAT CostM;
   FLOAT CostIx;

   if (Col == 0)
   {
      /* Ix = -infinity */
      if (RealRow == 0)
      {
         PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_IX_POS_FINAL);
      }
   }
   else 
   {
      if (Col == 1)
      {
         if (RealRow == 0)
         {
            m_Ix.Rows[WriteRow].Cols[Col] = ENSEMBLEADT_InDelTailGapStartCost + m_WeightIxGap;
            PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_IX_POS_IX);
         }
         else
         {
            /* Ix = -infinity */
         }
      }
      else
      {
         /*
            If we are on the first or final row, then we need to adjust for the ending tail of Y.
         */
         if ((RealRow == 0) || (RealRow == (m_Rows - 1)))
         {
            CostM = m_M.Rows[WriteRow].Cols[Col - 1] + ENSEMBLEADT_InDelTailGapStartCost + m_WeightIxGap;
            CostIx = m_Ix.Rows[WriteRow].Cols[Col - 1] + ENSEMBLEADT_InDelTailGapExtensionCost + m_WeightIxGapExt;
         }
         else
         {
            CostM = m_M.Rows[WriteRow].Cols[Col - 1] + Ensemble->TFNode->Info.IxGapCost + m_WeightIxGap;
            CostIx = m_Ix.Rows[WriteRow].Cols[Col - 1] + ENSEMBLEADT_InDelGapExtensionCost + m_WeightIxGapExt;
         }

         if (RealRow == 0)
         {
            m_Ix.Rows[WriteRow].Cols[Col] = CostIx;
            PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_IX_POS_IX);
         }
         else
         {
            if ((Col == 2) || (Col == (m_RowStart + 1)))
            {
               m_Ix.Rows[WriteRow].Cols[Col] = CostM;
               PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_IX_POS_M);
            }
            else
            {
               PAIRS_DP_SET_VALUE(CostM,
                                  CostIx,
                                  m_Ix.Rows[WriteRow].Cols[Col],
                                  m_TB.Rows[TBRow].Cols[Col],
                                  PAIRS_IX_POS_M,
                                  PAIRS_IX_POS_IX);
            }
         }
      }
   }
}

/*********************************************************************************/

void PAIRS_DynamicProgrammingDetermineIyCellValue(ENSEMBLEADT_TYPE *Ensemble,
                                                  ULONG Col,
                                                  ULONG TBRow,
                                                  ULONG RealRow,
                                                  UBYTE WriteRow,
                                                  UBYTE ReadRow)
{
   FLOAT CostM;
   FLOAT CostIy;

   if (RealRow == 0)
   {
      /* Iy = -infinity */
      if (Col == 0)
      {
         PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_IY_POS_FINAL);
      }
   }
   else
   {
      if (RealRow == 1)
      {
         if (Col == 0)
         {
            m_Iy.Rows[WriteRow].Cols[Col] = ENSEMBLEADT_InDelTailGapStartCost + m_WeightIyGap;
            PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_IY_POS_IY);
         }
         else
         {
            /* Iy = -infinity */
         }
      }
      else
      {
         /*
            If we are on the first or final column, then we need to adjust for the ending tail of X.
         */
         if ((Col == 0) || (Col == (m_Cols - 1)))
         {
            CostM = m_M.Rows[ReadRow].Cols[Col] + ENSEMBLEADT_InDelTailGapStartCost + m_WeightIyGap;
            CostIy = m_Iy.Rows[ReadRow].Cols[Col] + ENSEMBLEADT_InDelTailGapExtensionCost + m_WeightIyGapExt;
         }
         else
         {
            CostM = m_M.Rows[ReadRow].Cols[Col] + Ensemble->TFNode->Info.IyGapCost + m_WeightIyGap;
            CostIy = m_Iy.Rows[ReadRow].Cols[Col] + ENSEMBLEADT_InDelGapExtensionCost + m_WeightIyGapExt;
         }

         if (Col == 0)
         {
            m_Iy.Rows[WriteRow].Cols[Col] = CostIy;
            PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_IY_POS_IY);
         }
         else
         {
            if ((RealRow == 2) || 
                (RealRow == (m_Cols + 1 - m_End)) ||
                (Col == ((m_RowEnd - 1) - 1)))
            {
               m_Iy.Rows[WriteRow].Cols[Col] = CostM;
               PAIRS_SET_VALUE(m_TB.Rows[TBRow].Cols[Col],PAIRS_IY_POS_M);
            }
            else
            {
               PAIRS_DP_SET_VALUE(CostM,
                                  CostIy,
                                  m_Iy.Rows[WriteRow].Cols[Col],
                                  m_TB.Rows[TBRow].Cols[Col],
                                  PAIRS_IY_POS_M,
                                  PAIRS_IY_POS_IY);
            }
         }
      }
   }
}

/*********************************************************************************/

void PAIRS_DynamicProgrammingAlignmentWithAffineGapScoresBackward(ENSEMBLEADT_TYPE *Ensemble,
                                                                  ULONG Id2,
                                                                  STRINGADT_STRING_TYPE *StringToAdd, 
                                                                  STRINGADT_STRING_TYPE *GrammarPieceStringToAdd)
{
   ULONG i;
   ULONG j;
   PAIRS_DIRECTION Cell;
   BOOLEAN Done = FALSE;
   ULONG TBRow;
   
   i = m_Cols - 1;
   j = m_Rows - 1;
   
   Cell = PAIRS_DynamicProgrammingDetermineStartingCell(i, j % PAIRS_TWO_ROW_MATRIX);

   if (m_UsingPaging)
   {
      TBRow = j % m_RowsPerPage;
   }
   else
   {
      TBRow = j;
   }

   /*
      Reset for trace back
   */
   Ensemble->TBNode = Ensemble->End;

   while (Done == FALSE)
   {
      switch(Cell)
      {
         case PAIRS_IX:
            Cell = PAIRS_GET_IX_VALUE(m_TB.Rows[TBRow].Cols[i]);
            if (Cell != PAIRS_FINAL)
            {
               if (j > 0)
               {
                  ENSEMBLEADT_PerformTraceBackIx(Ensemble,
                                                 StringToAdd->Characters[i - 1], 
                                                 GrammarPieceStringToAdd->Characters[i - 1], 
                                                 Id2);
               }
               else
               {
                  ENSEMBLEADT_PerformTraceBackIxFinal(Ensemble,
                                                      StringToAdd->Characters[i - 1], 
                                                      GrammarPieceStringToAdd->Characters[i - 1], 
                                                      Id2);
                  Ensemble->TBNode = Ensemble->TBNode->Prev;
               }

               i--;
            }
            break;
            
         case PAIRS_IY:
            Cell = PAIRS_GET_IY_VALUE(m_TB.Rows[TBRow].Cols[i]);
            if (Cell != PAIRS_FINAL)
            {
               if (i > 0)
               {
                  ENSEMBLEADT_PerformTraceBackIy(Ensemble, Id2, 1);
               }
               else
               {
                  ENSEMBLEADT_PerformTraceBackIyFinal(Ensemble, Id2, 1);
               }

               Ensemble->TBNode = Ensemble->TBNode->Prev; 

               j--;
               if (m_UsingPaging)
               {
                  TBRow = j % m_RowsPerPage;
                  if (TBRow == (m_RowsPerPage - 1))
                  {
                     if (m_CurrentPage == 0)
                     {
                        printf("Error: there is a problem with the trace-back paging.\n");
                        exit(1);
                     }
                     else
                     {
                        m_CurrentPage--;            
                        FILEIO_ReadTraceBackMatrixPage(&m_TB, m_CurrentPage);
                        FILEIO_DeleteTraceBackMatrixPage(m_CurrentPage);
                     }
                  }
               }
               else
               {
                  TBRow = j;
               }
            }
            break;
            
         case PAIRS_M:
            Cell = PAIRS_GET_M_VALUE(m_TB.Rows[TBRow].Cols[i]);
            if (Cell != PAIRS_FINAL)
            {
               ENSEMBLEADT_PerformTraceBackM(Ensemble,
                                             StringToAdd->Characters[i - 1], 
                                             GrammarPieceStringToAdd->Characters[i - 1], 
                                             Id2,
                                             1);

               Ensemble->TBNode = Ensemble->TBNode->Prev; 

               i--;
               j--;
               if (m_UsingPaging)
               {
                  TBRow = j % m_RowsPerPage;
                  if (TBRow == (m_RowsPerPage - 1))
                  {
                     if (m_CurrentPage == 0)
                     {
                        printf("Error: there is a problem with the trace-back paging.\n");
                        exit(1);
                     }
                     else
                     {
                        m_CurrentPage--;            
                        FILEIO_ReadTraceBackMatrixPage(&m_TB, m_CurrentPage);
                        FILEIO_DeleteTraceBackMatrixPage(m_CurrentPage);
                     }
                  }
               }
               else
               {
                  TBRow = j;
               }
            }
            break;

         case PAIRS_FINAL:
            Done = TRUE;
            break;
            
         default:
            printf("Error: there is a problem with the trace-back.\n");
            exit(1);
            break;
      }
   }

   /*
      Complete trace back
   */
   Ensemble->NumberOfSequences += 1;
}

/*********************************************************************************/

void PAIRS_DynamicProgrammingAlignmentWithAffineGapScoresBackwardOnEnsembles(ENSEMBLEADT_TYPE *Ensemble,
                                                                             ENSEMBLEADT_TYPE *LocalGroupEnsemble)
{
   ULONG i;
   ULONG j;
   ULONG k;
   PAIRS_DIRECTION Cell;
   BOOLEAN Done = FALSE;
   ULONG TBRow;
   
   i = m_Cols - 1;
   j = m_Rows - 1;
   
   Cell = PAIRS_DynamicProgrammingDetermineStartingCell(i, j % PAIRS_TWO_ROW_MATRIX);

   if (m_UsingPaging)
   {
      TBRow = j % m_RowsPerPage;
   }
   else
   {
      TBRow = j;
   }

   /*
      Reset for trace back
   */
   Ensemble->TBNode = Ensemble->End;
   LocalGroupEnsemble->TBNode = LocalGroupEnsemble->End;

   while (Done == FALSE)
   {
      switch(Cell)
      {
         case PAIRS_IX:
            Cell = PAIRS_GET_IX_VALUE(m_TB.Rows[TBRow].Cols[i]);
            if (Cell != PAIRS_FINAL)
            {
               if (j > 0)
               {
                  ENSEMBLEADT_PerformTraceBackIx(Ensemble,
                                                 LocalGroupEnsemble->TBNode->Info.Characters[0],
                                                 LocalGroupEnsemble->TBNode->Info.GrammarPieces[0],
                                                 Ensemble->NumberOfSequences);
                  Ensemble->TBNode = Ensemble->TBNode->Next;
               }
               else
               {
                  ENSEMBLEADT_PerformTraceBackIxFinal(Ensemble,
                                                      LocalGroupEnsemble->TBNode->Info.Characters[0],
                                                      LocalGroupEnsemble->TBNode->Info.GrammarPieces[0],
                                                      Ensemble->NumberOfSequences);
                  Ensemble->TBNode = Ensemble->TBNode->Prev;
               }

               for (k = 1; k < LocalGroupEnsemble->NumberOfSequences; k++)
               {
                  ENSEMBLEADT_PerformTraceBackM(Ensemble,
                                                LocalGroupEnsemble->TBNode->Info.Characters[k], 
                                                LocalGroupEnsemble->TBNode->Info.GrammarPieces[k], 
                                                Ensemble->NumberOfSequences + k,
                                                k + 1);
               }

               Ensemble->TBNode = Ensemble->TBNode->Prev; 

               LocalGroupEnsemble->TBNode = LocalGroupEnsemble->TBNode->Prev; 
               i--;
            }
            break;
            
         case PAIRS_IY:
            Cell = PAIRS_GET_IY_VALUE(m_TB.Rows[TBRow].Cols[i]);
            if (Cell != PAIRS_FINAL)
            {
               for (k = 0; k < LocalGroupEnsemble->NumberOfSequences; k++)
               {
                  if (i > 0)
                  {
                     ENSEMBLEADT_PerformTraceBackIy(Ensemble, Ensemble->NumberOfSequences + k, k + 1);
                  }
                  else
                  {
                     ENSEMBLEADT_PerformTraceBackIyFinal(Ensemble, Ensemble->NumberOfSequences + k, k + 1);
                  }
               }

               Ensemble->TBNode = Ensemble->TBNode->Prev; 

               j--;
               if (m_UsingPaging)
               {
                  TBRow = j % m_RowsPerPage;
                  if (TBRow == (m_RowsPerPage - 1))
                  {
                     if (m_CurrentPage == 0)
                     {
                        printf("Error: there is a problem with the trace-back paging.\n");
                        exit(1);
                     }
                     else
                     {
                        m_CurrentPage--;            
                        FILEIO_ReadTraceBackMatrixPage(&m_TB, m_CurrentPage);
                        FILEIO_DeleteTraceBackMatrixPage(m_CurrentPage);
                     }
                  }
               }
               else
               {
                  TBRow = j;
               }
            }
            break;
            
         case PAIRS_M:
            Cell = PAIRS_GET_M_VALUE(m_TB.Rows[TBRow].Cols[i]);
            if (Cell != PAIRS_FINAL)
            {
               for (k = 0; k < LocalGroupEnsemble->NumberOfSequences; k++)
               {
                  ENSEMBLEADT_PerformTraceBackM(Ensemble,
                                                LocalGroupEnsemble->TBNode->Info.Characters[k], 
                                                LocalGroupEnsemble->TBNode->Info.GrammarPieces[k], 
                                                Ensemble->NumberOfSequences + k,
                                                k + 1);
               }

               Ensemble->TBNode = Ensemble->TBNode->Prev; 
               LocalGroupEnsemble->TBNode = LocalGroupEnsemble->TBNode->Prev; 

               i--;
               j--;
               if (m_UsingPaging)
               {
                  TBRow = j % m_RowsPerPage;
                  if (TBRow == (m_RowsPerPage - 1))
                  {
                     if (m_CurrentPage == 0)
                     {
                        printf("Error: there is a problem with the trace-back paging.\n");
                        exit(1);
                     }
                     else
                     {
                        m_CurrentPage--;            
                        FILEIO_ReadTraceBackMatrixPage(&m_TB, m_CurrentPage);
                        FILEIO_DeleteTraceBackMatrixPage(m_CurrentPage);
                     }
                  }
               }
               else
               {
                  TBRow = j;
               }
            }
            break;

         case PAIRS_FINAL:
            Done = TRUE;
            break;
            
         default:
            printf("Error: there is a problem with the trace-back.\n");
            exit(1);
            break;
      }
   }

   /*
      Complete trace back
   */
   Ensemble->NumberOfSequences += LocalGroupEnsemble->NumberOfSequences;
}

/*********************************************************************************/

PAIRS_DIRECTION PAIRS_DynamicProgrammingDetermineStartingCell(ULONG FinalCol, ULONG FinalRow)
{
   PAIRS_DIRECTION Cell;
   FLOAT MVal;
   FLOAT IxVal;
   FLOAT IyVal;
   
   MVal = m_M.Rows[FinalRow].Cols[FinalCol];
   IxVal = m_Ix.Rows[FinalRow].Cols[FinalCol];
   IyVal = m_Iy.Rows[FinalRow].Cols[FinalCol];
   
   if (MVal > IxVal)
   {
      if (MVal > IyVal)
      {
         Cell = PAIRS_M;
      }
      else if (MVal < IyVal)
      {
         Cell = PAIRS_IY;
      }
      else
      {
         if (COIN_FLIP)
         {
            Cell = PAIRS_IY;
         }
         else
         {
            Cell = PAIRS_M;
         }
      }
   }
   else if (MVal < IxVal)
   {
      if (IxVal > IyVal)
      {
         Cell = PAIRS_IX;
      }
      else if (IxVal < IyVal)
      {
         Cell = PAIRS_IY;
      }
      else
      {
         if (COIN_FLIP)
         {
            Cell = PAIRS_IY;
         }
         else
         {
            Cell = PAIRS_IX;
         }
      }
   }
   else
   {
      if (MVal > IyVal)
      {         
         if (COIN_FLIP)
         {
            Cell = PAIRS_IX;
         }
         else
         {
            Cell = PAIRS_M;
         }
      }
      else if (MVal < IyVal)
      {
         Cell = PAIRS_IY;
      }
      else
      {
         if (COIN_FLIP)
         {
            Cell = PAIRS_IX;
         }
         else if (COIN_FLIP)
         {
            Cell = PAIRS_IY;
         }
         else
         {
            Cell = PAIRS_M;
         }
      }
   }

   return (Cell);
}
