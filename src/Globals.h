/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <time.h>

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/
#define VERSION_MAJOR            3
#define VERSION_MINOR            00
#define VERSION_DATE             "9/25/12"

#define MIN(a,b)                 ((a)<(b)?(a):(b))
#define MAX(a,b)                 ((a)>(b)?(a):(b))

#define ALPHABETIC_CHARACTERS    27 /* Including an unknown index */ 
#define ALPHABETIC_UNKNOWN_INDEX 26
#define ALPHA_INDEX(a)           (((a >= 'A') && (a <= 'Z'))?(a - 'A'):(ALPHABETIC_UNKNOWN_INDEX))

#define FALSE                    0
#define TRUE                     1

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/
typedef char            BYTE;
typedef unsigned char   UBYTE;

typedef short           SHORT;
typedef unsigned short  USHORT;

typedef long            LONG;
typedef unsigned long   ULONG;

typedef float           FLOAT;

typedef int             INT;
typedef unsigned int    UINT;

typedef unsigned char   BOOLEAN;

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/
