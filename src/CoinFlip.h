/*****************************************************************/
/*  University of Nebraska-Lincoln                               */
/*  Department of Electrical Engineering                         */
/*  David J. Russell                                             */
/*****************************************************************/

#ifdef _COINFLIP
#define COINFLIP_EXTERN
#else
#define COINFLIP_EXTERN extern
#endif

/******************************************************************/
/* Public Module Constants                                        */
/******************************************************************/
#define COIN_FLIP    (((COINFLIP_Mask = (1 << ++COINFLIP_Index)) < 0) ? \
                        COINFLIP_Reset() : \
                        (((COINFLIP_Bits & COINFLIP_Mask) == COINFLIP_Mask) ? TRUE : FALSE))

/******************************************************************/
/* Public Type Definitions                                        */
/******************************************************************/

/*********************************************************************************/
/* Public Module Variables */
/*********************************************************************************/
COINFLIP_EXTERN INT COINFLIP_Bits;
COINFLIP_EXTERN INT COINFLIP_Mask;
COINFLIP_EXTERN UBYTE COINFLIP_Index;

/*********************************************************************************/
/* Public Module Function Prototypes */
/*********************************************************************************/
BOOLEAN COINFLIP_Reset(void);

